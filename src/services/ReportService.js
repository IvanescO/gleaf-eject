import http from './http';

class ReportService {
  static async createReport(report) {
    return http.get('report', { params: report });
  }
}
export default ReportService;
