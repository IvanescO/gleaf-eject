import StackNavigator from 'react-navigation/src/navigators/StackNavigator';
import InventoryList from '../../pages/InventoryScreens/InventoryList';
import Inventory from '../../pages/InventoryScreens/Inventory';
import Cart from '../../pages/CartScreens/Cart';
import Certificate from '../../pages/InventoryScreens/Certificate';
import InventoryHistory from '../../pages/InventoryScreens/InventoryHistory';

const InventoryRouting = StackNavigator(
  {
    InventoryList: { screen: InventoryList },
    InventoryEdit: { screen: Inventory },
    Cart: { screen: Cart },
    Certificate: { screen: Certificate },
    InventoryHistory: { screen: InventoryHistory },
    Inventory: { screen: Inventory }
  },
  {
    initialRouteName: 'InventoryList',
    headerMode: 'none',

    navigationOptions: {
      drawerLockMode: 'locked-closed',
      headerVisible: false
    }
  }
);

export default InventoryRouting;
