import React from 'react';
import { StyleSheet, Text, ListView, RefreshControl, Modal, Alert } from 'react-native';
import {
  Container,
  List,
  ListItem,
  Row,
  Col,
  Button,
  Icon,
  Header,
  Item,
  Input,
  View,
  Form,
  Spinner,
  Picker,
  CheckBox
} from 'native-base';
import connect from 'react-redux/lib/connect/connect';
import Layout from '../../components/Layout';
import { ORDERS_REQUESTING, DEL_ORDER, GET_ORDER_TYPES, UPDATE_ORDER } from '../../constants/index';
import { colors } from '../../theme';
import OrdersService from '../../services/OrdersService';
import { Pagination } from '../../components';

class OrdersList extends React.Component {
  state = {
    isRefreshing: false,
    orders: [],
    orderTypes: [],
    modalVisible: false,
    params: { search: '', page: 1 },
    selectedOrder: []
  };
  ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
  componentWillMount() {
    const { getOrderTypes } = this.props;
    const { params } = this.state;
    getOrderTypes();
    this.getOrders(params);
  }

  goToPage = num => {
    this.setState({ currentPage: num });
    this.getPaginatedInventoryList(num); //TODO: add action
  };
  getPaginatedInventoryList = pageNum => {
    const { params } = this.state;
    this.getOrders({ ...params, page: pageNum });
  };
  getOrders = params => {
    const { getOrders } = this.props;

    getOrders(params, this.userHasRole('customer_admin') || this.userHasRole('customer_agent'));
  };
  componentWillReceiveProps(nextProps) {
    console.log('herreee');
    const { OrdersReducer } = nextProps;
    const { orders, orderTypes, updatingMes, currentPage, itemsPerPage, total } = OrdersReducer;

    if (updatingMes) {
      this.setState({
        orders,
        orderTypes,
        modalVisible: false,
        currentPage,
        totalPage: Math.ceil(total / itemsPerPage)
      });
    } else {
      this.setState({
        orders,
        orderTypes,
        currentPage,
        totalPage: Math.ceil(total / itemsPerPage)
      });
    }
  }
  editOrder = order => {
    const id = order.id;
    const mode = order.payment_details ? 'update' : 'create';
    this.props.navigation.navigate('OrderEdit', {
      id,
      mode
    });
  };
  refreshOrderList = () => {
    // const { getOrders } = this.props;
    const { params } = this.state;
    // getOrders(params);
    this.getOrders(params);
  };
  goToDetailView = id => {
    this.props.navigation.navigate('OrderInfo', {
      id,
      isMarket: this.userHasRole('customer_admin') || this.userHasRole('customer_agent')
    });
  };
  deleteOrderByID = id => {
    const { delOrder } = this.props;
    delOrder(id);
  };
  updateOrder = () => {
    const { updatedOrder, order_status } = this.state;
    updatedOrder.order_status_id = order_status;
    const { updateOrder } = this.props;
    updateOrder(updatedOrder);
  };
  Search = () => {
    const { params } = this.state;
    // const { getOrders } = this.props;
    this.getOrders(params);
  };
  userHasRole = role => {
    const { AuthReducer } = this.props;
    return AuthReducer.user.roles.includes(role);
  };
  isOrderSelected = id => Boolean(~this.state.selectedOrder.indexOf(id));

  mergeOrders = async () => {
    const { selectedOrder, params } = this.state;
    const mergedOrder = await OrdersService.mergeOrder(selectedOrder);
    // const { getOrders } = this.props;
    // getOrders(params);
    this.getOrders(params);
    this.setState({ selectedOrder: [] });
  };
  render() {
    const { OrdersReducer } = this.props;
    const { orderListLoading, orders } = OrdersReducer;
    const TITLE = 'Orders';
    const { orderTypes, order_status, modalVisible } = this.state;

    ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

    return (
      <Layout title={TITLE} drawerOpen={() => this.props.navigation.navigate('DrawerOpen')}>
        <Container>
          <Header
            style={{
              backgroundColor: '#f6f6f6',
              borderBottomColor: 'rgba(255, 255, 255, 0.4)',
              borderTopWidth: 0
            }}
            searchBar
            rounded
          >
            <Item style={{ flex: 0.75 }}>
              <Icon name="ios-search" />
              <Input
                onChangeText={value => {
                  console.log(value);
                  this.setState({ params: { ...this.state.params, search: value } });
                }}
                placeholder="Search"
              />
            </Item>

            <Item style={{ flex: 0.2 }}>
              <Button
                style={{ flex: 1, justifyContent: 'center' }}
                small
                onPress={this.Search}
                success
              >
                <Text style={{ color: '#fff' }}>Search</Text>
              </Button>
            </Item>
          </Header>
          <View style={{ height: 50 }}>
            <Row>
              <Col size={25} style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Text>Agent</Text>
              </Col>
              <Col size={30} style={{ justifyContent: 'center' }}>
                <Text>Customer</Text>
              </Col>
              <Col size={30} style={{ justifyContent: 'center' }}>
                <Text>Date created</Text>
              </Col>
              {!(this.userHasRole('customer_admin') || this.userHasRole('customer_agent')) && (
                <Col size={15} style={{ justifyContent: 'center', alignContent: 'center' }}>
                  <Button
                    onPress={this.mergeOrders}
                    disabled={this.state.selectedOrder.length <= 1}
                    style={{ height: 25, paddingHorizontal: 5 }}
                    success
                  >
                    <Text style={{ color: '#fff', fontSize: 12 }}>Merge</Text>
                  </Button>
                </Col>
              )}
            </Row>
          </View>
          <Modal onRequestClose={() => {}} animationType="slide" transparent visible={modalVisible}>
            <Container style={{ justifyContent: 'center' }}>
              <Form
                style={{
                  backgroundColor: 'white',
                  height: 130,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderColor: colors.primary,
                  borderWidth: 1
                }}
              >
                <Row style={{ justifyContent: 'center' }}>
                  <Picker
                    mode="dropdown"
                    placeholder="Order status"
                    iosIcon={<Icon name="ios-arrow-down-outline" />}
                    selectedValue={order_status}
                    onValueChange={value => {
                      console.log(value);
                      this.setState({ order_status: value });
                    }}
                  >
                    {orderTypes.map(type => (
                      <Picker.Item key={type.id} label={type.inventory_status} value={type.id} />
                    ))}
                  </Picker>
                </Row>

                <Row style={{ justifyContent: 'center' }}>
                  <Col style={{ paddingHorizontal: 20 }}>
                    <Button
                      style={{
                        // alignSelf: 'center',
                        paddingHorizontal: 40
                        // color: colors.secondary,
                        // marginVertical: 20
                      }}
                      onPress={() => {
                        this.setState({ modalVisible: false });
                      }}
                      block
                      danger
                    >
                      <Text style={{ color: '#fff' }}>Cancel</Text>
                    </Button>
                  </Col>
                  <Col style={{ paddingHorizontal: 40 }}>
                    <Button
                      style={{
                        // alignSelf: 'center',
                        paddingHorizontal: 20
                        // color: colors.secondary,
                        // marginVertical: 20
                      }}
                      onPress={this.updateOrder}
                      success
                      block
                    >
                      <Text style={{ color: '#fff' }}>Update</Text>
                    </Button>
                  </Col>
                </Row>
              </Form>
            </Container>
          </Modal>
          {Boolean(!orderListLoading && orders) && (
            <List
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isRefreshing}
                  onRefresh={this.refreshOrderList}
                />
              }
              dataSource={this.ds.cloneWithRows(orders)}
              renderRow={data => (
                <ListItem
                  style={data.edit_mode ? { backgroundColor: colors.disabled } : {}}
                  onPress={() => (!data.edit_mode ? this.goToDetailView(data.id) : null)}
                >
                  <Row>
                    <Col size={10} style={{ justifyContent: 'center', alignItems: 'center' }}>
                      <Icon
                        info
                        type="FontAwesome"
                        onPress={() => {
                          //TODO: MOVE TO OWN FUNCTION
                          if (data.edit_mode) return;
                          this.setState({
                            updatedOrder: data,
                            order_status: data.order_status_id,
                            modalVisible: true
                          });
                        }}
                        name="circle"
                        style={{ color: data.status_color, fontSize: 15 }}
                      />
                    </Col>

                    <Col size={20}>
                      <Text>{data.order_agent}</Text>
                    </Col>
                    <Col size={30}>
                      <Text>{data.customer}</Text>
                    </Col>
                    <Col size={30}>
                      <Text>{data.date_created}</Text>
                    </Col>
                    {!(
                      this.userHasRole('customer_admin') || this.userHasRole('customer_agent')
                    ) && (
                      <Col size={10}>
                        <CheckBox
                          onPress={value => {
                            // TODO: MOVE TO OWN FUNCTION
                            if (data.edit_mode) return;
                            if (this.isOrderSelected(data.id)) {
                              const index = this.state.selectedOrder.indexOf(data.id);
                              console.log(index);
                              this.state.selectedOrder.splice(index, 1);
                              const selectedOrder = [...this.state.selectedOrder];
                              console.log(selectedOrder);
                              return this.setState({
                                selectedOrder
                              });
                            }
                            this.setState({
                              selectedOrder: [...this.state.selectedOrder, data.id]
                            });
                          }}
                          checked={this.isOrderSelected(data.id)}
                          color="green"
                        />
                      </Col>
                    )}
                  </Row>
                </ListItem>
              )}
              renderRightHiddenRow={(data, secId, rowId, rowMap) => {
                if (this.userHasRole('customer_admin') || this.userHasRole('customer_agent')) {
                  return;
                }
                return (
                  <Row>
                    {!data.url && (
                      <Col>
                        <Button
                          warning
                          style={data.edit_mode ? { backgroundColor: colors.darkDisabled } : {}}
                          onPress={() => {
                            if (data.edit_mode) return;
                            this.props.navigation.navigate('OrderHistory', {
                              orderID: data.id
                            });
                            rowMap[`${secId}${rowId}`].props.closeRow();
                          }}
                        >
                          <Icon warning type="FontAwesome" name="history" />
                        </Button>
                      </Col>
                    )}
                    {!data.url && (
                      <Col>
                        <Button
                          info
                          style={data.edit_mode ? { backgroundColor: colors.darkDisabled } : {}}
                          onPress={() => {
                            if (data.edit_mode) return;
                            this.editOrder(data, secId, rowId, rowMap);
                            rowMap[`${secId}${rowId}`].props.closeRow();
                          }}
                        >
                          <Icon info type="FontAwesome" name="money" />
                        </Button>
                      </Col>
                    )}
                    {!data.url && (
                      <Col>
                        <Button
                          danger
                          style={data.edit_mode ? { backgroundColor: colors.darkDisabled } : {}}
                          onPress={() => {
                            if (data.edit_mode) return;
                            this.deleteOrderByID(data.id, secId, rowId, rowMap);
                            rowMap[`${secId}${rowId}`].props.closeRow();
                          }}
                        >
                          <Icon active type="FontAwesome" name="trash" />
                        </Button>
                      </Col>
                    )}
                  </Row>
                );
              }}
              renderLeftHiddenRow={(data, secId, rowId, rowMap) => {
                if (this.userHasRole('customer_admin') || this.userHasRole('customer_agent')) {
                  return (
                    <Row>
                      {data.url && ( 
                        <Col>
                          <Button
                            info
                            style={data.edit_mode ? { backgroundColor: colors.darkDisabled } : {}}
                            onPress={() => {
                              if (data.edit_mode) return;
                              this.props.navigation.navigate('Invoice', {
                                url: data.url
                              });
                              rowMap[`${secId}${rowId}`].props.closeRow();
                            }}
                          >
                            <Icon warning type="FontAwesome" name="cloud-download" />
                          </Button>
                        </Col>
                      )}
                    </Row>
                  );
                }
                return (
                  <Row>
                    {data.url && (
                      <Col>
                        <Button
                          info
                          style={data.edit_mode ? { backgroundColor: colors.darkDisabled } : {}}
                          onPress={() => {
                            if (data.edit_mode) return;
                            this.props.navigation.navigate('Invoice', {
                              url: data.url
                            });
                            rowMap[`${secId}${rowId}`].props.closeRow();
                          }}
                        >
                          <Icon warning type="FontAwesome" name="cloud-download" />
                        </Button>
                      </Col>
                    )}
                    {!data.url && (
                      <Col>
                        <Button
                          warning
                          style={data.edit_mode ? { backgroundColor: colors.darkDisabled } : {}}
                          onPress={() => {
                            if (data.edit_mode) return;
                            this.alertBeforeMove(data, rowId);
                            rowMap[`${secId}${rowId}`].props.closeRow();
                          }}
                        >
                          <Icon warning type="FontAwesome" name="pencil" />
                        </Button>
                      </Col>
                    )}
                    {!data.url && (
                      <Col>
                        <Button
                          info
                          style={data.edit_mode ? { backgroundColor: colors.darkDisabled } : {}}
                          onPress={() => {
                            console.log('test');
                            if (data.edit_mode) return;
                            this.createInvoice(data, rowId);
                            rowMap[`${secId}${rowId}`].props.closeRow();
                          }}
                        >
                          <Icon warning type="FontAwesome" name="credit-card" />
                        </Button>
                      </Col>
                    )}
                  </Row>
                );
              }}
              disableLeftSwipe={
                this.userHasRole('customer_admin') || this.userHasRole('customer_agent')
              }
              // disableRightSwipe={data => console.log("",data)}
              leftOpenValue={125}
              rightOpenValue={-175}
            />
          )}
          {Boolean(orderListLoading) && <Spinner style={{ height: 300 }} />}
          {!orderListLoading && (
            <Pagination
              currentPage={this.state.currentPage}
              totalPage={this.state.totalPage}
              pageHandler={this.goToPage}
            />
          )}
        </Container>
      </Layout>
    );
  }
  createInvoice = async (data, rowID) => {
    try {
      const response = await OrdersService.createOrderInvoice(data.id);
      const order = response.data;
      let { orders } = this.state;
      orders = [...orders];
      orders[rowID] = order;
      const { params } = this.state;
      // getOrders(params);

      this.setState({ orders });
      this.getOrders(params);
    } catch (e) {}
  };
  moveOrderToCart = async (order, rowID) => {
    console.log('////////////');
    console.log('////////////');
    console.log(order);
    console.log('////////////');
    try {
      const cartResponse = await OrdersService.updateOrderInCart(order);

      const orders = [...this.state.orders].map(e => {
        e.edit_mode = false;
        return e;
      });
      order.edit_mode = true;
      orders[rowID] = order;
      this.setState({ orders });
      const { params } = this.state;
      this.getOrders(getOrders);
    } catch (e) {
      console.log(e);
    } //TODO: ADD VALIDATION
  };

  alertBeforeMove = (order, rowID) => {
    console.log(order);
    console.log(this.props.CartReducer);
    const {
      CartReducer: { cart }
    } = this.props;
    if (cart.length) {
      Alert.alert(
        'Confirtmation',
        'Your cart is not empty! Remove all items from the Cart and move current order to the Cart for editing?',
        [
          { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          {
            text: 'OK',
            onPress: () => this.moveOrderToCart(order, rowID)
          }
        ]
      );
    } else {
      this.moveOrderToCart(order, rowID);
    }
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#d3da49',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 40
  },
  inputContainer: {
    marginTop: 20
  }
});
function mapStateToProps(state) {
  const { OrdersReducer, AuthReducer, CartReducer } = state;
  return { OrdersReducer, AuthReducer, CartReducer };
}
function mapDispatchToProps(dispatch) {
  return {
    getOrders: (params, isForMarket = false) => {
      console.log(isForMarket);
      dispatch({ type: ORDERS_REQUESTING, params, isForMarket });
    },
    delOrder: id => dispatch({ type: DEL_ORDER, id }),
    updateOrder: order => dispatch({ type: UPDATE_ORDER, order }),
    getOrderTypes: () => {
      dispatch({ type: GET_ORDER_TYPES });
    }
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrdersList);
