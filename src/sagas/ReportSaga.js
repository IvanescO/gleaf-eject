/**
 * Watchers
 */
import { takeEvery, call, put, fork, all } from 'redux-saga/effects';
import ReportService from '../services/ReportService';
import { CREATE_REPORT, CREATE_REPORT_SUCCESSFUL, CREATE_REPORT_FAILURE } from '../constants/index';

function* createReport(action) {
  try {
    const response = yield call(ReportService.createReport, action.report);
    const report = response.data;
    console.log(report);
    yield put({ type: CREATE_REPORT_SUCCESSFUL, report });
  } catch (error) {
    yield put({ type: CREATE_REPORT_FAILURE, error });
  }
}
function* watchCreateReport() {
  yield takeEvery(CREATE_REPORT, createReport);
}

export default function* () {
  yield [fork(watchCreateReport)];
}
