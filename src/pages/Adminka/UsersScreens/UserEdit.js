import React from 'react';
import { connect } from 'react-redux';
import Layout from '../../../components/Layout';
import { USER_EDIT_STORE } from '../../../constants/index';
import UserForm from './UserForm';

class UserEdit extends React.Component {
  componentWillMount = async () => {
    const { params } = this.props.navigation.state;
    const { STORE_USERS } = this.props;
    console.log(params.customer);
    STORE_USERS(params.customer);
  };
  render() {
    const TITLE = 'UserEdit';
    const { params } = this.props.navigation.state;
    console.log(this.props);
    return (
      <Layout title={TITLE} drawerOpen={() => this.props.navigation.navigate('DrawerOpen')}>
        <UserForm customerID={params.rowId} />
      </Layout>
    );
  }
}
function mapStateToProps(state) {
  return {};
}
function mapDispatchToProps(dispatch) {
  return {
    STORE_USERS: user => {
      dispatch({ type: USER_EDIT_STORE, user });
    }
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserEdit);
