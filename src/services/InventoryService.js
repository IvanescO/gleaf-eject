import http from './http';

class InventoryService {
  static async getInventory(params = { type: { id: 'all' }, search: '' }) {
    const type = params.type.id === 'all' ? '' : params.type.id;
    return http.get('inventory', {
      params: {
        search: params.search,
        hide_empty: params.hide_empty,
        type,
        page: params.page
      }
    });
  }
  static async getMarketInventory(params = { type: { id: 'all' }, search: '' }) {
    const type = params.type.id === 'all' ? '' : params.type.id;
    return http.get('market-inventory', {
      params: {
        search: params.search,
        hide_empty: params.hide_empty,
        type,
        page: params.page
      }
    });
  }
  static async updateInventory(inventory) {
    console.log(inventory);
    return http.put(`inventory/${inventory.id}`, inventory);
  }
  static async createInventory(inventory) {
    return http.post('inventory', inventory);
  }
  static async getSingleInventory(id) {
    return http.get(`inventory/${id}`);
  }
  static async delInventory(id) {
    return http.delete(`inventory/${id}`);
  }
  static async getInventoryType() {
    return http.get('inventory-type');
  }

  static async getInventoryHistory(id) {
    return http.get(`inventory/${id}/history`);
  }
  static async getQbProducts() {
    return http.get('inventory-qbproducts');
  }
}
export default InventoryService;
