import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button, Input } from '../components';
import connect from 'react-redux/lib/connect/connect';
import { USER_AUTHENTICATION } from '../constants';
import Layout from '../components/Layout';
class AdminMarketPlace extends React.Component {
  render() {
    const { AuthReducer } = this.props;
    const TITLE = 'Admin Market Place';
    console.log(this.props);
    return (
      <Layout title={TITLE} drawerOpen={() => this.props.navigation.navigate('DrawerOpen')}>
        {/* <View style={styles.container}> */}
        <Text>Admin Market Place</Text>
        {/* </View> */}
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#d3da49',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 40
  },
  inputContainer: {
    marginTop: 20
  }
});
function mapStateToProps(state) {
  const { AuthReducer } = state;
  return { AuthReducer };
}

export default AdminMarketPlace; //connect(mapStateToProps)(MarketPlace);
