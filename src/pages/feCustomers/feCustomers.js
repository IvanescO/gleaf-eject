import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
// import { Pagination } from '../../components';
import { StyleSheet } from 'react-native';
import { TabHeading, Tab, Tabs, Text } from 'native-base';
import Layout from '../../components/Layout';
import FeCustomersList from './feCustomersList';
import FeCustomersNew from './feCustomersNew';
import { FE_CUSTOMER_LIST, CREATE_FE_CUSTOMER, FE_CUSTOMER_STORE } from '../../constants/index';

class FeCustomersTabs extends Component {
  state = {
    feCustomers: null
  };

  componentWillMount = async () => {
    const { getCustomersList } = this.props;
    getCustomersList({});
  };

  componentWillReceiveProps = async nextProps => {
    const { feCustomersReducer } = nextProps;
    const { feCustomers, currentPage, itemsPerPage, total, feCustomerLoading } = feCustomersReducer;
    console.log(feCustomers, currentPage, itemsPerPage, total);
    this.setState({
      feCustomers,
      currentPage,
      totalPage: Math.ceil(total / itemsPerPage),
      feCustomerLoading
    });
  };

  submitNewCustomer = (newCustomer, form) => {
    const { createCustomer } = this.props;
    createCustomer(newCustomer, form);
  };

  render() {
    const { feCustomers } = this.state;
    const { storeFeCustomer } = this.props;
    const TITLE = 'Customers';
    const TAB1_TITLE = 'Customers List';
    const TAB2_TITLE = 'Create customer';

    // console.log("FE state", this.state);

    return (
      <Layout
        style={styles.tabContainer}
        title={TITLE}
        drawerOpen={() => this.props.navigation.navigate('DrawerOpen')}
      >
        <Tabs activeTextStyle={{ color: '#fff' }}>
          <Tab
            heading={TAB1_TITLE}
            tabStyle={styles.tab}
            activeTextStyle={styles.tabText}
            textStyle={styles.tabText}
            activeTabStyle={styles.tab}
          >
            <FeCustomersList
              getCustomersList={this.props.getCustomersList}
              customerMode={false}
              {...this.state}
              {...this.props}
            />
          </Tab>

          <Tab
            heading={TAB2_TITLE}
            tabStyle={styles.tab}
            activeTextStyle={styles.tabText}
            textStyle={styles.tabText}
            activeTabStyle={styles.tab}
          >
            <FeCustomersNew
              customerMode
              onSubmitNewCustomer={this.submitNewCustomer}
              {...this.props}
            />
          </Tab>
        </Tabs>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  tabContainer: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  tab: {
    backgroundColor: '#5cb85c'
  },
  tabText: {
    color: '#ffffff'
  }
});

function mapStateToProps(state) {
  const { feCustomersReducer } = state;
  return { feCustomersReducer };
}

function mapDispatchToProps(dispatch) {
  return {
    getCustomersList: params => {
      dispatch({ type: FE_CUSTOMER_LIST, params });
    },
    createCustomer: (newCustomer, form) => {
      dispatch({ type: CREATE_FE_CUSTOMER, newCustomer, form });
    },
    storeFeCustomer: feCustomer => {
      dispatch({ type: FE_CUSTOMER_STORE, feCustomer });
    }
  };
}

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(FeCustomersTabs);
