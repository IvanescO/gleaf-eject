import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { Button, Input } from '../components';
import connect from 'react-redux/lib/connect/connect';
import { USER_AUTHENTICATION } from '../constants';
import { Icon } from 'native-base';
import { colors } from '../theme';

class SignIn extends React.Component {
  state = {
    username: '',
    password: '',
    isAuthenticating: false //TODO: fix it
  };

  render() {
    const { username, password, isAuthenticating } = this.state;
    const { AuthReducer } = this.props;
    return (
      <View style={styles.container}>
        <View style={{ alignItems: 'center' }}>
          <Image source={require('../../assets/gLeaf-logo-green.fw.png')} />
        </View>

        <View style={styles.inputContainer}>
          <Input
            placeholder="User Name"
            type="username"
            onChangeText={this.onChangeText}
            value={username}
          />
          <Input
            placeholder="Password"
            type="password"
            onChangeText={this.onChangeText}
            value={password}
            secureTextEntry
          />
        </View>
        {AuthReducer.errors && <Text style={styles.errors}>{AuthReducer.errors}</Text>}
        <Button isLoading={AuthReducer.requestingAuth} title="Sign In" onPress={this.signIn} />

        {AuthReducer.requestingAuth && <Text>isLoading</Text>}
      </View>
    );
  }
  signIn = () => {
    const { username, password } = this.state;
    const { userAuthenticate } = this.props;
    const cerdentials = { username, password };
    userAuthenticate(cerdentials);
  };
  onChangeText = (key, value) => {
    this.setState({
      [key]: value
    });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    // alignItems: "center",
    justifyContent: 'center',
    paddingHorizontal: 40
  },
  errors: {
    color: colors.error
  },
  inputContainer: {
    marginTop: 20
  }
});
function mapStateToProps(state) {
  const { AuthReducer } = state;
  return { AuthReducer };
}
function mapDispatchToProps(dispatch) {
  return {
    userAuthenticate: cerdentials => {
      dispatch({ type: USER_AUTHENTICATION, payload: { cerdentials } });
    }
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignIn);
