import _ from 'lodash';
import * as cst from '../constants';

const initialState = {
  requestingInventory: false,
  inventory: [],
  selectedInventory: {},
  inventoryTypes: []
};

function MarketplaceReducer(state = initialState, action) {
  // console.log("MarketplaceReducer", action)
  switch (action.type) {
    case cst.MARKETPLACE_REQUESTING: {
      return {
        ...state,
        requestingInventory: true,
        inventory: [],
        errors: '',
        message: ''
      };
    }
    case cst.MARKETPLACE_SUCCESSFUL: {
      return {
        ...state,
        requestingInventory: false,
        inventory: action.inventory,
        currentPage: action.currentPage,
        total: action.total,
        itemsPerPage: action.itemsPerPage,
        errors: '',
        inventoryTypes: action.inventoryTypes || state.inventoryTypes
      };
    }
    case cst.MARKETPLACE_FAILURE: {
      return {
        ...state,
        requestingInventory: false,
        errors: action.errors
      };
    }

    default:
      return state;
  }
}
export default MarketplaceReducer;
