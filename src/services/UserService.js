import http from './http';

class UsersService {
  static async getUsersList(params) {
    return http.get('user', { params });
  }

  static async getUserByID(id) {
    return http.get(`user/${id}`);
  }
  static async updUser(newUser) {
    return http.put(`user/${newUser.id}`, newUser);
  }

  static async getUserStatue() {
    return http.get('user-account-status');
  }
}
export default UsersService;
