Artem Lashin, [13.07.18 11:43]
private function handleRequest(UrlManager $urlManager)
    {
        try {
            switch($urlManager->getRoute()) {
                case 'login':
                    if ($urlManager->isPost()) {
                        return (new Controller())->userLogin($this->getRawData());
                    }
                    return $this->getDefaultMessage();

                case 'registration':
                    if ($urlManager->isPost()) {
                        return (new Controller())->registrationNewUser($this->getRawData());
                    }
                    return $this->getDefaultMessage();

                case 'inventory':
                    if ($urlManager->isGet()) {
                        if(empty($urlManager->getID())) {
                            return (new Controller(true, true))->getInventoryList($urlManager->getGetParams());
                        } else {
                            if ($urlManager->getAction() == 'history') {
                                return (new Controller(true, true))->getInventoryHistory($urlManager->getID());
                            }
                            return (new Controller(true, true))->getInventory($urlManager->getID());
                        }
                    }
                    if ($urlManager->isPost()) {
                        if ($urlManager->getAction() == 'add-to-cart') {
                            return (new Controller(true, true))->addInventoryToCart($urlManager->getID(), $this->getRawData());
                        }
                        return (new Controller(true, true))->createInventory($this->getRawData());
                    }
                    if ($urlManager->isPut()) {
                        return (new Controller(true, true))->updateInventory($urlManager->getID(), $this->getRawData());
                    }
                    if ($urlManager->isDelete()) {
                        return (new Controller(true, true))->deleteInventory($urlManager->getID());
                    }
                    return $this->getDefaultMessage();

                case 'inventory-type':
                    if ($urlManager->isGet()) {
                        return (new Controller(true))->getInventoryTypes();
                    }
                    return $this->getDefaultMessage();

                case 'inventory-status':
                    if ($urlManager->isGet()) {
                        return (new Controller(true))->getInventoryStatuses();
                    }
                    return $this->getDefaultMessage();

                case 'inventory-harvest':
                    if ($urlManager->isGet()) {
                        return (new Controller(true))->getInventoryHarvest();
                    }
                    return $this->getDefaultMessage();

                case 'inventory-qbproducts':
                    if ($urlManager->isGet()) {
                        return (new Controller(true))->getQbProductsList();
                    }
                    return $this->getDefaultMessage();

                case 'report':
                    if ($urlManager->isGet()) {
                        return (new Controller(true, true))->getReportList($urlManager->getGetParams());
                    }
                    return $this->getDefaultMessage();

Artem Lashin, [13.07.18 11:43]
case 'order':
                    if ($urlManager->isGet()) {
                        if(empty($urlManager->getID())) {
                            return (new Controller(true, true))->getOrders($urlManager->getGetParams());
                        } else {
                            if ($urlManager->getAction() == 'history') {
                                return (new Controller(true, true))->getOrderHistory($urlManager->getID());
                            }
                            return (new Controller(true, true))->getOrder($urlManager->getID());
                        }
                    }
                    if ($urlManager->isPost()) {
                        if ($urlManager->getAction() == 'create_invoice') {
                            return (new Controller(true, true))->createOrderInvoice($urlManager->getID());
                        }
                        return (new Controller(true, true))->handleAdminCart();
                    }
                    if ($urlManager->isPut()) {
                        if ($urlManager->getAction() == 'inventory') {
                            return (new Controller(true, true))
                                ->updateInventoryOrder($urlManager->getID(), $urlManager->getActionID(), $this->getRawData());
                        }

                        if ($urlManager->getAction() == 'update') {
                            return (new Controller(true, true))->loadOrderToCart($urlManager->getID());
                        }

                        return (new Controller(true, true))->changeOrderStatus($urlManager->getID(), $this->getRawData());
                    }
                    if ($urlManager->isDelete()) {
                        return (new Controller(true, true))->deleteOrder($urlManager->getID());
                    }
                    return $this->getDefaultMessage();

                case 'order-payment':
                    if ($urlManager->isPost()) {
                        return (new Controller(true, true))->createOrderPayment($this->getRawData());
                    }
                    if ($urlManager->isPut()) {
                        return (new Controller(true, true))->updateOrderPayment($urlManager->getID(), $this->getRawData());
                    }
                    return $this->getDefaultMessage();

                case 'order-status':
                    if ($urlManager->isGet()) {
                        if(empty($urlManager->getID())) {
                            return (new Controller(true, false))->getOrderStatuses($urlManager->getGetParams()); //TODO onlyForAdmin changed to false for Vanya
                        } else {
                            return (new Controller(true, false))->getOrderStatus($urlManager->getID()); //TODO onlyForAdmin changed to false for Vanya
                        }
                    }
                    return $this->getDefaultMessage();

                case 'orders-merge':
                    if ($urlManager->isPut()) {
                        return (new Controller(true, true))->mergeOrders($this->getRawData());
                    }
                    return $this->getDefaultMessage();
                case 'payment-type':
                    if ($urlManager->isGet()) {
                        if(empty($urlManager->getID())) {
                            return (new Controller(true, false))->getPaymentTypes($urlManager->getGetParams()); //TODO onlyForAdmin changed to false for Vanya
                        } else {
                            return (new Controller(true, false))->getPaymentType($urlManager->getID()); //TODO onlyForAdmin changed to false for Vanya
                        }
                    }
                    return $this->getDefaultMessage();

Artem Lashin, [13.07.18 11:43]
case 'cart':
                    if ($urlManager->isGet()) {
                        return (new Controller(true, true))->getCart();
                    }
                    if ($urlManager->isPut()) {
                        return (new Controller(true, true))->updateCart($this->getRawData());
                    }
                    if ($urlManager->isDelete()) {
                        return (new Controller(true, true))->clearCart();
                    }
                    return $this->getDefaultMessage();

                case 'cart-item':
                    if ($urlManager->isPut()) {
                        return (new Controller(true, true))->updateCartItem($urlManager->getID(), $this->getRawData());
                    }
                    if ($urlManager->isDelete()) {
                        return (new Controller(true, true))->deleteCartItem($urlManager->getID());
                    }
                    return $this->getDefaultMessage();

                case 'check-cart':
                    if ($urlManager->isGet()) {
                        return (new Controller(true, true))->checkCart();
                    }
                    return $this->getDefaultMessage();

                case 'market-cart':
                    if ($urlManager->isGet()) {
                        return (new Controller(true))->getMarketCart();
                    }
                    if ($urlManager->isPost()) {
                        return (new Controller(true))->createMarketCart($this->getRawData());
                    }
                    if ($urlManager->isDelete()) {
                        return (new Controller(true))->clearMarketCart();
                    }
                    return $this->getDefaultMessage();

                case 'market-cart-timeout':
                    if ($urlManager->isDelete()) {
                        return (new Controller(true))->clearMarketCartTimeout();
                    }
                    return $this->getDefaultMessage();

                case 'market-cart-item':
                    if ($urlManager->isDelete()) {
                        return (new Controller(true))->deleteMarketCartItem($urlManager->getID());
                    }
                    return $this->getDefaultMessage();

                case 'user':
                    if ($urlManager->isGet()) {
                        if(empty($urlManager->getID())) {
                            return (new Controller(true, true))->getUserList($urlManager->getGetParams());
                        }
                    }
                    if ($urlManager->isPut()) {
                        return (new Controller(true, true))->updateUser($urlManager->getID(), $this->getRawData());
                    }

                    return $this->getDefaultMessage();
                case 'customer':
                    if ($urlManager->isGet()) {
                        if(empty($urlManager->getID())) {
                            return (new Controller(true, true))->getCustomerList($urlManager->getGetParams());
                        }
                    }

                    return $this->getDefaultMessage();
                case 'customer-roles':
                    if ($urlManager->isGet()) {
                        return (new Controller(true, true))->getCustomerRoles();
                    }
                    return $this->getDefaultMessage();

                case 'user-account-status':
                    if ($urlManager->isGet()) {
                        return (new Controller(true, true))->getUserAccountStatuses();
                    }
                    return $this->getDefaultMessage();

                case 'market-inventory':
                    if ($urlManager->isGet()) {
                        return (new Controller(true))->getMarketInventoryList($urlManager->getGetParams());
                    }

                    return $this->getDefaultMessage();

                case 'market-order':
                    if ($urlManager->isGet()) {
                        if(empty($urlManager->getID())) {
                            return (new Controller(true))->getMarketOrders($urlManager->getGetParams());
                        } else {
                            return (new Controller(true))->getMarketOrder($urlManager->getID());
                        }
                    }
                    if ($urlManager->isPost()) {
                        return (new Controller(true))->createMarketOrder();
                    }
                    return $this->getDefaultMessage();

                case 'market-customer':
                    if ($urlManager->isGet()) {
                        return (new Controller(true))->getMarketCustomers($urlManager->getGetParams());
                    }
                    if ($urlManager->isPost()) {
                        return (new Controller(true))->createMarketCustomer($this->getRawData());
                    }
                    return $this->getDefaultMessage();

                case 'notification':
                    if ($urlManager->isGet()) {
                        return (new Controller(true))->getCustomerNotification();
                    }
                    if ($urlManager->isPost()) {
                        return (new Controller(true))->createCustomerNotification($this->getRawData());
                    }
                    return $this->getDefaultMessage();

                default:
                    return $this->getDefaultMessage();