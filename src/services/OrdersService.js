import http from './http';

class OrderService {
  static async getOrderList(params) {
    return http.get('order', {
      params: {
        search: params.search,
        page: params.page
      }
    });
  }
  static async getMarketOrderList(params) {
    return http.get('market-order', {
      params: {
        search: params.search,
        page: params.page
      }
    });
  }
  static async createOrderInvoice(id) {
    return http.post(`order/${id}/create_invoice`);
  }
  static async updateOrder(order) {
    console.log(order);
    return http.put(`order/${order.id}`, order);
  }
  static async createAdminOrder(order) {
    order.admin_cart = true;
    return http.post('order', order);
  }
  static async getOrder(id) {
    return http.get(`order/${id}`);
  }
  static async getMarketOrder(id) {
    return http.get(`market-order/${id}`);
  }
  static async delOrder(id) {
    return http.delete(`order/${id}`);
  }
  static async getOrderType() {
    return http.get('order-status'); // order-status
  }
  static async getPaymentType() {
    return http.get('payment-type');
  }
  static async createPaymentDetail(body) {
    body.payment_type_id = body.payment_type_id || 3;
    return http.post('order-payment/', body);
  }
  static async updPaymentDetail(body) {
    return http.put(`order-payment/${body.id}`, body);
  }
  static async getOrderHistory(id) {
    return http.get(`order/${id}/history`);
  }
  static async mergeOrder(orders) {
    return http.put('/orders-merge', { orders });
  }
  static async updateOrderInCart(order) {
    return http.put(`order/${order.id}/update`);
  }
  static async createMarketOrder() {
    return http.post('market-order');
  }
}
export default OrderService;
