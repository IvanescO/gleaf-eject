import _ from 'lodash';
import {
  INVENTORY_FAILURE,
  INVENTORY_REQUESTING,
  INVENTORY_SUCCESSFUL,
  UPDATE_INVETORY,
  UPDATE_INVETORY_SUCCESSFUL,
  UPDATE_INVETORY_FAILURE,
  DEL_INVENTORY_SUCCESSFUL,
  SINGLE_INVENTORY,
  SINGLE_INVENTORY_SUCCESSFUL,
  SINGLE_INVENTORY_FAILURE,
  CREATE_INVENTORY,
  CREATE_INVENTORY_FAILURE,
  CREATE_INVENTORY_SUCCESSFUL,
  INVENTORY_QBPRODUCTS,
  INVENTORY_QBPRODUCTS_SUCCESSFUL,
  INVENTORY_QBPRODUCTS_FAILURE,
} from '../constants';

const initialState = {
  requestingInventory: false,
  single_loading: false,
  inventory: [],
  selectedInventory: {},
  inventoryTypes: [],
  qbProducts: []
};

function InventoryReducer(state = initialState, action) {
  switch (action.type) {
    case INVENTORY_REQUESTING: {
      return { ...state, requestingInventory: true, inventory: [], errors: '', message: '' };
    }
    case INVENTORY_SUCCESSFUL: {
      return {
        ...state,
        requestingInventory: false,
        inventory: action.inventory,
        currentPage: action.currentPage,
        total: action.total,
        itemsPerPage: action.itemsPerPage,
        errors: '',
        inventoryTypes: action.inventoryTypes || state.inventoryTypes
      };
    }
    case INVENTORY_FAILURE: {
      return { ...state, requestingInventory: false, errors: action.errors };
    }
    case UPDATE_INVETORY: {
      return { ...state, loading: true, errors: '', message: '' };
    }
    case UPDATE_INVETORY_SUCCESSFUL: {
      const inventory = state.inventory;
      const index = _.findIndex(inventory, elem => elem.id === action.inventory.id);
      inventory[index] = action.inventory;
      return {
        ...state,
        inventory,
        loading: false,
        errors: '',
        message: 'Inventory Updated Successful'
      };
    }
    case UPDATE_INVETORY_FAILURE: {
      return { ...state, loading: false, errors: 'Inventory Updated Failure', message: '' };
    }
    case DEL_INVENTORY_SUCCESSFUL: {
      const inventory = state.inventory;
      _.remove(inventory, elem => elem.id === action.id);
      return { ...state, inventory };
    }
    case SINGLE_INVENTORY: {
      return { ...state, single_loading: true, errors: '', message: '' };
    }
    case SINGLE_INVENTORY_SUCCESSFUL: {
      return {
        ...state,
        selectedInventory: action.inventory,
        single_loading: false,
        inventoryTypes: action.inventoryTypes
      };
    }
    case SINGLE_INVENTORY_FAILURE: {
      return { ...state, single_loading: false, errors: action.errors, message: '' };
    }
    case CREATE_INVENTORY: {
      return { ...state, loading: true, errors: '', message: '' };
    }
    case CREATE_INVENTORY_FAILURE: {
      return { ...state, loading: false, errors: 'Inventory Created Failure', message: '' };
    }
    case CREATE_INVENTORY_SUCCESSFUL: {
      return {
        ...state,
        inventory: [...state.inventory, action.inventory],
        loading: false,
          selectedInventory:{},
        message: 'Inventory Created Successful',
        errors: ''
      };
    }

    case INVENTORY_QBPRODUCTS: {
      return {
        ...state,
        single_loading: true,
        errors: '',
        message: ''
      };
    }
    case INVENTORY_QBPRODUCTS_SUCCESSFUL: {
      return {
        ...state,
        single_loading: false,
        qbProducts: action.qbProducts,
        message: 'Fetch inventory-qbproducts Successful',
        errors: ''
      };
    }
    case INVENTORY_QBPRODUCTS_FAILURE: {
      return {
        ...state,
        single_loading: false,
        message: 'Fetch inventory-qbproducts Failure',
        errors: action.errors,
      };
    }

    default:
      return state;
  }
}
export default InventoryReducer;
