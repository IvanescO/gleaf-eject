import React from 'react';
import { StyleSheet, Text, View, AppRegistry } from 'react-native';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { Root } from 'native-base';
import Tabs from './src/auth/AuthTabs';
import store from './src/store';
import AppInit from './src/AppInit';

export default class App extends React.Component {
  state = { loading: true };
  render() {
    console.disableYellowBox = true;
    console.log(store);
    return (
      <Provider store={store}>
        <Root>
          <AppInit />
        </Root>
        {/* <Tabs /> */}
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#19e3f1',
    alignItems: 'center',
    justifyContent: 'center'
  }
});
