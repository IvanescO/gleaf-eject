import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { Item, Input, Label, Icon } from 'native-base';

const RenderInput = ({
  input, disabled, label, isPassword = false, meta: { touched, error, valid }
}) => {

  let hasError = false;
  if (touched && error && valid !== undefined) {
    hasError = true;
  }

  return (
    <Item disabled={disabled} style={styles.input} error={hasError}>
      <Label>{label}</Label>
      <Input disabled={disabled} secureTextEntry={isPassword} {...input} />
      {hasError ? <Icon name="close-circle" /> : <Text />}
    </Item>
  );
};

const styles = StyleSheet.create({
  input: {
    margin: 10
  }
});

export default RenderInput;
