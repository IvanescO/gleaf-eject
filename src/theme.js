const colors = {
  primary: '#1fb50c',
  secondary: '#b9b9b9',
  error: '#FF0000',
  disabled: '#C0C0C0',
  darkDisabled: '#606060',
  white: '#fff',
  black: '#000'
};

const fonts = {
  hairline: 'Lato-Hairline',
  light: 'Lato-Light',
  base: 'Lato-Regular',
  bold: 'Lato-Bold'
};

export { colors, fonts };
