import React, { Component } from 'react';
import Pdf from 'react-native-pdf';
import { StyleSheet, Text, View, WebView, Dimensions } from 'react-native';
import connect from 'react-redux/lib/connect/connect';
import Layout from '../../components/Layout';

class InvoicePage extends React.Component {
  state = {
    source: 'http://testwp.smiss.ua/wp-content/uploads/certificate/1A40301000000CA000002920.pdf'
  };
  componentWillMount() {
    const { params } = this.props.navigation.state;
    this.setState({ source: params.url });
    console.log(params);
  }
  render() {
    const TITLE = 'Invoice';
    console.log(this.props);
    const source = { uri: this.state.source, cache: true };
    console.log(source);

    return (
      <Layout title={TITLE} drawerOpen={() => this.props.navigation.navigate('DrawerOpen')}>
        <Pdf
          source={source}
          onLoadComplete={(numberOfPages, filePath) => {
            console.log(`number of pages: ${numberOfPages}`);
          }}
          onPageChanged={(page, numberOfPages) => {
            console.log(`current page: ${page}`);
          }}
          onError={error => {
            console.log(error);
          }}
          style={styles.pdf}
        />
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#d3da49',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 40
  },
  inputContainer: {
    marginTop: 20
  },
  pdf: {
    flex: 1,
    width: Dimensions.get('window').width
  }
});
function mapStateToProps(state) {
  const { AuthReducer } = state;
  return { AuthReducer };
}

export default InvoicePage; //connect(mapStateToProps)(MarketPlace);
