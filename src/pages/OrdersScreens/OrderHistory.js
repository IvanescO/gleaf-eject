import React, { Component } from 'react';
import connect from 'react-redux/lib/connect/connect';
import {
  Text,
  Content,
  List,
  ListItem,
  Row,
  Container,
  View,
  Left,
  Right,
  Header
} from 'native-base';
import Layout from '../../components/Layout';
import OrdersService from '../../services/OrdersService';
import { Col, Grid } from 'react-native-easy-grid';

class OrderHistory extends React.Component {
  state = {
    orderID: '',
    history: null,
    differences: []
  };
  componentWillMount = async () => {
    const { params } = this.props.navigation.state;
    const orderID = params.orderID;
    const orderResp = await OrdersService.getOrderHistory(orderID);
    console.log(orderResp);
    const history = orderResp.data.map(e => {
      e.message = e.message.replace(/&#39;/g, "'");
      return e;
    });
    this.setState({ orderID, history });
  };
  renderHistory = () => {
    const { history } = this.state;
    if (!history) {
      return (
        <Container>
          <Text>loading...</Text>
        </Container>
      );
    }
    if (!history.length) {
      return (
        <Container style={{ justifyContent: 'center', alignItems: 'center' }}>
          <Text>history is empty</Text>
        </Container>
      );
    }
    return history.map((e, i) => (
      <ListItem key={i}>
        <Grid>
          <Row style={{ marginBottom: 10 }}>
            <Col>
              <View style={{ flex: 1, alignItems: 'flex-start' }}>
                <Text>{e.agent}</Text>
              </View>
            </Col>

            <Col>
              <Text>{e.date}</Text>
            </Col>
          </Row>
          <Row>
            <Text>{e.message}</Text>
          </Row>
        </Grid>
      </ListItem>
    ));
  };
  render() {
    const { orderID } = this.state;
    const TITLE = `Order ${orderID} history`;
    return (
      <Layout title={TITLE} drawerOpen={() => this.props.navigation.navigate('DrawerOpen')}>
        <Content>
          <List>{this.renderHistory()}</List>
        </Content>
      </Layout>
    );
  }
}

function mapStateToProps(state) {}

export default OrderHistory; //connect(mapStateToProps)(MarketPlace);
