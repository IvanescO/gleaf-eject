import {
  AUTHENTICATION_FAILURE,
  AUTHENTICATION_SUCCESSFUL,
  USER_AUTHENTICATION,
  USER_LOGOUT,
  USER_REGISTRATION,
  REGISTRATION__FAILURE,
  REGISTRATION_SUCCESSFUL,
} from '../constants';

const initialState = {
  requestingAuth: false,
  requestingReigstration: false
};

function AuthReducer(state = initialState, action) {
  // console.log("AuthReducer", action)
  switch (action.type) {
    case USER_AUTHENTICATION: {
      return { ...state, requestingAuth: true, errors: null };
    }
    case AUTHENTICATION_SUCCESSFUL: {
      return { ...state, user: action.user, token: action.token, requestingAuth: false };
    }
    case AUTHENTICATION_FAILURE: {
      return { ...state, errors: action.errors, requestingAuth: false };
    }
    case USER_LOGOUT: {
      return { ...state, user: null, token: null, requestingAuth: false };
    }
    case USER_REGISTRATION: {
      return { ...state, requestingReigstration: true, errors: null, succsessMessage: null };
    }
    case REGISTRATION__FAILURE: {
      return {
        ...state,
        requestingReigstration: false,
        errors: action.errors,
        succsessMessage: null
      };
    }
    case REGISTRATION_SUCCESSFUL: {
      return {
        ...state,
        requestingReigstration: true,
        errors: null,
        // user: action.user,
        // token: action.token,
        succsessMessage: action.succsessMessage
      };
    }

    default:
      return state;
  }
}
export default AuthReducer;
