import React from 'react';
import { StyleSheet } from 'react-native';
import connect from 'react-redux/lib/connect/connect';
import {
  USER_AUTHENTICATION,
  SINGLE_ORDER,
  UPDATE_PAYMENT_DETAIL,
  CREATE_PAYMENT_DETAIL
} from '../../constants';
import Layout from '../../components/Layout';
import {
  Form,
  Item,
  Label,
  Input,
  Icon,
  Picker,
  Content,
  Button,
  Text,
  Spinner
} from 'native-base';
import { colors } from '../../theme';

class OrderEdit extends React.Component {
  state = {
    singleOrder: { payment_details: {} },
    paymentDetails: { payment_type_id: 1 },
    loading: false
  };

  componentWillMount() {
    const {
      params: { id, mode }
    } = this.props.navigation.state;
    console.log(mode);
    this.setState({ mode });
    const { getSingleOrder } = this.props;
    getSingleOrder(id);
  }
  componentWillReceiveProps(nextProps) {
    const { singleOrder } = nextProps.OrdersReducer;
    const paymentDetails = singleOrder.payment_details || {};
    this.setState({ singleOrder, paymentDetails });
  }
  onChangeText = (key, value) => {
    const { paymentDetails } = this.state;
    paymentDetails[key] = value;
    this.setState({
      paymentDetails
    });
  };
  updateOrderDetail = () => {
    const {
      params: { id }
    } = this.props.navigation.state;
    const { updateDetail } = this.props;
    const { paymentDetails } = this.state;
    updateDetail(id, paymentDetails);
  };

  createOrderDetail = () => {
    const {
      params: { id }
    } = this.props.navigation.state;
    const { createDetail } = this.props;
    const { paymentDetails } = this.state;
    paymentDetails.order_id = id;
    createDetail(id, paymentDetails);
  };

  render() {
    const { OrdersReducer } = this.props;
    const TITLE = 'Order Edit';
    let { singleLoading, paymentTypes, detailUpdating, updatingError } = OrdersReducer;
    const { singleOrder, paymentDetails, mode } = this.state;
    paymentTypes = paymentTypes || [];
    console.log(this.props);
    return (
      <Layout title={TITLE} drawerOpen={() => this.props.navigation.navigate('DrawerOpen')}>
        <Content>
          {Boolean(!singleLoading) && (
            <Form>
              {/* <Text>inventory type</Text> */}
              <Picker
                mode="dropdown"
                placeholder="Payment type"
                iosIcon={<Icon name="ios-arrow-down-outline" />}
                style={{ width: undefined }}
                selectedValue={paymentDetails.payment_type_id}
                onValueChange={value => this.onChangeText('payment_type_id', value)}
              >
                {paymentTypes.map(type => (
                  <Picker.Item key={type.id} label={type.payment_type} value={type.id} />
                ))}
              </Picker>
              {Boolean(mode === 'update') && (
                <Item disabled floatingLabel>
                  <Label>agent</Label>
                  <Input
                    type="agent"
                    disabled
                    value={paymentDetails.agent}
                    onChangeText={value => this.onChangeText('agent', value)}
                  />
                </Item>
              )}
              <Item floatingLabel>
                <Label>customer</Label>
                <Input
                  type="customer"
                  value={paymentDetails.customer}
                  onChangeText={value => this.onChangeText('customer', value)}
                />
              </Item>
              {Boolean(mode === 'update') && (
                <Item disabled floatingLabel>
                  <Label>amount</Label>
                  <Input
                    type="amount"
                    disabled
                    value={paymentDetails.amount}
                    onChangeText={value => this.onChangeText('amount', value)}
                  />
                </Item>
              )}
              <Item floatingLabel>
                <Label>cash received</Label>
                <Input
                  type="cash_received"
                  value={paymentDetails.cash_received}
                  onChangeText={value => this.onChangeText('cash_received', value)}
                />
              </Item>
              <Item floatingLabel>
                <Label>date received</Label>
                <Input
                  type="date_received"
                  value={paymentDetails.date_received}
                  onChangeText={value => this.onChangeText('date_received', value)}
                />
              </Item>
              <Item floatingLabel>
                <Label>Received from</Label>
                <Input
                  type="rec_from"
                  value={paymentDetails.rec_from}
                  onChangeText={value => this.onChangeText('rec_from', value)}
                />
              </Item>
              <Item floatingLabel>
                <Label>SSN</Label>
                <Input
                  type="rec_from_ssn"
                  value={paymentDetails.rec_from_ssn}
                  onChangeText={value => this.onChangeText('rec_from_ssn', value)}
                />
              </Item>
              <Item floatingLabel>
                <Label>Date of birth</Label>
                <Input
                  type="rec_from_dob"
                  value={paymentDetails.rec_from_dob}
                  onChangeText={value => this.onChangeText('rec_from_dob', value)}
                />
              </Item>
              <Item floatingLabel>
                <Label>Driver Licence</Label>
                <Input
                  type="rec_from_driver_licence"
                  value={paymentDetails.rec_from_driver_licence}
                  onChangeText={value => this.onChangeText('rec_from_driver_licence', value)}
                />
              </Item>
              {Boolean(updatingError) && (
                <Text style={{ color: colors.error }}>{updatingError}</Text>
              )}
              {Boolean(!detailUpdating) &&
                Boolean(mode === 'create') && (
                  <Button style={{ marginTop: 10 }} success full onPress={this.createOrderDetail}>
                    <Text style={{ color: '#fff' }}> Save </Text>
                  </Button>
                )}
              {Boolean(!detailUpdating) &&
                Boolean(mode === 'update') && (
                  <Button style={{ marginTop: 10 }} success full onPress={this.updateOrderDetail}>
                    <Text style={{ color: '#fff' }}> Update </Text>
                  </Button>
                )}
              {Boolean(detailUpdating) && <Spinner />}
            </Form>
          )}
        </Content>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#d3da49',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 40
  },
  inputContainer: {
    marginTop: 20
  }
});
function mapStateToProps(state) {
  const { OrdersReducer } = state;
  return { OrdersReducer };
}
function mapDispatchToProps(dispatch) {
  return {
    getSingleOrder: id => {
      dispatch({ type: SINGLE_ORDER, id });
    },
    updateDetail: (id, detail) => {
      dispatch({ type: UPDATE_PAYMENT_DETAIL, id, detail });
    },
    createDetail: (id, detail) => {
      dispatch({ type: CREATE_PAYMENT_DETAIL, id, detail });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderEdit);
