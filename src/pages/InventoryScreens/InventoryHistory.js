import React, { Component } from 'react';
import connect from 'react-redux/lib/connect/connect';
import { Text, Content, List, ListItem, Row, Container, Grid, Col, View } from 'native-base';
import Layout from '../../components/Layout';
import InventoryService from '../../services/InventoryService';
import UtilsService from '../../services/UtilsService';

class InventoryHistory extends React.Component {
  state = {
    inventoryID: '',
    history: null,
    differences: []
  };
  componentWillMount = async () => {
    const { params } = this.props.navigation.state;
    const inventoryID = params.inventoryID;
    this.setState({ inventoryID });
    const { data } = await InventoryService.getInventoryHistory(inventoryID);

    console.log(data);

    this.setState({ history: data });

    console.log(data);
  };
  renderHistory = () => {
    const { history } = this.state;
    console.log('_______________');
    console.log(history);
    console.log('_______________');
    if (!history) {
      return (
        <Container>
          <Text>loading...</Text>
        </Container>
      );
    }
    if (!history.length) {
      return (
        <Container style={{ justifyContent: 'center', alignItems: 'center' }}>
          <Text>history is empty</Text>
        </Container>
      );
    }
    return history.map((e, i) => (
      <ListItem key={i}>
        <Grid>
          <Row style={{ marginBottom: 10 }}>
            <Col>
              <View style={{ flex: 1, alignItems: 'flex-start' }}>
                <Text>{e.agent}</Text>
              </View>
            </Col>

            <Col>
              <Text>{e.date}</Text>
            </Col>
          </Row>
          <Row style={{ marginBottom: 10 }}>
            <Text>{e.event_source}</Text>
          </Row>
          <Row>
            <Text>{e.message}</Text>
          </Row>
        </Grid>
      </ListItem>
    ));
  };
  render() {
    const { inventoryID } = this.state;
    const TITLE = `Inventory ${inventoryID} history`;
    return (
      <Layout title={TITLE} drawerOpen={() => this.props.navigation.navigate('DrawerOpen')}>
        <Content>
          <List>{this.renderHistory()}</List>
        </Content>
      </Layout>
    );
  }
}

function mapStateToProps(state) {}

export default InventoryHistory; //connect(mapStateToProps)(MarketPlace);
