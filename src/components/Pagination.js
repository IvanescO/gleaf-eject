import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Row, Button, Title } from 'native-base';
import { colors } from '../theme';

export default ({ currentPage, totalPage, pageHandler }) => {
  totalPage = totalPage || 1;
  return (
    <View style={[{ height: 30, marginVertical: 10 }, styles.centerElement]}>
      <Row
        style={[
          styles.inputContainer,
          { display: 'flex', justifyContent: 'center', alignItems: 'center' }
        ]}
      >
        <Button
            success
          disabled={currentPage - 1 == 0}
          style={[styles.pageButton, { width: 60 }]}
          onPress={() => pageHandler(Number(currentPage) - 1)}
        >
          <Title style={styles.pageButtonFS}> Prev </Title>
        </Button>

        <Text style={[styles.centerElement, { marginLeft: 15, marginRight: 15 }]}>
          Page: {currentPage} of {totalPage || 1}
        </Text>

        <Button
            success
          disabled={currentPage == totalPage}
          style={[styles.pageButton, { width: 60 }]}
          onPress={() => pageHandler(Number(currentPage) + 1)}
        >
          <Title style={styles.pageButtonFS}> Next </Title>
        </Button>
      </Row>
    </View>
  );
};

const styles = StyleSheet.create({
  centerElement: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  pageButton: {
    height: 27,
    width: 23,
    marginLeft: 7,
    marginBottom: 20,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  pageButtonFS: {
    color:colors.white,
    lineHeight: 12,
    fontSize: 10
  }
});
