import { takeEvery, call, put, fork, all, delay } from 'redux-saga/effects';
import {
  ORDERS_REQUESTING,
  UPDATE_ORDER,
  DEL_ORDER,
  SINGLE_ORDER,
  CREATE_ADMIN_ORDER,
  ORDERS_SUCCESSFUL,
  ORDERS_FAILURE,
  SINGLE_ORDER_SUCCESSFUL,
  SINGLE_ORDER_FAILURE,
  DEL_ORDER_SUCCESSFUL,
  DEL_ORDER_FAILURE,
  UPDATE_PAYMENT_DETAIL,
  UPDATE_PAYMENT_DETAIL_SUCCESSFUL,
  UPDATE_PAYMENT_DETAIL_FAILURE,
  CREATE_PAYMENT_DETAIL,
  CREATE_ADMIN_ORDER_SUCCESSFUL,
  CREATE_ADMIN_ORDER_FAILURE,
  GET_ORDER_TYPES,
  GET_ORDER_TYPES_SUCCESSFUL,
  UPDATE_ORDER_SUCCESSFUL,
  UPDATE_ORDER_FAILURE,
  USER_DEL_CART_SUCCESSFUL
} from '../constants/index';
import OrdersService from '../services/OrdersService';
import { Toast } from 'native-base';

function* orderListRequesting({ params, isForMarket }) {
  try {
    let response;
    // console.log(is);
    if (isForMarket) {
      response = yield call(OrdersService.getMarketOrderList, params);
    } else {
      response = yield call(OrdersService.getOrderList, params);
    }
    console.log('______________');
    console.log(response);
    console.log('______________');
    const { currentPage, itemsPerPage, orders, total } = response.data;
    yield put({ type: ORDERS_SUCCESSFUL, currentPage, itemsPerPage, orders, total });
  } catch (listError) {
    yield put({ type: ORDERS_FAILURE, listError });
  }
}
function* updateOrder(action) {
  const oldOrder = {
    id: action.order.id,
    order_status_id: action.order.order_status_id
  };
  try {
    const response = yield call(OrdersService.updateOrder, oldOrder);
    const order = response.data;
    console.log(order);
    yield put({ type: UPDATE_ORDER_SUCCESSFUL, order });
  } catch (updateError) {
    yield put({ type: UPDATE_ORDER_FAILURE, updateError });
  }
}
function* deleteOrder(action) {
  try {
    const response = yield call(OrdersService.delOrder, action.id);
    yield put({ type: DEL_ORDER_SUCCESSFUL, id: action.id });
  } catch (e) {
    yield put({ type: DEL_ORDER_FAILURE, errors: e.message });
  }
}
function* updatePaymentDetail(action) {
  try {
    const response = yield call(OrdersService.updPaymentDetail, action.detail);

    console.log(response);
    const detail = response.data;

    yield put({ type: UPDATE_PAYMENT_DETAIL_SUCCESSFUL, orderId: action.id, detail });
  } catch (e) {
    console.log(e);
    yield put({ type: UPDATE_PAYMENT_DETAIL_FAILURE, errors: e });
  }
}
function* getSingleOrder(action) {
  const { isMarket } = action;
  console.log(isMarket);
  if (isMarket) {
    try {
      const [responseOrder, responseType] = yield all([
        call(OrdersService.getMarketOrder, action.id),
        call(OrdersService.getPaymentType)
      ]);
      const singleOrder = responseOrder.data;
      const paymentTypes = responseType.data.types;
      console.log(singleOrder);
      yield put({ type: SINGLE_ORDER_SUCCESSFUL, singleOrder, paymentTypes });
    } catch (e) {
      yield put({ type: SINGLE_ORDER_FAILURE, errors: e.message });
    }
  } else {
    try {
      const [responseOrder, responseType] = yield all([
        call(OrdersService.getOrder, action.id),
        call(OrdersService.getPaymentType)
      ]);
      const singleOrder = responseOrder.data;
      const paymentTypes = responseType.data.types;
      yield put({ type: SINGLE_ORDER_SUCCESSFUL, singleOrder, paymentTypes });
    } catch (e) {
      yield put({ type: SINGLE_ORDER_FAILURE, errors: e.message });
    }
  }
}
function* createPaymentDetail(action) {
  try {
    const response = yield call(OrdersService.createPaymentDetail, action.detail);
    console.log(response);
    const detail = response.data;

    yield put({ type: UPDATE_PAYMENT_DETAIL_SUCCESSFUL, orderId: action.id, detail });
  } catch (e) {
    console.log(e);
    yield put({ type: UPDATE_PAYMENT_DETAIL_FAILURE, errors: e });
  }
}

function* createAdminOrder(action) {
  try {
    const response = yield call(OrdersService.createAdminOrder, action.order);
    const cart = response.data;
    console.log("createAdminOrder resp", response)

    yield put({
      type: CREATE_ADMIN_ORDER_SUCCESSFUL,
      cart: cart
    });

    Toast.show({
      text: 'Order was created successfully',
      type: 'success'
    });
  } catch (e) {
    console.log(e);
    yield put({ type: CREATE_ADMIN_ORDER_FAILURE, errors: e });
    Toast.show({
      text: 'Order was failed',
      type: 'danger'
    });
  }
}


function* getOrderTypes() {
  try {
    const response = yield call(OrdersService.getOrderType);
    const orderTypes = response.data.order_statuses;
    yield put({
      type: GET_ORDER_TYPES_SUCCESSFUL,
      orderTypes
    });
  } catch (e) {}
}

function* watchOrderRequesting() {
  yield takeEvery(ORDERS_REQUESTING, orderListRequesting);
}
function* watchOrderUpdating() {
  yield takeEvery(UPDATE_ORDER, updateOrder);
}
function* watchOrderDeleting() {
  yield takeEvery(DEL_ORDER, deleteOrder);
}
function* watchGetSingleOrder() {
  yield takeEvery(SINGLE_ORDER, getSingleOrder);
}
function* watchCreateAdminOrder() {
  yield takeEvery(CREATE_ADMIN_ORDER, createAdminOrder);
}
function* watchUpdatePaymentDetail() {
  yield takeEvery(UPDATE_PAYMENT_DETAIL, updatePaymentDetail);
}
function* watchCreatePaymentDetail() {
  yield takeEvery(CREATE_PAYMENT_DETAIL, createPaymentDetail);
}
function* watchGetOrderTypes() {
  yield takeEvery(GET_ORDER_TYPES, getOrderTypes);
}
export default function* () {
  yield [
    fork(watchOrderRequesting),
    fork(watchOrderUpdating),
    fork(watchOrderDeleting),
    fork(watchGetOrderTypes),
    fork(watchGetSingleOrder),
    fork(watchCreateAdminOrder),
    fork(watchUpdatePaymentDetail),
    fork(watchCreatePaymentDetail),
  ];
}
