import React from 'react';
import { StyleSheet, ListView, RefreshControl, Modal } from 'react-native';
import {
  Container,
  Header,
  Item,
  Input,
  Icon,
  Button,
  Text,
  List,
  ListItem,
  Form,
  Label,
  View,
  Col,
  Row,
  Left,
  Spinner,
  Right,
  Picker,
  Title,
  Body,
  Toast
} from 'native-base';
import _ from 'lodash';
import connect from 'react-redux/lib/connect/connect';
import * as cst from '../../constants';
import Layout from '../../components/Layout';
import { colors } from '../../theme';
import Pagination from '../../components/Pagination';

class InventoryList extends React.Component {
  state = {
    isRefreshing: false,
    cart: [],
    cartItem: {},
    modalVisible: false,
    inventory: [],
    params: { search: '', type: { id: 'all', inventory_type: 'sad' }, hide_empty: false },
    currentPage: 1,
    totalPage: 1
  };

  ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

  refreshInventoryList = () => {
    const { params } = this.state;
    const { getCart, getInventoryList } = this.props;

    getInventoryList(params);
    getCart();
  };

  componentWillMount() {
    const { params } = this.state;
    const { getCart, getInventoryList } = this.props;

    getInventoryList(params);
    getCart();
  }

  componentWillReceiveProps(nextProps) {
    const { CartReducer, InventoryReducer } = nextProps;
    const { inventory, total, itemsPerPage } = InventoryReducer;
    const { cart, loadingAdding, showModal, timestamp } = CartReducer;

    this.setState({
      cart,
      loadingAdding,
      modalVisible: showModal,
      inventory,
      timestamp,
      totalPage: Math.ceil(total / itemsPerPage)
    });
  }

  deleteInventoryByID = (data, secId, rowId, rowMap) => {
    const { delInventory } = this.props;
    console.log(rowId);
    delInventory(data.id);
  };

  editInventory = data => {
    const { navigation } = this.props;

    navigation.navigate('Inventory', {
      id: data.id,
      inventory: data,
      mode: 'edit'
    });
  };

  showModal = data => {
    const cartItem = { inventory_id: data.id };
    console.log('showModal', data);

    this.setState({
      cartItem,
      modalVisible: true
    });
  };

  addToCart = () => {
    const { cartItem } = this.state;
    const { addInvToCart } = this.props;
    console.log('cartItem', cartItem);

    addInvToCart(cartItem);
  };

  getTypeNameById = id => {
    const {
      InventoryReducer: { inventoryTypes }
    } = this.props;
    const elem = inventoryTypes.find(type => type.id === id);
    return elem ? elem.inventory_type : '';
  };

  onChangeText = (key, value) => {
    const { cartItem } = this.state;
    cartItem[key] = value;
    this.setState({ cartItem });
  };

  hideModal = () => this.setState({ modalVisible: false });

  goToCart = () => this.props.navigation.navigate('Cart');

  goToDrawer = () => this.props.navigation.navigate('DrawerOpen');

  goToInventory = data => {
    const { navigation } = this.props;

    navigation.navigate('Inventory', {
      id: data.id,
      inventory: data,
      mode: 'view'
    });
  };

  searchByQuery = () => {
    const { params } = this.state;
    const { getInventoryList } = this.props;
    getInventoryList(params);
  };

  searchByType = id => {
    const { getInventoryList } = this.props;
    console.log('searchByType', id);

    this.setState({ params: { ...this.state.params, type: { id } } }, () => {
      const { params } = this.state;
      getInventoryList(params);
    });
  };

  emptyToggle = () => {
    const {
      params: { hide_empty }
    } = this.state;
    const { getInventoryList } = this.props;

    this.setState({ params: { ...this.state.params, hide_empty: !hide_empty } }, () => {
      const { params } = this.state;
      getInventoryList(params);
    });
  };

  goToPage = num => {
    this.setState({ currentPage: num });
    this.getPaginatedInventoryList(num);
  };

  getPaginatedInventoryList = pageNum => {
    const { params } = this.state;
    const { getInventoryList } = this.props;
    getInventoryList({ ...params, page: pageNum });
  };

  onSearchChange = value => {
    const { params } = this.state;
    this.setState({ params: { ...params, search: value } });
  };

  render() {
    const {
      cart,
      loadingAdding,
      params,
      inventory,
      totalPage,
      cartItem,
      modalVisible,
      currentPage,
      isRefreshing
    } = this.state;
    const {
      navigation,
      InventoryReducer: { requestingInventory, inventoryTypes }
    } = this.props;
    const { search, type, hide_empty } = params;

    const TITLE = 'Inventory List';
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    const newInventoryTypes = [...inventoryTypes];
    newInventoryTypes.push({ id: 'all', inventory_type: 'All' });

    console.log('PROPS', this.props);

    return (
      <Layout title={TITLE} drawerOpen={this.goToDrawer}>
        <Container>
          <Header
            style={{
              backgroundColor: '#f6f6f6',
              borderBottomWidth: 1,
              borderBottomColor: 'rgba(255, 255, 255, 0.4)'
            }}
            rounded
          >
            <Left>
              <Button iconLeft rounded small success onPress={this.goToCart}>
                <Icon type="FontAwesome" name="shopping-cart" style={{ color: '#fff' }} />
                <Text>{cart.length}</Text>
              </Button>
            </Left>
            <Body>
              {hide_empty && (
                <Button iconLeft rounded small success onPress={this.emptyToggle}>
                  <Text>Show Empty</Text>
                </Button>
              )}
              {!hide_empty && (
                <Button iconLeft rounded small success onPress={this.emptyToggle}>
                  <Text>Hide Empty</Text>
                </Button>
              )}
            </Body>
            <Right>
              <Item style={{ flex: 1 }}>
                <Picker
                  mode="dropdown"
                  placeholder="inventory type"
                  // iosIcon={<Icon name="ios-arrow-down-outline" />}
                  style={{ width: undefined }}
                  selectedValue={type.id}
                  onValueChange={this.searchByType}
                >
                  {newInventoryTypes.map(invType => (
                    <Picker.Item
                      key={invType.id}
                      label={invType.inventory_type}
                      value={invType.id}
                    />
                  ))}
                </Picker>
              </Item>
            </Right>
          </Header>
          <Header
            style={{
              backgroundColor: '#f6f6f6',
              borderBottomWidth: 1,
              borderBottomColor: 'rgba(255, 255, 255, 0.4)'
            }}
            searchBar
            rounded
          >
            <Item style={{ flex: 0.75 }}>
              <Icon name="ios-search" />
              <Input
                value={search}
                onChangeText={this.onSearchChange}
                onSearchChange
                placeholder="Search"
              />
            </Item>
            <Item style={{ flex: 0.25, alignContent: 'center' }}>
              <Button
                style={{ flex: 1, justifyContent: 'center' }}
                success
                small
                onPress={this.searchByQuery}
              >
                <Text style={{ color: colors.white, fontSize: 12 }}>Search</Text>
              </Button>
            </Item>
          </Header>
          <View style={{ height: 40 }}>
            <Row style={styles.centerElement}>
              <Col size={1} style={styles.centerElement} />
              <Col size={1}>
                <Text>Strain</Text>
              </Col>
              <Col size={1} style={styles.centerElement} />
              <Col size={1}>
                <Text>THC</Text>
              </Col>
              <Col size={3}>
                <Text>Type</Text>
              </Col>
            </Row>
          </View>
          <Modal onRequestClose={() => {}} animationType="slide" transparent visible={modalVisible}>
            <Container style={{ justifyContent: 'center' }}>
              <Form
                style={{
                  backgroundColor: 'white',
                  height: 140,
                  justifyContent: 'center',
                  borderColor: colors.primary,
                  borderWidth: 1
                }}
              >
                <Item floatingLabel>
                  <Label>price</Label>
                  <Input
                    value={cartItem.price}
                    onChangeText={value => this.onChangeText('price', value)}
                  />
                </Item>
                {Boolean(loadingAdding) && <Spinner />}
                {Boolean(!loadingAdding) && (
                  <Row style={{ justifyContent: 'center', marginTop: 10, marginBottom: 10 }}>
                    <Col style={{ paddingHorizontal: 20, backgroundColor: 'white' }}>
                      <Button style={{ marginTop: 10 }} danger block onPress={this.hideModal}>
                        <Text style={{ color: '#fff' }}> cancel </Text>
                      </Button>
                    </Col>
                    <Col style={{ paddingHorizontal: 20, backgroundColor: 'white' }}>
                      <Button style={{ marginTop: 10 }} success block onPress={this.addToCart}>
                        <Text style={{ color: '#fff' }}> add to cart </Text>
                      </Button>
                    </Col>
                  </Row>
                )}
              </Form>
            </Container>
          </Modal>

          {Boolean(!requestingInventory) &&
            Boolean(inventory.length) && (
              <Container>
                <List
                  swipeToOpenPercent={50}
                  refreshControl={
                    <RefreshControl
                      refreshing={isRefreshing}
                      onRefresh={this.refreshInventoryList}
                    />
                  }
                  dataSource={this.ds.cloneWithRows(inventory)}
                  renderRow={data => (
                    <ListItem style={{ height: 40 }} onPress={_ => this.goToInventory(data)}>
                      <Row>
                        <Col
                          size={1}
                          style={{
                            justifyContent: 'center',
                            alignItems: 'center'
                          }}
                        >
                          {data.star === 'all' && (
                            <Text>
                              <Icon
                                type="FontAwesome"
                                name="star"
                                style={{
                                  fontSize: 15,
                                  color: colors.primary,
                                  lineHeight: 20
                                }}
                              />
                            </Text>
                          )}
                          {data.star === 'advanced' && (
                            <Text>
                              <Icon
                                name="ios-star-outline"
                                style={{
                                  fontSize: 15,
                                  color: colors.primary,
                                  lineHeight: 20
                                }}
                              />
                            </Text>
                          )}
                        </Col>
                        <Col size={2}>
                          <Text style={{ fontSize: 11 }}>{data.strain}</Text>
                        </Col>
                        <Col size={1}>
                          <Text style={{ fontSize: 11 }}> {data.thc} </Text>
                        </Col>
                        <Col size={2}>
                          <Text style={{ fontSize: 11 }}>
                            {this.getTypeNameById(data.type_id || data.inventory_type_id)}
                          </Text>
                        </Col>
                        <Col size={1} style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
                          <Button small success onPress={_ => this.showModal(data)}>
                            <Icon
                              style={{ fontSize: 15 }}
                              active
                              type="FontAwesome"
                              name="cart-plus"
                            />
                          </Button>
                        </Col>
                      </Row>
                    </ListItem>
                  )}
                  renderRightHiddenRow={(data, secId, rowId, rowMap) => (
                    <Row>
                      <Col>
                        <Button
                          warning
                          onPress={_ => {
                            navigation.navigate('InventoryHistory', {
                              inventoryID: data.id //TODO: change to data.id
                            });
                            rowMap[`${secId}${rowId}`].props.closeRow();
                          }}
                        >
                          <Icon warning type="FontAwesome" name="history" />
                        </Button>
                      </Col>
                      <Col>
                        <Button
                          info
                          onPress={_ => {
                            this.editInventory(data, secId, rowId, rowMap);
                            rowMap[`${secId}${rowId}`].props.closeRow();
                          }}
                        >
                          <Icon info type="FontAwesome" name="pencil" />
                        </Button>
                      </Col>
                      <Col>
                        <Button
                          danger
                          onPress={_ => {
                            this.deleteInventoryByID(data, secId, rowId, rowMap);
                            rowMap[`${secId}${rowId}`].props.closeRow();
                          }}
                        >
                          <Icon active type="FontAwesome" name="trash" />
                        </Button>
                      </Col>
                    </Row>
                  )}
                  renderLeftHiddenRow={(data, secId, rowId, rowMap) =>
                    Boolean(data.sertificate.length) && (
                      <Row>
                        <Col>
                          <Button
                            info
                            onPress={_ => {
                              navigation.navigate('Certificate', {
                                certificate_name: data.sertificate[0].url //TODO: FIX IT
                              });
                              rowMap[`${secId}${rowId}`].props.closeRow();
                            }}
                          >
                            <Icon info type="FontAwesome" name="file" />
                          </Button>
                        </Col>
                      </Row>
                    )
                  }
                  leftOpenValue={75}
                  rightOpenValue={-175}
                />
              </Container>
            )}

          {Boolean(requestingInventory) && <Spinner style={{ height: 300 }} />}
          {Boolean(!requestingInventory) && (
            <Pagination
              currentPage={currentPage}
              totalPage={totalPage}
              pageHandler={this.goToPage}
            />
          )}
        </Container>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#d3da49',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 40
  },
  centerElement: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  modalWrap: {
    backgroundColor: 'white',
    height: 140,
    justifyContent: 'center',
    borderColor: colors.primary,
    borderWidth: 1
  },
  modalItem: {
    alignItems: 'center',
    marginTop: 10
  },
  modalBtns: {
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 10
  },
  modalBtnCol: {
    paddingHorizontal: 20,
    backgroundColor: 'white'
  },
  itemText: {
    fontSize: 15,
    color: colors.primary,
    lineHeight: 20
  }
});

function mapStateToProps(state) {
  const { InventoryReducer, CartReducer } = state;
  return { InventoryReducer, CartReducer };
}

function mapDispatchToProps(dispatch) {
  return {
    getInventoryList: params => {
      dispatch({ type: cst.INVENTORY_REQUESTING, params });
    },
    delInventory: id => {
      dispatch({ type: cst.DEL_INVENTORY, id });
    },
    getCart: () => {
      dispatch({ type: cst.ADMIN_GET_CART });
    },
    addInvToCart: item => {
      dispatch({ type: cst.ADMIN_ADD_CART_ITEM, item });
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InventoryList);
