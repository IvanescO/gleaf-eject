import { Icon, Button, Container, Header, Left, Right, Body, Title } from 'native-base';
import React, { Component } from 'react';
import connect from 'react-redux/lib/connect/connect';
import { USER_LOGOUT } from '../constants/index';

class CustomHeader extends Component {
  render() {
    const { title, drawerOpen, logout } = this.props;
    return (
      <Header
        style={{
          backgroundColor: '#f6f6f6',
          borderBottomWidth: 0.5,
          borderBottomColor: 'rgba(255, 255, 255, 0.4)'
        }}
      >
        <Left>
          <Icon
            ios="ios-menu"
            android="md-menu"
            onPress={() => {
              drawerOpen();
            }}
          />
        </Left>
        <Body>
          <Title style={{ color: '#000' }}>{title}</Title>
        </Body>
        <Right>
          <Icon
            type="FontAwesome"
            name="sign-out"
            onPress={() => {
              logout();
            }}
          />
        </Right>
      </Header>
    );
  }
}
function mapStateToProps() {
  return {};
}
function mapDispatchToProps(dispatch) {
  return {
    logout: () => dispatch({ type: USER_LOGOUT })
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(CustomHeader);
