import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import {
  Container,
  Form,
  Item,
  Label,
  Input,
  Picker,
  Icon,
  Button,
  Content,
  CardItem,
  Card,
  Right,
  Spinner,
  List,
  ListItem
} from 'native-base';
import _ from 'lodash';
import connect from 'react-redux/lib/connect/connect';
import Layout from '../../components/Layout';
import { GET_ORDER_TYPES, CREATE_REPORT } from '../../constants/index';
import  moment from "moment";

class ReportScreen extends React.Component {
  state = { orderTypes: [], report: {date_start:moment("01.01.2018").format('L'),date_end:moment().format('L')}, generatedReport: {}, reportCreating: false };
  componentWillMount() {
    const { getOrderTypes } = this.props;
    getOrderTypes();
  }
  componentWillReceiveProps(nextProps) {
    const { OrdersReducer, ReportReducer } = nextProps;
    const { orderTypes } = OrdersReducer;
    const { generatedReport, reportCreating } = ReportReducer;
    this.setState({ orderTypes, generatedReport, reportCreating });
  }
  onChangeText = (key, value) => {
    const { report } = this.state;
    report[key] = value;
    this.setState({
      report
    });
  };
  generateReport = () => {
    const { report } = this.state;
    const { createOrder } = this.props;
    createOrder(report);
  };
  goToDetailView = id => {
    this.props.navigation.navigate('Order', {
      id
    });
  };
  render() {
    const { OrdersReducer } = this.props;
    const TITLE = 'Reports';
    const { orderTypes, report, generatedReport, reportCreating } = this.state;
    console.log(orderTypes);
    return (
      <Layout title={TITLE} drawerOpen={() => this.props.navigation.navigate('DrawerOpen')}>
        <Container>
          <Content>
            <Row>
              <Col>
                <Item floatingLabel>
                  <Label>Start Date</Label>
                  <Input
                    value={report.date_start}
                    type="Start Date"
                    onChangeText={value => this.onChangeText('date_start', value)}
                  />
                </Item>
              </Col>
              <Col>
                <Item floatingLabel>
                  <Label>End Date</Label>
                  <Input
                    type="End Date"
                    value={report.date_end}
                    onChangeText={value => this.onChangeText('date_end', value)}
                  />
                </Item>
              </Col>
            </Row>
            <Row>
              <Col>
                <Item>
                  <Picker
                    mode="dropdown"
                    placeholder="Order Source"
                    iosIcon={<Icon name="ios-arrow-down-outline" />}
                    style={{ width: undefined }}
                    selectedValue={report.source}
                    onValueChange={value => this.onChangeText('source', value)}
                  >
                    <Picker.Item key={'all'} label={'all'} value={''} />
                    {
                      //TODO: ADD OMIT IF ALL
                    }
                    <Picker.Item key={'customer'} label={'from customer'} value={'customer'} />
                    <Picker.Item key={'manually'} label={'from agent'} value={'manually'} />
                  </Picker>
                </Item>
              </Col>
              <Col>
                <Item>
                  <Picker
                    mode="dropdown"
                    placeholder="Order status"
                    iosIcon={<Icon name="ios-arrow-down-outline" />}
                    style={{ width: undefined }}
                    selectedValue={report.order_status}
                    onValueChange={value => this.onChangeText('order_status', value)}
                  >
                    {orderTypes.map(type => (
                      <Picker.Item key={type.id} label={type.inventory_status} value={type.id} />
                    ))}
                  </Picker>
                </Item>
              </Col>
            </Row>
            <Row>
              <Col>
                <Item floatingLabel>
                  <Label>Order agent</Label>
                  <Input
                    type="Customer"
                    value={report.order_agent}
                    onChangeText={value => this.onChangeText('order_agent', value)}
                  />
                </Item>
              </Col>
            </Row>
            <Row>
              <Col>
                <Item floatingLabel>
                  <Label>Strain</Label>
                  <Input
                    type="strain"
                    value={report.strain}
                    onChangeText={value => this.onChangeText('strain', value)}
                  />
                </Item>
              </Col>
            </Row>
            <Row>
              <Col>
                <Item floatingLabel>
                  <Label>Customer</Label>
                  <Input
                    type="Customer"
                    value={report.customer}
                    onChangeText={value => this.onChangeText('customer', value)}
                  />
                </Item>
              </Col>
            </Row>
            {!reportCreating && (
              <Button full success onPress={this.generateReport}>
                <Text style={{ color: '#fff' }}>Generate Report</Text>
              </Button>
            )}
            {reportCreating && <Spinner />}
            {!_.isEmpty(generatedReport) && (
              <View>
                <Card>
                  <CardItem>
                    <Text>Total price:${generatedReport.total_price}</Text>
                  </CardItem>
                  <CardItem>
                    <Text>Total grams:{generatedReport.total_quantity}</Text>
                  </CardItem>
                  <CardItem>
                    <Text>Total positions in orders:{generatedReport.total_positions}</Text>
                  </CardItem>
                  <CardItem>
                    <Text>Reserved in shopping carts:${generatedReport.reserved_price} </Text>
                  </CardItem>
                  <CardItem>
                    <Text>Reserved grams:{generatedReport.reserved_quantity} </Text>
                  </CardItem>
                  <CardItem>
                    <Text>Reserved positions:{generatedReport.reserved_positions} </Text>
                  </CardItem>
                  <CardItem>
                    <Text>Purchased price:${generatedReport.purchased_price} </Text>
                  </CardItem>
                  <CardItem>
                    <Text>Purchased grams:{generatedReport.purchased_quantity} </Text>
                  </CardItem>
                  <CardItem>
                    <Text>Purchased positions:{generatedReport.purchased_positions} </Text>
                  </CardItem>
                </Card>
                <Card>
                  <View style={{ height: 50 }}>
                    <Row>
                      <Col size={1} style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Text>Status</Text>
                      </Col>

                      <Col size={2} style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Text>Agent</Text>
                      </Col>
                      <Col size={2} style={{ justifyContent: 'center' }}>
                        <Text>Customer</Text>
                      </Col>
                      <Col size={2} style={{ justifyContent: 'center' }}>
                        <Text>Date created</Text>
                      </Col>
                    </Row>
                  </View>
                  <List>
                    {generatedReport.reports.map(order => (
                      <ListItem onPress={() => this.goToDetailView(order.id)}>
                        <Row>
                          <Col size={1} style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Icon
                              info
                              type="FontAwesome"
                              name="circle"
                              style={{ color: order.status_color, fontSize: 15 }}
                            />
                          </Col>

                          <Col size={2} style={{ justifyContent: 'center' }}>
                            <Text>{order.order_agent}</Text>
                          </Col>
                          <Col size={2} style={{ justifyContent: 'center' }}>
                            <Text>{order.customer}</Text>
                          </Col>
                          <Col size={2} style={{ justifyContent: 'center' }}>
                            <Text>{order.date_created}</Text>
                          </Col>
                        </Row>
                      </ListItem>
                    ))}
                  </List>
                </Card>
              </View>
            )}
          </Content>
        </Container>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#d3da49',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 40
  },
  inputContainer: {
    marginTop: 20
  }
});
function mapStateToProps(state) {
  const { OrdersReducer, ReportReducer } = state;
  return { OrdersReducer, ReportReducer };
}
function mapDispatchToProps(dispatch) {
  return {
    getOrderTypes: () => {
      dispatch({ type: GET_ORDER_TYPES });
    },
    createOrder: report => {
      dispatch({ type: CREATE_REPORT, report });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ReportScreen);
