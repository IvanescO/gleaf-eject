import { reducer as reduxFormReducer } from 'redux-form';
import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import InventoryReducer from './InventoryReducer';
import OrdersReducer from './OrdersReducer';
import CartReducer from './CartReducer';
import ReportReducer from './ReportReducer';
import AdminEditing from './AdminEditing';
import SettingsReducer from './SettingsReducer';
import feCustomersReducer from './feCustomersReducer';
import MarketplaceReducer from './MarketplaceReducer';
import MarketplaceCartReducer from './MarketplaceCartReducer';

export default combineReducers({
  AuthReducer,
  InventoryReducer,
  AdminEditing,
  CartReducer,
  ReportReducer,
  feCustomersReducer,
  OrdersReducer,
  SettingsReducer,
  MarketplaceReducer,
  MarketplaceCartReducer,
  form: reduxFormReducer
});
