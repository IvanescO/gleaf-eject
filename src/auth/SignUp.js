import React from 'react';
import { StyleSheet, Text, View, Image, Alert } from 'react-native';
import connect from 'react-redux/lib/connect/connect';
import { Icon, Item, Label, Input, Content } from 'native-base';
import { Field, reduxForm } from 'redux-form';
import { colors } from '../theme';
import { Button } from '../components';
import { USER_REGISTRATION } from '../constants';
import AuthService from '../services/AuthService';

class SignIn extends React.Component {
  state = {
    username: '',
    password: '',
    email: '',
    is_adult: false,
    isAuthenticating: false //TODO: fix it
  };
  renderInput = ({
    input,
    isPassword,
    disabled,
    label,
    type,
    meta: { touched, error, warning }
  }) => {
    let hasError = false;
    if (error !== undefined) {
      hasError = true;
    }
    console.log(isPassword);
    return (
      <Item
        disabled={disabled}
        style={{
          margin: 10,
          borderBottomColor: touched && hasError ? colors.error : colors.primary
        }}
        error={hasError}
      >
        <Label style={{ color: '#a0a0a0' }}>{label}</Label>
        <Input
          secureTextEntry={isPassword}
          style={{ color: colors.white }}
          disabled={disabled}
          {...input}
        />
        {touched && hasError ? <Icon name="close-circle" /> : <Text />}
      </Item>
    );
  };
  render() {
    const { username, password, isAuthenticating, email, zip, is_adult } = this.state;
    const { AuthReducer, handleSubmit } = this.props;
    // const {} = this.props;
    return (
      <Content>
        <View style={styles.container}>
          <View style={{ alignItems: 'center' }}>
            <Image source={require('../../assets/gLeaf-logo-green.fw.png')} />
          </View>

          <View style={styles.inputContainer}>
            <Field label="Username" name="user_login" component={this.renderInput} />
            <Field label="E-mail" name="user_email" component={this.renderInput} />
            <Field label="Extra emails" name="extra_email" component={this.renderInput} />
            <Field isPassword label="Password" name="user_pass" component={this.renderInput} />
            <Field
              isPassword
              label="Confirm Password"
              name="user_pass_confirm"
              component={this.renderInput}
            />
            <Field label="Dispensary name" name="dispensary_name" component={this.renderInput} />
            <Field label="Address" name="address" component={this.renderInput} />
            <Field label="First Name" name="first_name" component={this.renderInput} />
            <Field label="Last Name" name="last_name" component={this.renderInput} />
            <Field label="Phone Number" name="phone_number" component={this.renderInput} />
            {/* <Input
            placeholder="User Name"
            type="username"
            onChangeText={this.onChangeText}
            value={username}
          />
          <Input placeholder="email" type="email" onChangeText={this.onChangeText} value={email} />
          <Input placeholder="zip" type="zip" onChangeText={this.onChangeText} value={zip} />
          <ListItem style={{ borderBottomColor: colors.primary }}>
            <CheckBox
              checked={is_adult}
              color={colors.primary}
              onPress={() => {
                this.setState({ is_adult: !this.state.is_adult });
              }}
            />
            <Body>
              <Text style={{ paddingLeft: 20, color: '#fff' }}>Over 21 years old?</Text>
            </Body>
          </ListItem> */}
          </View>
          <Button
            isLoading={isAuthenticating}
            title="Sign Up"
            onPress={handleSubmit(this.signUp)}
          />
          {AuthReducer.errors && <Text>{AuthReducer.errors}</Text>}
          {AuthReducer.successfulMessage && <Text>{AuthReducer.successfulMessage}</Text>}
        </View>
      </Content>
    );
  }
  signUp = async cerdentials => {
    // const { username, password, isAuthenticating, email, zip, is_adult } = this.state;
    const { reset } = this.props;
    // const resetForm(); = { username, email, zip, is_adult };
    try {
      const responce = await AuthService.Registration(cerdentials);
      // Toast.show({
      //   text: 'Registration Successful',
      //   type: 'success'
      // });
      Alert.alert(
        'Registration Successful',
        'Thank you for applying for membership to our site.We will review your details and send you an email letting you know whether your application has been successful or not.',
        [{ text: 'OK', onPress: () => {} }]
      );
      reset();
    } catch (e) {
      console.log(e);
    }

    // userRegistrate(cerdentials);
    // userAuthenticate(cerdentials);
  };
  onChangeText = (key, value) => {
    this.setState({
      [key]: value
    });
  };
}
const validate = values => {
  const errors = {};
  const emailRegexp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if (!values.user_login) {
    errors.user_login = 'Required';
  }
  if (!values.user_email) {
    errors.user_email = 'Required';
  } else if (!emailRegexp.test(values.user_email)) {
    errors.user_email = 'Invalid';
  }
  if (!values.extra_email) {
    errors.extra_email = 'Required';
  } else if (!emailRegexp.test(values.extra_email)) {
    errors.extra_email = 'Invalid';
  }
  if (!values.dispensary_name) {
    errors.dispensary_name = 'Required';
  }
  if (!values.address) {
    errors.address = 'Required';
  }
  if (!values.first_name) {
    errors.first_name = 'Required';
  }
  if (!values.last_name) {
    errors.last_name = 'Required';
  }
  if (!values.phone_number) {
    errors.phone_number = 'Required';
  }
  if (!values.user_pass) {
    errors.user_pass = 'Required';
  }
  if (!values.user_pass_confirm) {
    errors.user_pass_confirm = 'Required';
  } else if (values.user_pass !== values.user_pass_confirm) {
    errors.user_pass_confirm = 'Invalid';
  }
  return errors;
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    // alignItems: "center",
    justifyContent: 'center',
    paddingHorizontal: 40
  },
  inputContainer: {
    marginTop: 20
  }
});
const withReduxForm = reduxForm({
  form: 'RegistrationForm', // a unique identifier for this form
  validate
})(SignIn);
function mapStateToProps(state) {
  const { AuthReducer } = state;
  return { AuthReducer };
}
function mapDispatchToProps(dispatch) {
  return {
    userRegistrate: cerdentials => {
      dispatch({ type: USER_REGISTRATION, payload: { cerdentials } });
    }
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withReduxForm);
