/**
 * Watchers
 */
import { takeEvery, call, put, fork, all, throttle } from 'redux-saga/effects';
import { Toast } from 'native-base';
import InventoryService from '../services/InventoryService';
import * as cst from '../constants';

function* inventoryListRequesting(action) {
  try {
    console.log("inventoryListRequesting saga", action)

    const [inventoryList, types] = yield all([
      call(InventoryService.getInventory, action.params),
      call(InventoryService.getInventoryType)
    ]);

    // console.log(inventoryList);

    const { inventory, currentPage, total, itemsPerPage } = inventoryList.data;
    const inventoryTypes = types.data;
    // console.log("inventoryList saga", inventoryList.data);

    yield put({
      type: cst.INVENTORY_SUCCESSFUL,
      inventory,
      currentPage,
      itemsPerPage,
      total,
      inventoryTypes
    });
  } catch (e) {
    yield put({ type: cst.INVENTORY_FAILURE, errors: e.message });
  }
}

function* updateInventory(action) {
  try {
    const response = yield call(InventoryService.updateInventory, action.inventory);
    const inventory = response.data;
    console.log("updateInventory", response);

    yield put({ type: cst.UPDATE_INVETORY_SUCCESSFUL, inventory });
    Toast.show({
      text: 'Inventory updated successfully',
      type: 'success'
    });

  } catch (e) {
    console.log(e);
    yield put({ type: cst.UPDATE_INVETORY_FAILURE, errors: e.message });
    Toast.show({
      text: 'Inventory update failure',
      type: 'danger'
    });
  }
}

function* getSingleInventory(action) {
  try {
    const [singleInvenotry, types] = yield all([
      call(InventoryService.getSingleInventory, action.id),
      call(InventoryService.getInventoryType)
    ]);
    const data = singleInvenotry.data;
    const inventoryTypes = types.data;

    yield put({
      type: cst.SINGLE_INVENTORY_SUCCESSFUL,
      inventory: data, inventoryTypes
    });
  } catch (e) {
    yield put({
      type: cst.SINGLE_INVENTORY_FAILURE,
      errors: e.message
    });
  }
}

function* deleteInventory(action) {
  try {
    const response = yield call(InventoryService.delInventory, action.id);
    yield put({ type: cst.DEL_INVENTORY_SUCCESSFUL, id: action.id });
  } catch (e) {
    yield put({ type: cst.DEL_INVENTORY_FAILURE, errors: e.message });
  }
}

function* createInventory(action) {
  try {
    const response = yield call(InventoryService.createInventory, action.inventory);
    const inventory = response.data;
    console.log("createInventory resp", response);

    yield put({ type: cst.CREATE_INVENTORY_SUCCESSFUL, inventory });
      Toast.show({
        text: 'Inventory Created Successful',
        type: 'success'
      });
  } catch (e) {
    yield put({ type: cst.CREATE_INVENTORY_FAILURE, errors: e.message });
  }
}

function* getInventoryQbProducts(action) {
  try {
    const response = yield call(InventoryService.getQbProducts);
    const qbProducts = response.data;
    console.log("getInventoryQbProducts resp", response);

    yield put({ type: cst.INVENTORY_QBPRODUCTS_SUCCESSFUL, qbProducts });
  } catch (e) {
    yield put({
      type: cst.INVENTORY_QBPRODUCTS_FAILURE,
      errors: e.message
    });
  }

}

function* watchInventoryRequesting() {
  yield throttle(500, cst.INVENTORY_REQUESTING, inventoryListRequesting);
}
function* watchInventoryUpdating() {
  yield takeEvery(cst.UPDATE_INVETORY, updateInventory);
}
function* watchInventoryDeleting() {
  yield takeEvery(cst.DEL_INVENTORY, deleteInventory);
}
function* watchGetSingleInventory() {
  yield takeEvery(cst.SINGLE_INVENTORY, getSingleInventory);
}
function* watchCreateInventory() {
  yield takeEvery(cst.CREATE_INVENTORY, createInventory);
}
function* watchGetQbProducts() {
  yield takeEvery(cst.INVENTORY_QBPRODUCTS, getInventoryQbProducts);
}

export default function* () {
  yield [
    fork(watchInventoryRequesting),
    fork(watchInventoryUpdating),
    fork(watchGetSingleInventory),
    fork(watchInventoryDeleting),
    fork(watchCreateInventory),
    fork(watchGetQbProducts),
  ];
}
