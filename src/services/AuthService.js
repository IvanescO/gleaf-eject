import axios from 'axios';
import config from './config';
import { Toast } from 'native-base';

const http = axios.create({
  baseURL: config.apiUrl
});

http.interceptors.response.use(
  response => response,
  error => {
    Toast.show({
      text: error.response.data.error,
      type: 'danger',
      duration: 5000,
      buttonText: 'Close'
    });
    
    return Promise.reject(error.response.data.error);
  }
);

class AuthService {
  static async Login(cerdetials) {
    return http.post('login', cerdetials);
  }
  static async Registration(cerdetials) {
    console.log(cerdetials);

    return http.post('registration', cerdetials);
  }
  static async ResetPassword() {}
}
export default AuthService;
