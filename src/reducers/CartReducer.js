import * as cst from '../constants/index';

const initialState = {
  cart: [],
  addingSuccessMes: '',
  addingError: '',
  showModal: false,
  loadingAdding: false,
  loadingGetting: false,
  loadingSaveOrder: false,
  total: null,
};

function CartReducer(state = initialState, action) {
  switch (action.type) {
    case cst.ADMIN_ADD_CART_ITEM: {
      return {
        ...state,
        showModal: true,
        loadingAdding: true
      };
    }
    case cst.ADMIN_ADD_CART_ITEM_SUCCESSFUL: {
      return {
        ...state,
        loadingAdding: false,
        showModal: false,
        cart: action.cart,
      };
    }
    case cst.ADMIN_ADD_CART_ITEM_FAILURE: {
      return {
        ...state,
        loadingAdding: false,
        showModal: false,
      };
    }
    case cst.ADMIN_GET_CART: {
      return {
        ...state,
        loadingGetting: true
      };
    }
    case cst.ADMIN_GET_CART_SUCCESSFUL: {
      return { ...state, cart: action.cart, total: action.total, loadingGetting: false };
    }
    case cst.ADMIN_GET_CART_FAILURE: {
      return { ...state, loadingGetting: false };
    }
    case cst.ADMIN_DEL_CART_ITEM_SUCCESSFUL: {
      return { ...state, cart: action.cart, loadingGetting: false };
    }
    case cst.CREATE_ADMIN_ORDER: {
      return { ...state, loadingSaveOrder: true };
    }
    case cst.CREATE_ADMIN_ORDER_SUCCESSFUL: {
      const cart = action.cart || state.cart;
      return {
        ...state,
        cart,
        loadingSaveOrder: false
      };
    }
    case cst.CREATE_ADMIN_ORDER_FAILURE: {
      return {
        ...state,
        loadingSaveOrder: false
      };
    }
    case cst.ADMIN_UPDATE_CART_ITEM : {
      return { ...state, loadingAdding: true }
    }
    case cst.ADMIN_UPDATE_CART_ITEM_SUCCESSFUL : {
      return {
        ...state,
        cart: action.cart,
        total: action.total,
        loadingAdding: false
       }
    }
    case cst.ADMIN_UPDATE_CART_ITEM_FAILURE : {
      return {
        ...state,
        loadingAdding: false }
    }
    case cst.ADMIN_CLEAR_CART : {
      return { ...state, loadingSaveOrder: true }
    }
    case cst.ADMIN_CLEAR_CART_SUCCESSFUL : {
      return {
        ...state,
        cart: [],
        loadingSaveOrder: false,
        showModal: false
      }
    }
    case cst.ADMIN_CLEAR_CART_FAILURE : {
      return { ...state, errors: action.errors, loadingSaveOrder: false }
    }

    default:
      return state;
  }
}
export default CartReducer;
