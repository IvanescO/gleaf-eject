import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Field, reduxForm } from 'redux-form';
import { StyleSheet } from 'react-native';
import { Item, Container, Form, Title, Button } from 'native-base';
import RenderInput from './renderInput';
import validate from './formValidate';

class CustomerForm extends Component {

  onSubmit = values => {
    const { navigation: { onSubmitNewCustomer }, form } = this.props;
    const newCustomer = { ...values };
    onSubmitNewCustomer(newCustomer, form);
  };

  render() {
    const { handleSubmit } = this.props;
    const { customerMode } = this.props.navigation.state;
    console.log('CUSTOMER FORM', this.props);

    return (
      <Container style={styles.container}>
        <Form style={styles.formContainer} onSubmit={this.onSubmit}>
          <Field
            disabled={!customerMode}
            label="Username"
            name="user_login"
            component={RenderInput}
          />
          <Field
            disabled={!customerMode}
            label="E-mail Address"
            name="user_email"
            component={RenderInput}
          />
          <Field
            disabled={!customerMode}
            label="Extra emails"
            name="extra_email"
            component={RenderInput}
          />
          {customerMode && (
            <Fragment>
              <Field
                disabled={!customerMode}
                label="Password"
                name="user_pass"
                isPassword={true}
                component={RenderInput}
              />
              <Field
                disabled={!customerMode}
                label="Confirm Password"
                name="user_pass_confirm"
                isPassword={true}
                component={RenderInput}
              />
            </Fragment>
          )}

          <Field
            disabled={!customerMode}
            label="Dispensary name"
            name="dispensary_name"
            component={RenderInput}
          />
          <Field
            disabled={!customerMode}
            label="Address"
            name="address"
            component={RenderInput}
          />
          <Field
            disabled={!customerMode}
            label="First Name"
            name="first_name"
            component={RenderInput}
          />
          <Field
            disabled={!customerMode}
            label="Last Name"
            name="last_name"
            component={RenderInput}
          />
          <Field
            disabled={!customerMode}
            label="Phone Number"
            name="phone_number"
            component={RenderInput}
          />
          {customerMode && (
            <Item style={styles.btnContainer}>
              <Button style={styles.btn} block onPress={handleSubmit(this.onSubmit)}>
                <Title style={styles.btnText}>Create Agent</Title>
              </Button>
            </Item>
          )}
        </Form>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 'auto'
  },
  formContainer: {
    marginBottom: 20
  },
  btnContainer: {
    justifyContent: 'center',
    borderBottomWidth: 0,
    marginTop: 20
  },
  btn: {
    width: 200,
    backgroundColor: '#5cb85c'
  },
  btnText: {
    fontSize: 16
  },
});

const mapStateToProps = state => {
  // console.log(state);
  const customer = state.feCustomersReducer.feCustomer;

  if (!customer) return {};
  return {
    initialValues: {
      ...customer,
      roles: customer.roles.toString()
    }
  };
};

export default compose(
  connect(mapStateToProps),
  reduxForm({
    form: 'feCustomerEdit', // a unique identifier for this form
    validate
  })
)(CustomerForm);
