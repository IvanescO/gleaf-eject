import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, ListView, RefreshControl, Modal } from 'react-native';
import { Icon, Button, Text, List, ListItem, Col, Row, Container, Spinner } from 'native-base';
import { FE_CUSTOMER_LIST, FE_CUSTOMER_STORE } from '../../constants/index';
import Pagination from '../../components/Pagination';

class feCustomersList extends React.Component {
  constructor(props) {
    super(props);
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
  }
  state = { params: {} };
  getPaginatedList = pageNum => {
    console.log(pageNum);
    const { params } = this.state;
    this.props.getCustomersList({ ...params, page: pageNum });
  };
  goToPage = num => {
    // this.setState({ currentPage: num });
    this.getPaginatedList(num);
  };
  storeFeCustomer = data => {
    const { navigation, storeFeCustomer, customerMode } = this.props;
    storeFeCustomer(data);
    // console.log("DATA", data)
    // console.log("customerMode", customerMode)
    navigation.navigate('customer', { user: data, customerMode });
  };

  render() {
    const { feCustomers, currentPage, totalPage, feCustomerLoading } = this.props;
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    console.log('LIST PROPS', this.props);

    return (
      <Container>
        {feCustomers && (
          <List
            renderHeader={() => (
              <Row style={{ marginTop: 10, height: 25 }}>
                <Col size={3} style={styles.green}>
                  <Row>
                    <Col size={1} style={styles.red} />
                    <Col size={9} style={styles.green}>
                      <Text> Login </Text>
                    </Col>
                  </Row>
                </Col>

                <Col size={3} style={styles.red}>
                  <Text> Name </Text>
                </Col>

                <Col size={4} style={styles.blue}>
                  <Text> Email </Text>
                </Col>
              </Row>
            )}
            leftOpenValue={75}
            rightOpenValue={-75}
            dataSource={ds.cloneWithRows(feCustomers)}
            renderRow={data => (
              <ListItem
                style={{
                  height: 50,
                  paddingRight: 0,
                  justifyContent: 'flex-start',
                  flexDirection: 'column'
                }}
                onPress={() => {
                  this.storeFeCustomer(data);
                }}
              >
                <Row>
                  <Col size={3}>
                    <Row>
                      <Col size={1} />
                      <Col size={8}>
                        <Text style={styles.rowText}> {data.user_login} </Text>
                      </Col>
                    </Row>
                  </Col>

                  <Col size={3}>
                    <Text style={styles.rowText}> {data.first_name} </Text>
                  </Col>

                  <Col size={4}>
                    <Text style={styles.rowText}> {data.user_email} </Text>
                  </Col>
                </Row>
              </ListItem>
            )}
            renderLeftHiddenRow={data => (
              <Button full onPress={() => alert(data)}>
                <Icon active name="information-circle" />
              </Button>
            )}
            renderRightHiddenRow={(data, secId, rowId, rowMap) => (
              <Button full danger onPress={_ => this.deleteRow(secId, rowId, rowMap)}>
                <Icon active name="trash" />
              </Button>
            )}
            disableLeftSwipe
            disableRightSwipe
          />
        )}
        {Boolean(feCustomerLoading) && <Spinner style={{ height: 300 }} />}
        {!feCustomerLoading && (
          <Pagination currentPage={currentPage} totalPage={totalPage} pageHandler={this.goToPage} />
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  rowText: {
    alignSelf: 'flex-start',
    fontSize: 12
  }
});

export default feCustomersList;
