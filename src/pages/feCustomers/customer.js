import React from 'react';
import { Content } from 'native-base';
import CustomerForm from './customerForm';

const CustomerComponent = (props) => (
  <Content>
    <CustomerForm {...props} />
  </Content>
);

export default CustomerComponent;
