import { fork } from 'redux-saga/effects';
import AuthSaga from './AuthSaga';
import InventorySaga from './InventorySaga';
import OrdersSage from './OrdersSaga';
import CartSaga from './CartSaga';
import ReportSaga from './ReportSaga';
import SettingsSaga from './SettingsSaga';
import FeCustomerSaga from './FeCustomerSaga';
import MarketPlaceSaga from './MarketPlaceSaga';

export default function* rootSaga() {
  yield [
    fork(AuthSaga),
    fork(FeCustomerSaga),
    fork(InventorySaga),
    fork(OrdersSage),
    fork(CartSaga),
    fork(ReportSaga),
    fork(SettingsSaga),
    fork(MarketPlaceSaga)
  ];
}
