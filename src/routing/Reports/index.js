import StackNavigator from 'react-navigation/src/navigators/StackNavigator';
import Report from '../../pages/Reports';
import Order from '../../pages/OrdersScreens/Order';

const ReportRouting = StackNavigator(
  {
    Report: { screen: Report },
    Order: { screen: Order }
  },
  {
    headerMode: 'none',
    navigationOptions: {
      drawerLockMode: 'locked-closed',
      headerVisible: false
    }
  }
);

export default ReportRouting;
