import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {
  Picker,
  Content,
  Form,
  Item,
  Label,
  Input,
  Button,
  Spinner,
  CheckBox,
  ListItem,
  Body,
  Icon,
  Left,
  Right,
  Radio,
  Row,
  Col
} from 'native-base';
import _ from 'lodash';
import moment from 'moment';
import connect from 'react-redux/lib/connect/connect';
import { UPDATE_INVETORY, SINGLE_INVENTORY, CREATE_INVENTORY, INVENTORY_QBPRODUCTS } from '../../constants';
import Layout from '../../components/Layout';
import InventoryService from '../../services/InventoryService';
import arrify from 'arrify';
// import { colors } from '../../theme';

class Inventory extends React.Component {
  state = {
    loading: false,
    selectedInventory: { date_created: moment().format('L'), qb_product_id: '155' },
    harvest: [...Array(255).keys()].map(e => e.toString()),
      single_loading: true,
  };

  componentWillMount = async () => {
    const { getSingleInventory, getQbProducts } = this.props;
    let { params } = this.props.navigation.state;

    if (!params) {
      params = { mode: 'create' };
      this.setState({ mode: params.mode });
      return;
    }

    this.setState({ mode: params.mode });

    getQbProducts();
    getSingleInventory(params.id);
  };

  componentWillReceiveProps(nextProps) {
    console.log("nextProps", nextProps)
    const { selectedInventory } = nextProps.InventoryReducer;
    if (this.state.mode === 'create') {
        if(!Object.keys(selectedInventory).length)
        this.setState({ selectedInventory:{} });
    };
    this.setState({ selectedInventory });
  }

  onChangeText = (key, value) => {
    const { selectedInventory } = this.state;
    selectedInventory[key] = value;
    this.setState({
      selectedInventory
    });
  };

  updateInventory = () => {
    const { selectedInventory } = this.state;
    const { updateInventory } = this.props;
    updateInventory(selectedInventory);
  };

  createInventory = () => {
    const { selectedInventory } = this.state;
    const { createInventory } = this.props;
    console.log(selectedInventory);
    createInventory(selectedInventory);
    // this.setState({
    //   selectedInventory: {}
    // });
  };

  getTypeNameById = id => {
    const { InventoryReducer } = this.props;
    const { inventoryTypes } = InventoryReducer;
    const elem = inventoryTypes.find(type => type.id === id);
    return elem ? elem.inventory_type : '';
  };

  render() {
    const { mode, selectedInventory, harvest } = this.state;
    const { InventoryReducer } = this.props;
    const { loading, single_loading, errors, message, inventoryTypes, qbProducts } = InventoryReducer;

    // console.log(selectedInventory);

    let TITLE;
    if (mode === 'create') TITLE = 'New Inventory';
    else TITLE = 'Inventory info';

    console.log("PROPS", this.props);

    return (
      <Layout title={TITLE} drawerOpen={() => this.props.navigation.navigate('DrawerOpen')}>
        <Content >

          {Boolean(single_loading) && <Spinner style={{ height: 450 }} />}

          {Boolean(!single_loading) && (
            <Form>
              {Boolean(mode === 'view') && (
                <Item disabled floatingLabel>
                  <Label>ID</Label>
                  <Input
                    type="id"
                    disabled
                    value={selectedInventory.id}
                    onChangeText={value => this.onChangeText('id', value)}
                  />
                </Item>
              )}
              <Item disabled={mode === 'view'} floatingLabel>
                <Label>THC</Label>
                <Input
                  disabled={mode === 'view'}
                  value={selectedInventory.thc}
                  onChangeText={value => this.onChangeText('thc', value)}
                />
              </Item>
              <Item disabled={mode === 'view'} floatingLabel>
                <Label>Batch</Label>
                <Input
                  disabled={mode === 'view'}
                  value={selectedInventory.batch}
                  onChangeText={value => this.onChangeText('batch', value)}
                />
              </Item>
              <Item disabled={mode === 'view'} floatingLabel>
                <Label>Package</Label>
                <Input
                  disabled={mode === 'view'}
                  value={selectedInventory.package}
                  onChangeText={value => this.onChangeText('package', value)}
                />
              </Item>

              {/* <Item disabled={mode === 'view'} floatingLabel>
                <Label>Strain</Label>
                <Input
                  disabled={mode === 'view'}
                  value={selectedInventory.strain}
                  onChangeText={value => this.onChangeText('strain', value)}
                />
              </Item> */}

              <Item disabled={mode === 'view'} floatingLabel>
                <Label>Warehouse quantity</Label>
                <Input
                  disabled={mode === 'view'}
                  value={selectedInventory.warehouse_quantity}
                  onChangeText={value => this.onChangeText('warehouse_quantity', value)}
                />
              </Item>
              {Boolean(mode === 'view') && (
                <Item disabled floatingLabel>
                  <Label>Agent</Label>
                  <Input
                    disabled
                    value={selectedInventory.agent}
                    onChangeText={value => this.onChangeText('agent', value)}
                  />
                </Item>
              )}
              <Item disabled={mode === 'view'} floatingLabel last>
                <Label>Imarket price</Label>
                <Input
                  disabled={mode === 'view'}
                  value={selectedInventory.imarket_price}
                  onChangeText={value => this.onChangeText('imarket_price', value)}
                />
              </Item>
              <Item disabled={mode === 'view'} floatingLabel last>
                <Label>CBD</Label>
                <Input
                  disabled={mode === 'view'}
                  value={selectedInventory.cbd}
                  onChangeText={cbd => this.onChangeText('cbd', cbd)}
                />
              </Item>
              <Item disabled={mode === 'view'} floatingLabel last>
                <Label>Date created</Label>
                <Input
                  disabled={mode === 'view'}
                  value={selectedInventory.date_created}
                  onChangeText={value => this.onChangeText('date_created', value)}
                />
              </Item>
              {Boolean(mode === 'edit' || mode === 'create') && (
                <Row>
                  <Col
                    size={15}
                    style={{ justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 10 }}
                  >
                    <Text style={{ fontSize: 15 }}>Strain</Text>
                  </Col>
                  <Col size={80}>
                    <Item picker>
                      <Picker
                        mode="dropdown"
                        enabled
                        placeholder="inventory type"
                        iosIcon={<Icon name="ios-arrow-down-outline" />}
                        style={{ width: undefined }}
                        selectedValue={selectedInventory.qb_product_id}
                        onValueChange={value => this.onChangeText('qb_product_id', value)}
                      >
                        {qbProducts.map(product => (
                          <Picker.Item
                            key={product.qb_product_id}
                            label={product.qb_product_name}
                            value={product.qb_product_id}
                          />
                        ))}
                      </Picker>
                    </Item>
                  </Col>
                </Row>
              )}
              {Boolean(mode === 'edit' || mode === 'create') && (
                <Row>
                  <Col
                    size={15}
                    style={{ justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 10 }}
                  >
                    <Text style={{ fontSize: 15 }}>Type</Text>
                  </Col>
                  <Col size={80}>
                    <Item picker>
                      <Picker
                        mode="dropdown"
                        enabled
                        placeholder="inventory type"
                        iosIcon={<Icon name="ios-arrow-down-outline" />}
                        style={{ width: undefined }}
                        selectedValue={selectedInventory.inventory_type_id}
                        onValueChange={value => this.onChangeText('inventory_type_id', value)}
                      >
                        {inventoryTypes.map(type => (
                          <Picker.Item key={type.id} label={type.inventory_type} value={type.id} />
                        ))}
                      </Picker>
                    </Item>
                  </Col>
                </Row>
              )}
              {Boolean(mode === 'edit' || mode === 'create') && (
                <Row>
                  <Col
                    size={15}
                    style={{ justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 10 }}
                  >
                    <Text style={{ fontSize: 15 }}>Harvest</Text>
                  </Col>
                  <Col size={80}>
                    <Item picker>
                      <Picker
                        mode="dropdown"
                        enabled
                        placeholder="Harvest"
                        iosIcon={<Icon name="ios-arrow-down-outline" />}
                        style={{ width: undefined }}
                        selectedValue={selectedInventory.harvest}
                        onValueChange={value => this.onChangeText('harvest', value)}
                      >
                        {harvest.map(type => <Picker.Item key={type} label={type} value={type} />)}
                      </Picker>
                    </Item>
                  </Col>
                </Row>
              )}
              {Boolean(mode === 'view') && (
                <Item disabled={mode === 'view'} floatingLabel last>
                  <Label>Strain</Label>
                  <Input disabled={mode === 'view'} value={selectedInventory.strain} />
                </Item>
              )}
              {Boolean(mode === 'view') && (
                <Item disabled={mode === 'view'} floatingLabel last>
                  <Label>Inventory type</Label>
                  <Input
                    disabled={mode === 'view'}
                    value={this.getTypeNameById(selectedInventory.inventory_type_id)}
                  />
                </Item>
              )}
              {Boolean(mode === 'view') && (
                <Item disabled={mode === 'view'} floatingLabel last>
                  <Label>Harvest</Label>
                  <Input disabled={mode === 'view'} value={selectedInventory.harvest} />
                </Item>
              )}
              <ListItem disabled={mode === 'view'} floatingLabel last>
                <Body>
                  <Text>Imarket visible</Text>
                </Body>
                <CheckBox
                  checked={selectedInventory.imarket_visible}
                  color={'green'}
                  onPress={value => {
                    if (mode === 'view') return;
                    console.log(value);
                    this.onChangeText('imarket_visible', !selectedInventory.imarket_visible);
                  }}
                />
              </ListItem>
              {selectedInventory.imarket_visible && (
                <View>
                  <ListItem>
                    <Left>
                      <Text>Display to all Customers</Text>
                    </Left>
                    <Right>
                      <Radio
                        onPress={() => {
                          if (mode === 'view') return;
                          this.onChangeText('show_to_all', true);
                        }}
                        selectedColor={'#5cb85c'}
                        selected={selectedInventory.show_to_all}
                      />
                    </Right>
                  </ListItem>
                  <ListItem>
                    <Left>
                      <Text>Display to Advanced Customers only</Text>
                    </Left>
                    <Right>
                      <Radio
                        onPress={() => {
                          if (mode === 'view') return;
                          this.onChangeText('show_to_all', false);
                        }}
                        selected={!selectedInventory.show_to_all}
                        selectedColor={'#5cb85c'}
                      />
                    </Right>
                  </ListItem>
                </View>
              )}

              {/* {Boolean(errors) && (
                <View style={{ alignItems: 'center' }}>
                  <Text style={{ color: colors.error }}>{errors}</Text>
                </View>
              )}
              {Boolean(message) && (
                <View style={{ alignItems: 'center' }}>
                  <Text style={{ color: colors.primary }}>{message}</Text>
                </View>
              )} */}

              {Boolean(!loading) &&
                Boolean(mode === 'edit') && (
                  <Button style={{ marginTop: 10 }} success full onPress={this.updateInventory}>
                    <Text style={{ color: '#fff' }}> Save </Text>
                  </Button>
                )}
              {Boolean(!loading) &&
                Boolean(mode === 'create') && (
                  <Button style={{ marginTop: 10 }} success full onPress={this.createInventory}>
                    <Text style={{ color: '#fff' }}> Save </Text>
                  </Button>
                )}
              {Boolean(loading) && <Spinner />}
            </Form>
          )}

        </Content>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#d3da49',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 40,
  }
});

function mapStateToProps(state) {
  const { InventoryReducer } = state;
  return { InventoryReducer };
}
function mapDispatchToProps(dispatch) {
  return {
    updateInventory: inventory => {
      dispatch({ type: UPDATE_INVETORY, inventory });
    },
    createInventory: inventory => {
      dispatch({ type: CREATE_INVENTORY, inventory });
    },
    getSingleInventory: id => {
      dispatch({ type: SINGLE_INVENTORY, id });
    },
    getQbProducts: () => {
      dispatch({ type: INVENTORY_QBPRODUCTS });
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Inventory);
