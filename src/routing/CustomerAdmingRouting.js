import DrawerNavigator from 'react-navigation/src/navigators/DrawerNavigator';
import InventoryRouting from './Inventory';
import OrderRouting from './Orders';
import feCustomersRouting from './feCustomers';
import Settings from '../pages/Settings/Settings';

const UserRouter = DrawerNavigator(
  {
    Customers: { screen: feCustomersRouting },
    MarketPlace: { screen: InventoryRouting },
    Orders: { screen: OrderRouting },
    Settings: { screen: Settings }
  }
  /*{
      headerMode: 'none',
      mode: 'modal',
      navigationOptions: {
        gesturesEnabled: false,
      }
    }*/
);
export default UserRouter;
