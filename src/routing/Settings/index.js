import StackNavigator from 'react-navigation/src/navigators/StackNavigator';
import Settings from '../../pages/Settings/Settings';

const SettingsRouting = StackNavigator(
  {
    Settings: { screen: Settings },
  },
  {
    initialRouteName: 'Settings',
    headerMode: 'none',

    navigationOptions: {
      drawerLockMode: 'locked-closed',
      headerVisible: false
    }
  }
);

export default SettingsRouting;
