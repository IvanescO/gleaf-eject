/**
 * Watchers
 */
import { takeEvery, call, put, fork } from 'redux-saga/effects';
import MarketCustomerService from '../services/MarketCustomerService';
import {
  FE_CUSTOMER_LIST,
  FE_CUSTOMER_LIST_SUCCESSFUL,
  FE_CUSTOMER_LIST_FAILURE,
  CREATE_FE_CUSTOMER,
  CREATE_FE_CUSTOMER_SUCCESSFUL,
  CREATE_FE_CUSTOMER_FAILURE
} from '../constants/index';
import { reset } from 'redux-form';
import { Toast } from 'native-base';

function* getCustomerList(action) {
  try {
    const response = yield call(MarketCustomerService.getCustomersList, action.params);
    console.log('RESPONSE getCustomerList', response);
    const { currentPage, itemsPerPage, total, users } = response.data;

    yield put({
      type: FE_CUSTOMER_LIST_SUCCESSFUL,
      currentPage,
      itemsPerPage,
      total,
      feCustomers: users
    });
  } catch (error) {
    yield put({ type: FE_CUSTOMER_LIST_FAILURE, errors: error });
  }
}

function* createCustomer(action) {
  try {
    const { newCustomer, form } = action;
    const request = yield call(MarketCustomerService.createCustomer, newCustomer);
    const payload = request.data;

    yield put({ type: CREATE_FE_CUSTOMER_SUCCESSFUL, payload });
    yield put(reset(form)); // reset form

    Toast.show({
      text: 'Customer agent was created successfully',
      type: 'success'
    });
  } catch (error) {
    yield put({ type: CREATE_FE_CUSTOMER_FAILURE, errors: error });
  }
}

function* watchGetCustomerList() {
  yield takeEvery(FE_CUSTOMER_LIST, getCustomerList);
}

function* watchCreateCustomer() {
  yield takeEvery(CREATE_FE_CUSTOMER, createCustomer);
}

export default function* () {
  yield [fork(watchGetCustomerList), fork(watchCreateCustomer)];
}
