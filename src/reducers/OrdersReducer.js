import _ from 'lodash';
import {
  ORDERS_REQUESTING,
  ORDERS_SUCCESSFUL,
  ORDERS_FAILURE,
  SINGLE_ORDER,
  SINGLE_ORDER_FAILURE,
  SINGLE_ORDER_SUCCESSFUL,
  DEL_ORDER_SUCCESSFUL,
  DEL_ORDER_FAILURE,
  UPDATE_PAYMENT_DETAIL,
  UPDATE_PAYMENT_DETAIL_SUCCESSFUL,
  UPDATE_PAYMENT_DETAIL_FAILURE,
  GET_ORDER_TYPES_SUCCESSFUL,
  UPDATE_ORDER_SUCCESSFUL
} from '../constants';

const initialState = {
  orderListLoading: false,
  singleLoading: false,
  orders: [],
    currentPage:0, itemsPerPage:0, total:0,
  orderTypes: [],
  singleOrder: { order_items: [] }
};

function OrdersReducer(state = initialState, action) {
  switch (action.type) {
    case ORDERS_REQUESTING: {
      return { ...state, orderListLoading: true, orders: [], listError: '' };
    }
    case ORDERS_SUCCESSFUL: {
      const { currentPage, itemsPerPage, orders, total } = action;
      return { ...state, currentPage, itemsPerPage, orders, total, orderListLoading: false };
    }
    case ORDERS_FAILURE: {
      return { ...state, orders: action.orders, listError: 'Error :(', orderListLoading: false };
    }
    case SINGLE_ORDER: {
      return { ...state, singleLoading: true, singleError: '' };
    }
    case SINGLE_ORDER_SUCCESSFUL: {
      return {
        ...state,
        singleLoading: false,
        singleOrder: action.singleOrder,
        paymentTypes: action.paymentTypes,
        singleError: ''
      };
    }
    case UPDATE_ORDER_SUCCESSFUL: {
      const orders = state.orders;
      const index = _.findIndex(orders, elem => elem.id === action.order.id);
      orders[index] = action.order;
      console.log();
      return {
        ...state,
        updatingMes: 'Success',
        orders
      };
    }
    case SINGLE_ORDER_FAILURE: {
      return { ...state, singleLoading: false, singleError: action.errors };
    }
    case DEL_ORDER_SUCCESSFUL: {
      const orders = state.orders;
      _.remove(orders, elem => elem.id === action.id);
      return { ...state, orders };
    }
    case UPDATE_PAYMENT_DETAIL: {
      return { ...state, detailUpdating: true, updatingError: '' };
    }
    case UPDATE_PAYMENT_DETAIL_SUCCESSFUL: {
      const orders = state.orders;
      const index = _.findIndex(orders, elem => elem.id === action.orderId);
      console.log(index);
      orders[index].payment_details = action.detail;
      return { ...state, detailUpdating: false, updatingError: '', orders };
    }
    case UPDATE_PAYMENT_DETAIL_FAILURE: {
      return { ...state, detailUpdating: false, updatingError: action.errors };
    }
    case GET_ORDER_TYPES_SUCCESSFUL: {
      return { ...state, orderTypes: action.orderTypes };
    }
    default:
      return state;
  }
}
export default OrdersReducer;
