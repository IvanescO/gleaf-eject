import AdminRouter from './AdminRouting';
import CustomerAdminRouter from './CustomerAdmingRouting';
import CustomerAgentRouter from './CustomerAgentRouting';

export default { AdminRouter, CustomerAdminRouter, CustomerAgentRouter };
