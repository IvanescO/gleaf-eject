import React from 'react';
import { StyleSheet, ListView, RefreshControl, Modal } from 'react-native';
import {
  Container,
  Header,
  Item,
  Input,
  Icon,
  Button,
  Text,
  List,
  ListItem,
  Form,
  Label,
  View,
  Col,
  Row,
  Left,
  Spinner,
  Right,
  Picker,
  Title,
  Body,
  Toast
} from 'native-base';
import _ from 'lodash';
import moment from 'moment';
import connect from 'react-redux/lib/connect/connect';
import * as cst from '../../constants';
import Layout from '../../components/Layout';
import Pagination from '../../components/Pagination';
import { colors } from '../../theme';

class MarketPlace extends React.Component {
  state = {
    isRefreshing: false,
    cart: [],
    cartItem: {},
    inventory: [],
    timer: null,
    TimeToExpire: { minutes: 0, seconds: 0 },
    params: { search: '', type: { id: 'all', inventory_type: 'sad' }, hide_empty: false },
    currentPage: 1,
    totalPage: 1,
    itemsPerPage: 1
  };

  ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

  componentWillMount() {
    const { params } = this.state;
    const { getInventoryList, getMarketPlaceCart } = this.props;

    getInventoryList(params);
    getMarketPlaceCart();
  }

  componentWillReceiveProps(nextProps) {
    console.log('nextProps', nextProps);
    const { MarketplaceReducer, MarketplaceCartReducer } = nextProps;
    const { inventory, total, itemsPerPage } = MarketplaceReducer;
    const {
      cart,
      loadingAdding,
      timestamp
    } = MarketplaceCartReducer;

    clearInterval(this.state.timer);
    this.setState({ timer: null }, () => {
      this.timerHandler();
    });

    this.setState({
      cart,
      loadingAdding,
      inventory,
      timestamp,
      itemsPerPage,
      totalPage: Math.ceil(total / itemsPerPage)
    });
  }

  refreshInventoryList = () => {
    const { params } = this.state;
    const { getMarketPlaceCart, getInventoryList } = this.props;

    getInventoryList(params);
    getMarketPlaceCart();
  };

  timerHandler = () => {
    let { timer } = this.state;
    const { MarketplaceCartReducer, deleteCartTimeOut } = this.props;
    const { expireTime, timestamp } = MarketplaceCartReducer;
    console.log(timestamp, this.state.timestamp);

    if (!timestamp) {
      this.setState({ TimeToExpire: { minutes: 0, seconds: 0 } });
      return;
    }
    const eventTime = timestamp + expireTime;
    const currentTime = moment().unix();
    const diffTime = eventTime - currentTime;
    let duration = moment.duration(diffTime * 1000, 'milliseconds');
    const interval = 1000;

    console.log(this.state.timer);
    console.log(timer, duration, diffTime);
    if (!timer && duration && diffTime > 0) {
      timer = setInterval(() => {
        duration = moment.duration(duration - interval, 'milliseconds');
        const minutes = duration.minutes();
        const seconds = duration.seconds();
        if (minutes === 0 && seconds === 0) {
          clearInterval(timer);
          deleteCartTimeOut();
          return this.setState({ timer: null });
        }
        //console.log(duration);
        if (!isNaN(minutes) && !isNaN(seconds)) {
          this.setState({ TimeToExpire: { minutes, seconds } });
        }
      }, interval);
      this.setState({ timer });
    }
  };

  getTypeNameById = id => {
    const { MarketplaceReducer: { inventoryTypes } } = this.props;
    const elem = inventoryTypes.find(type => type.id === id);
    return elem ? elem.inventory_type : '';
  };

  goToCart = () => {
    this.props.navigation.navigate('MarketplaceCart');
  };

  searchByQuery = () => {
    const { params } = this.state;
    const { getInventoryList } = this.props;
    getInventoryList(params);
  };

  searchByType = id => {
    const { getInventoryList } = this.props;
    this.setState({ params: { ...this.state.params, type: { id } } }, () => {
      const { params } = this.state;
      getInventoryList(params);
    });
  };

  emptyToggle = () => {
    const {
      params: { hide_empty }
    } = this.state;
    const { getInventoryList } = this.props;

    this.setState({ params: { ...this.state.params, hide_empty: !hide_empty } }, () => {
      const { params } = this.state;
      getInventoryList(params);
    });
  };

  goToPage = num => {
    this.setState({ currentPage: num });
    this.getPaginatedInventoryList(num);
  };

  getPaginatedInventoryList = pageNum => {
    const { getInventoryList } = this.props;
    const { params } = this.state;
    getInventoryList({ ...params, page: pageNum });
  };

  addToCart = data => {
    const { marketAddItemToCart } = this.props;
    const cartItem = { inventory_id: data.id };
    marketAddItemToCart(cartItem);

    this.setState({ cartItem });
  };

  render() {
    const {
      cart,
      loadingAdding,
      params,
      inventory,
      totalPage,
      TimeToExpire,
      cartItem,
      isRefreshing,
      currentPage
    } = this.state;

    const {
      navigation: { navigate },
      MarketplaceReducer: { requestingInventory, inventoryTypes }
    } = this.props;

    const { search, type, hide_empty } = params;
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    const newInventoryTypes = [...inventoryTypes];
    newInventoryTypes.push({ id: 'all', inventory_type: 'All' });
    const TITLE = 'Marketplace';

    console.log('PROPS', this.props);

    return (
      <Layout title={TITLE} drawerOpen={() => navigate('DrawerOpen')}>
        <Header style={styles.header} rounded>
          <Left>
            <Button iconLeft rounded small success onPress={this.goToCart}>
              <Icon type="FontAwesome" name="shopping-cart" style={{ color: '#fff' }} />
              <Text>{cart.length}</Text>
            </Button>
          </Left>
          <Left style={{ marginLeft: 10 }}>
            <Button iconLeft rounded small success>
              <Text>{`${TimeToExpire.minutes}:${TimeToExpire.seconds}`}</Text>
            </Button>
          </Left>
          <Right>
            <Item style={{ flex: 1 }}>
              <Picker
                mode="dropdown"
                placeholder="inventory type"
                // iosIcon={<Icon name="ios-arrow-down-outline" />}
                style={{ width: undefined }}
                selectedValue={type.id}
                onValueChange={value => this.searchByType(value)}
              >
                {newInventoryTypes.map(invType => (
                  <Picker.Item key={invType.id} label={invType.inventory_type} value={invType.id} />
                ))}
              </Picker>
            </Item>
          </Right>
        </Header>
        <Header style={styles.header} searchBar rounded>
          <Item style={{ flex: 0.75 }}>
            <Icon name="ios-search" />
            <Input
              value={search}
              placeholder="Search"
              onChangeText={value => {
                console.log(value);
                this.setState({ params: { ...params, search: value } });
              }}
            />
          </Item>
          <Item style={{ flex: 0.25, alignContent: 'center' }}>
            <Button 
            style={{ flex: 1, justifyContent: 'center' }}
            success small onPress={this.searchByQuery}>
              <Text style={{ color: colors.white, fontSize: 12 }}>Search</Text>
            </Button>
          </Item>
        </Header>
        <View style={{ height: 40 }}>
          <Row style={styles.centerElement}>
            <Col size={1} style={styles.centerElement} />
            <Col size={1}>
              <Text>Strain</Text>
            </Col>
            <Col size={1} style={styles.centerElement} />
            <Col size={1}>
              <Text>THC</Text>
            </Col>
            <Col size={3}>
              <Text>Type</Text>
            </Col>
          </Row>
        </View>

        {Boolean(!requestingInventory) &&
          Boolean(inventory.length) && (
            <Container>
              {loadingAdding ? (
                <Spinner style={{ height: 300 }} />
              ) : (
                <List
                  swipeToOpenPercent={50}
                  refreshControl={
                    <RefreshControl
                      refreshing={isRefreshing}
                      onRefresh={this.refreshInventoryList}
                    />
                  }
                  dataSource={this.ds.cloneWithRows(inventory)}
                  renderRow={data => (
                    <ListItem
                      style={{ height: 40 }}
                      onPress={() => {
                        navigate('MarketplaceItem', {
                          id: data.id,
                          inventory: data,
                          mode: 'view'
                        });
                      }}
                    >
                      <Row>
                        <Col size={1} style={styles.centerElement}>
                          {data.star === 'all' && (
                            <Text>
                              <Icon type="FontAwesome" name="star" style={styles.itemText} />
                            </Text>
                          )}
                          {data.star === 'advanced' && (
                            <Text>
                              <Icon name="ios-star-outline" style={styles.itemText} />
                            </Text>
                          )}
                        </Col>
                        <Col size={2}>
                          <Text style={{ fontSize: 11 }}>{data.strain}</Text>
                        </Col>
                        <Col size={1}>
                          <Text style={{ fontSize: 11 }}> {data.thc} </Text>
                        </Col>
                        <Col size={2}>
                          <Text style={{ fontSize: 11 }}>
                            {this.getTypeNameById(data.type_id || data.inventory_type_id)}
                          </Text>
                        </Col>
                        <Col size={1} style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
                          <Button small success onPress={_ => this.addToCart(data)}>
                            <Icon
                              style={{ fontSize: 15 }}
                              active
                              type="FontAwesome"
                              name="cart-plus"
                            />
                          </Button>
                        </Col>
                      </Row>
                    </ListItem>
                  )}
                  renderRightHiddenRow={() => {}}
                  leftOpenValue={75}
                  rightOpenValue={-175}
                  disableRightSwipe
                  disableLeftSwipe
                />
              )}
            </Container>
          )}
        {Boolean(requestingInventory) && <Spinner style={{ height: 300 }} />}
        {Boolean(!requestingInventory) &&
          Boolean(inventory.length) && (
            <Pagination
              currentPage={currentPage}
              totalPage={totalPage}
              pageHandler={this.goToPage}
            />
          )}
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#f6f6f6',
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(255, 255, 255, 0.4)'
  },
  centerElement: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  itemText: {
    fontSize: 15,
    color: colors.primary,
    lineHeight: 20
  }
});

function mapStateToProps(state) {
  const { MarketplaceReducer, MarketplaceCartReducer } = state;
  return { MarketplaceReducer, MarketplaceCartReducer };
}

function mapDispatchToProps(dispatch) {
  return {
    getInventoryList: params => {
      dispatch({ type: cst.MARKETPLACE_REQUESTING, params });
    },
    getMarketPlaceCart: () => {
      dispatch({ type: cst.MARKETPLACE_GET_CART });
    },
    deleteCartTimeOut: () => {
      dispatch({ type: cst.MARKETPLACE_DELL_CART });
    },
    marketAddItemToCart: item => {
      dispatch({ type: cst.MARKETPLACE_ADD_CART_ITEM, item });
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MarketPlace);
