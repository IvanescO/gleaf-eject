import React from 'react';
import { Content } from 'native-base';
import CustomerForm from './customerForm';

const FeCustomersNew = ({
  customerMode,
  navigation,
  navigation: { state },
  onSubmitNewCustomer
}) => {

  const navigationProps = {
    ...navigation,
    state: { ...state, customerMode },
    onSubmitNewCustomer
  };

  const props = { navigation: navigationProps };

  return (
    <Content>
      <CustomerForm {...props} />
    </Content>
  )
};

export default FeCustomersNew;
