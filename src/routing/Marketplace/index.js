import StackNavigator from 'react-navigation/src/navigators/StackNavigator';
import MarketPlace from '../../pages/MarketPlaceScreens/MarketPlace';
import MarketplaceCart from '../../pages/MarketPlaceScreens/MarketplaceCart';
import Inventory from '../../pages/InventoryScreens/Inventory';

const MarketPlaceRouting = StackNavigator(
  {
    Marketplace: { screen: MarketPlace },
    MarketplaceCart: { screen: MarketplaceCart },
    MarketplaceItem: { screen: Inventory }
  },
  {
    initialRouteName: 'Marketplace',
    headerMode: 'none',

    navigationOptions: {
      drawerLockMode: 'locked-closed',
      headerVisible: false
    }
  }
);

export default MarketPlaceRouting;
