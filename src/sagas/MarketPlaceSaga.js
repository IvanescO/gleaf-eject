import { takeEvery, call, put, fork, all, throttle } from 'redux-saga/effects';
import * as cst  from '../constants';
import InventoryService from '../services/InventoryService';
import CartService from '../services/CartService';
import OrdersService from '../services/OrdersService';
import { Toast } from 'native-base';
import arrify from 'arrify';

function* marketInventoryListRequesting(action) {
  try {
    // console.log("MarketPlace SAGA", action);

    const [inventoryList, types] = yield all([
      call(InventoryService.getMarketInventory, action.params),
      call(InventoryService.getInventoryType)
    ]);

    // console.log("inventoryList", inventoryList);
    // console.log("types", types);

    const { inventory, currentPage, total, itemsPerPage } = inventoryList.data;
    const inventoryTypes = types.data;

    yield put({
      type: cst.MARKETPLACE_SUCCESSFUL,
      inventory,
      currentPage,
      itemsPerPage,
      total,
      inventoryTypes
    });
  } catch (e) {
    yield put({ type: cst.MARKETPLACE_FAILURE, errors: e.message });
  }
}

function* marketplaceGetCart() {
  try {
    const response = yield call(CartService.getUserCart);
    const cart = response.data;
    console.log("marketplaceGetCart resp", cart);

    yield put({
      type: cst.MARKETPLACE_GET_CART_SUCCESSFUL,
      cart: cart.data,
      total: cart.total,
      expireTime: cart.expire_time,
      timestamp: cart.timestamp
    });
  } catch (e) {
    yield put({ type: cst.MARKETPLACE_GET_CART_FAILURE, addingError: e });
  }
}

function* marketplaceDelCartItem(action) {
  try {
    const response = yield call(CartService.removeItemFromUserCart, action.id);
    console.log("marketplaceDelCartItem", response);

    const cart = response.data;
    yield put({
      type: cst.MARKETPLACE_DELL_CART_ITEM_SUCCESSFUL,
      cart: arrify(cart.data),
      total: cart.total,
      expireTime: cart.expire_time,
      timestamp: cart.timestamp
    });
  } catch (e) {
    yield put({ type: cst.MARKETPLACE_DELL_CART_ITEM_FAILURE, addingError: e });
  }
}

function* marketAddItemToCart(action) {
  console.log("marketAddItemToCart saga", action)
  try {
    const response = yield call(CartService.userAddItemToCart, action.item);
    const cart = response.data;

    yield put({
      type: cst.MARKETPLACE_ADD_CART_ITEM_SUCCESSFUL,
      cart: cart.data,
      total: cart.total,
      expireTime: cart.expire_time,
      timestamp: cart.timestamp
    });
    const inventoryList = yield call(InventoryService.getMarketInventory);

    console.log("marketAddItemToCart List", inventoryList);
    const { inventory, currentPage, total, itemsPerPage } = inventoryList.data;

    yield put({
      type: cst.MARKETPLACE_SUCCESSFUL,
      inventory, currentPage, total, itemsPerPage
    });

  } catch (e) {
    yield put({
      type: cst.MARKETPLACE_ADD_CART_ITEM_FAILURE,
      addingError: e
    });
  }
}

function* marketDelCartTimeout() {
  try {
    const response = yield call(CartService.deleteUserCartTimeout);
    const cart = response.data;
    console.log("marketDelCartTimeout", cart);

    yield put({
      type: cst.MARKETPLACE_DELL_CART_SUCCESSFUL,
      cart: [],
      expireTime: null,
      timestamp: null
    });

    const inventoryList = yield call(InventoryService.getMarketInventory);
    console.log("marketDelCartTimeout list", inventoryList);

    const { inventory, currentPage, total, itemsPerPage } = inventoryList.data;

    yield put({
      type: cst.MARKETPLACE_REQUESTING,
      inventory, currentPage, total, itemsPerPage
    });
  } catch (e) {
    yield put({ type: cst.MARKETPLACE_DELL_CART_FAILURE });
  }
}

function* cancelMarketplaceOrder(action) {
  try {
    const responce = yield call(CartService.deleteMarketCart);

    yield put({ type: cst.MARKETPLACE_CLEAR_CART_SUCCESSFUL });

    const inventoryList = yield call(InventoryService.getMarketInventory);

    console.log("marketAddItemToCart List", inventoryList);
    const { inventory, currentPage, total, itemsPerPage } = inventoryList.data;

    yield put({
      type: cst.MARKETPLACE_SUCCESSFUL,
      inventory, currentPage, total, itemsPerPage
    });

    Toast.show({
      text: 'Clear cart was successfully',
      type: 'success'
    });

  } catch (e) {
    console.log(e);
    yield put({ type: cst.MARKETPLACE_CLEAR_CART_FAILURE, errors: e });
    Toast.show({
      text: 'Clear cart was failed',
      type: 'danger'
    });
  }
}

function* createMarketplaceOrder(action) {
  try {
    const response = yield call(OrdersService.createMarketOrder);
    const cart = response.data;

    yield put({
      type: cst.MARKETPLACE_CREATE_ORDER_SUCCESSFUL,
      message: '',
      cart: cart
    });

    yield put({
      type: cst.MARKETPLACE_DELL_CART_SUCCESSFUL,
      cart: cart,
      expireTime: null,
      timestamp: null
    });

    Toast.show({
      text: 'Order was created successfully',
      type: 'success'
    });
  } catch (e) {
    console.log("MARKETPLACE_CREATE_ORDER_FAILURE", e);

    yield put({ type: MARKETPLACE_CREATE_ORDER_FAILURE, errors: e });
    Toast.show({
      text: 'Order creation failed',
      type: 'danger'
    });
  }
}

function* watchInventoryRequesting() {
  yield throttle(500, cst.MARKETPLACE_REQUESTING, marketInventoryListRequesting);
}
/* Cart */
function* watchMarketplaceGetCart() {
  yield takeEvery(cst.MARKETPLACE_GET_CART, marketplaceGetCart);
}
function* watchMarketplaceDelItem() {
  yield takeEvery(cst.MARKETPLACE_DELL_CART_ITEM, marketplaceDelCartItem);
}
function* watchMarketAddItemToCart() {
  yield takeEvery(cst.MARKETPLACE_ADD_CART_ITEM, marketAddItemToCart);
}
function* watchMarketCartTimeout() {
  yield takeEvery(cst.MARKETPLACE_DELL_CART, marketDelCartTimeout);
}
function* watchCancelMarketplaceOrder() {
  yield takeEvery(cst.MARKETPLACE_CLEAR_CART, cancelMarketplaceOrder);
}
function* watchCreateMarketplaceOrder() {
  yield takeEvery(cst.MARKETPLACE_CREATE_ORDER, createMarketplaceOrder);
}


export default function*() {
  yield [
    fork(watchInventoryRequesting),
    fork(watchMarketplaceGetCart),
    fork(watchMarketAddItemToCart),
    fork(watchMarketplaceDelItem),
    fork(watchMarketCartTimeout),
    fork(watchCancelMarketplaceOrder),
    fork(watchCreateMarketplaceOrder),
  ];
}
