import { CREATE_REPORT, CREATE_REPORT_SUCCESSFUL, CREATE_REPORT_FAILURE } from '../constants/index';

const initialState = {
  generatedReport: {},
  reportCreating: false
};

function ReportReducer(state = initialState, action) {
  switch (action.type) {
    case CREATE_REPORT: {
      return { ...state, reportCreating: true, generatedReport: {} };
    }
    case CREATE_REPORT_SUCCESSFUL: {
      return {
        ...state,
        generatedReport: action.report,
        reportCreating: false
      };
    }
    case CREATE_REPORT_FAILURE: {
      return {
        ...state,
        error: action.error,
        reportCreating: false
      };
    }
    default:
      return state;
  }
}
export default ReportReducer;
