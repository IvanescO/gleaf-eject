import React, { Component } from 'react';
import connect from 'react-redux/lib/connect/connect';
import { StyleSheet } from 'react-native';
import {
  Container,
  ListItem,
  Content,
  Text,
  Button,
  Item,
  Input,
  View,
  Form,
  Right,
  Title,
  Body,
  Switch,
  Spinner
} from 'native-base';
import Layout from '../../components/Layout';
import { SETTINGS_GET_REQUEST, SETTINGS_UPDATE_REQUEST } from '../../constants';
import { colors } from '../../theme';

class Settings extends Component {
  state = {
    email: '',
    phone: '',
    email_notify: false,
    phone_notify: false
  };

  componentDidMount() {
    const { fetchSettings } = this.props;
    fetchSettings();
  }

  componentWillReceiveProps(nextProps) {
    const { email, phone, email_notify, phone_notify } = nextProps;
    this.setState({ email, email_notify, phone, phone_notify });
  }

  onChangeEmail = value => {
    this.setState({ email: value });
  };

  onChangePhone = value => {
    this.setState({ phone: value });
  };

  onToggleEmail = value => {
    this.setState({ email_notify: value });
  };

  onToggleSms = value => {
    this.setState({ phone_notify: value });
  };

  onSubmit = () => {
    const settings = this.state;
    const { submitSettings } = this.props;
    submitSettings(settings);
  };

  render() {
    const { email, email_notify, phone, phone_notify } = this.state;
    const { loadingSettings } = this.props;
    const TITLE = 'Settings';

    return (
      <Layout title={TITLE} drawerOpen={() => this.props.navigation.navigate('DrawerOpen')}>
        <Container>
          <View style={styles.settingsHeader}>
            <Text>Notifications options</Text>
          </View>
          {loadingSettings && <Spinner />}
          {!loadingSettings && (
            <Content>
              <Form style={styles.formContainer}>
                <View style={{ marginBottom: 20 }}>
                  <ListItem style={styles.inputItem}>
                    <Body>
                      <Text>Switch email notifications</Text>
                    </Body>
                    <Right>
                      <Switch
                        thumbTintColor={email_notify ? colors.primary : null}
                        // onTintColor={colors.white}
                        // tintColor={colors.primary}
                        value={email_notify}
                        onValueChange={value => this.onToggleEmail(value)}
                      />
                    </Right>
                  </ListItem>
                  <ListItem style={styles.inputItem}>
                    <Body>
                      <Text>Switch sms notifications</Text>
                    </Body>
                    <Right>
                      <Switch
                        thumbTintColor={phone_notify ? colors.primary : null}
                        value={phone_notify}
                        onValueChange={value => this.onToggleSms(value)}
                      />
                    </Right>
                  </ListItem>
                </View>
                <View style={styles.inputContainer}>
                  <Item>
                    <Input
                      type="email"
                      value={email}
                      onChangeText={this.onChangeEmail}
                      placeholder="Email Adress"
                    />
                  </Item>
                  <Item>
                    <Input
                      type="phone"
                      value={phone}
                      onChangeText={this.onChangePhone}
                      placeholder="Phone number"
                    />
                  </Item>
                </View>
                <Item style={styles.btnContainer}>
                  <Button success style={styles.btn} block onPress={this.onSubmit}>
                    <Title style={{ fontSize: 16 }}>Save</Title>
                  </Button>
                </Item>
              </Form>
            </Content>
          )}
        </Container>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  settingsHeader: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  formContainer: {
    justifyContent: 'flex-start'
  },
  inputContainer: {
    marginBottom: 20,
    marginRight: 20,
    marginLeft: 20
  },
  inputItem: {
    height: 50,
    marginBottom: 5,
    marginTop: 5
  },
  btnContainer: {
    justifyContent: 'center',
    borderBottomWidth: 0
  },
  btn: {
    width: 200
  }
});

function mapStateToProps(state) {
  return {
    loadingSettings: state.SettingsReducer.loading,
    email: state.SettingsReducer.email,
    phone: state.SettingsReducer.phone,
    email_notify: state.SettingsReducer.email_notify,
    phone_notify: state.SettingsReducer.phone_notify
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchSettings: () => dispatch({ type: SETTINGS_GET_REQUEST }),
    submitSettings: settings => {
      dispatch({ type: SETTINGS_UPDATE_REQUEST, payload: settings });
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Settings);
