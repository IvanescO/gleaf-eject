import {
  SETTINGS_GET_REQUEST,
  SETTINGS_GET_SUCCESSFUL,
  SETTINGS_GET_FAILURE,
  SETTINGS_UPDATE_REQUEST,
  SETTINGS_UPDATE_SUCCESSFUL,
  SETTINGS_UPDATE_FAILURE
} from '../constants';

const initialState = {
  loading: false,
  email_notify: false,
  phone_notify: false,
  email: '',
  phone: ''
};

function SettingsReducer(state = initialState, action) {
  switch (action.type) {
    case SETTINGS_GET_REQUEST: {
      return { ...state, loading: true };
    }
    case SETTINGS_GET_SUCCESSFUL: {
      return {
        ...state,
        loading: false,
        email: action.payload.email,
        phone: action.payload.phone,
        email_notify: action.payload.email_notify,
        phone_notify: action.payload.phone_notify,
        customer_id: action.payload.customer_id
      };
    }
    case SETTINGS_UPDATE_REQUEST: {
      return { ...state, loading: true };
    }
    case SETTINGS_UPDATE_SUCCESSFUL: {
      return {
        ...state,
        loading: false,
        email: action.payload.email,
        phone: action.payload.phone,
        email_notify: action.payload.email_notify,
        phone_notify: action.payload.phone_notify
      };
    }

    case SETTINGS_GET_FAILURE: {
      return { ...state, error: action.errors, loading: false };
    }
    case SETTINGS_UPDATE_FAILURE: {
      return { ...state, error: action.errors, loading: false };
    }

    default:
      return state;
  }
}
export default SettingsReducer;
