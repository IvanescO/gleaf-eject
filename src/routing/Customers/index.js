import StackNavigator from 'react-navigation/src/navigators/StackNavigator';
import CustomersList from '../../pages/Adminka/CustomersScreens/CustomersList';
import CustomerEdit from '../../pages/Adminka/CustomersScreens/CustomerEdit';

const CustomersRouting = StackNavigator(
  {
    CustomersList: { screen: CustomersList },
    CustomerEdit: { screen: CustomerEdit }
  },
  {
    initialRouteName: 'CustomersList',
    headerMode: 'none',

    navigationOptions: {
      drawerLockMode: 'locked-closed',
      headerVisible: false
    }
  }
);

export default CustomersRouting;
