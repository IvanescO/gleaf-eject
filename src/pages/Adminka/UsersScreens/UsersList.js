import React from 'react';
import { StyleSheet, Text, View, RefreshControl, ListView } from 'react-native';
import Layout from '../../../components/Layout';
import UserService from '../../../services/UserService';
import { Grid, Row, Col, Container, List, ListItem, Button, Icon, Spinner } from 'native-base';
import { connect } from 'react-redux';
import { STORE_USERS } from '../../../constants/index';
import { Pagination } from '../../../components';

class UserList extends React.Component {
  state = { users: null, isRefreshing: false };
  componentWillMount = async () => {
    const usersResp = await UserService.getUsersList({ page: 1 });
    // const users = usersResp.data.users;
    const { users, total, currentPage, itemsPerPage } = usersResp.data;
    const { storeUsers } = this.props;
    storeUsers(users);
    this.setState({ users, currentPage, totalPage: Math.ceil(total / itemsPerPage) });
  };

  componentWillReceiveProps(nextProps) {
    const { users } = nextProps;
    this.setState({ users });
  }
  editUser = async (customer, secId, rowId) => {
    this.props.navigation.navigate('UserEdit', {
      customer,
      rowId
    });
  };

  goToPage = async num => {
    const usersResp = await UserService.getUsersList({ page: num });
    const { users, total, currentPage, itemsPerPage } = usersResp.data;
    const { storeUsers } = this.props;
    storeUsers(users);
    this.setState({ users, currentPage, totalPage: Math.ceil(total / itemsPerPage) });
  };

  ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
  render() {
    const TITLE = 'UserList';
    const { users } = this.state;
    return (
      <Layout title={TITLE} drawerOpen={() => this.props.navigation.navigate('DrawerOpen')}>
        {users && (
          <Container>
            <List
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isRefreshing}
                  onRefresh={this.refreshCustomer}
                />
              }
              dataSource={this.ds.cloneWithRows(users)}
              renderRow={data => (
                <ListItem>
                  <Grid>
                    <Row>
                      <Col style={styles.pdLeft5} size={22}>
                        <Text style={styles.fs12}>{data.user_login} </Text>
                      </Col>
                      <Col size={50}>
                        <Text style={styles.fs12}>{data.user_email}</Text>
                      </Col>
                      <Col size={28}>
                        <Text style={styles.fs12}>{data.roles}</Text>
                      </Col>
                    </Row>
                  </Grid>
                </ListItem>
              )}
              renderRightHiddenRow={(data, secId, rowId, rowMap) => (
                <Row>
                  <Col>
                    <Button
                      info
                      onPress={_ => {
                        this.editUser(data, secId, rowId, rowMap);
                        rowMap[`${secId}${rowId}`].props.closeRow();
                      }}
                    >
                      <Icon info type="FontAwesome" name="pencil" />
                    </Button>
                  </Col>
                </Row>
              )}
              renderHeader={() => (
                <Row>
                  <Col size={22}>
                    <Text>Login </Text>
                  </Col>
                  <Col size={50}>
                    <Text>Email</Text>
                  </Col>
                  <Col size={28}>
                    <Text>Roles </Text>
                  </Col>
                </Row>
              )}
              leftOpenValue={75}
              rightOpenValue={-50}
              //disableRightSwipe
            />
            <Pagination
              currentPage={this.state.currentPage}
              totalPage={this.state.totalPage}
              pageHandler={this.goToPage}
            />
          </Container>
        )}
        {!users && <Spinner />}
      </Layout>
    );
  }
}
function mapStateToProps(state) {
  const {
    AdminEditing: { users }
  } = state;
  return { users };
}
function mapDispatchToProps(dispatch) {
  return {
    storeUsers: users => {
      dispatch({ type: STORE_USERS, users });
    }
  };
}
const styles = StyleSheet.create({
    fs12: {
        fontSize:12
    },
    pdLeft5:{
        paddingLeft: 5
    }
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserList);
