const validate = values => {
  const errors = {};

  if (!values.user_login) {
    errors.user_login = 'Required';
  }

  if (!values.user_email) {
    errors.user_email = 'Required';
  } else if (
    !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
      values.user_email
    )
  ) {
    errors.user_email = 'Invalid email address';
  }

  if (!values.extra_email) {
    errors.extra_email = 'Required';
  } else if (
    !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
      values.extra_email
    )
  ) {
    errors.extra_email = 'Invalid email address';
  }

  if (!values.user_pass) {
    errors.user_pass = 'Required';
  }
  if (values.user_pass) {
    const patternPass = /^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]+$/g;

    if (values.user_pass.length < 8 || !patternPass.test(values.user_pass)) {
      errors.user_pass = 'Invalid';
    }
  }

  if (!values.user_pass_confirm) {
    errors.user_pass_confirm = 'Required';
  }
  if (values.user_pass_confirm) {
    if (values.user_pass_confirm != values.user_pass) {
      errors.user_pass_confirm = "Password doesn't mach";
    }
  }

  if (!values.dispensary_name) {
    errors.dispensary_name = 'Required';
  }
  if (!values.address) {
    errors.address = 'Required';
  }
  if (!values.first_name) {
    errors.first_name = 'Required';
  }
  if (!values.last_name) {
    errors.last_name = 'Required';
  }
  if (!values.phone_number) {
    errors.phone_number = 'Required';
  } else if (!/^\d+$/.test(values.phone_number)) {
    errors.phone_number = 'It is not number';
  }

  return errors;
};

export default validate;
