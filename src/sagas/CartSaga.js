/**
 * Watchers
 */
import { takeEvery, call, put, fork, all  } from 'redux-saga/effects';
import CartService from '../services/CartService';
import InventoryService from '../services/InventoryService';
import { Toast } from 'native-base';
import * as cst from '../constants/index';

function* adminAddItemToCart(action) {
  try {
    // console.log("задержка start")
    // yield call(delay, 6000);

    const response = yield call(CartService.adminAddItemToCart, action.item);

    // console.log("задержка end")

    const cart = response.data;
    console.log("adminAddItemToCart response", response);

    yield put({
      type: cst.ADMIN_ADD_CART_ITEM_SUCCESSFUL,
      cart: cart.items,
      total: cart.total
    });
  } catch (e) {
    yield put({
      type: cst.ADMIN_ADD_CART_ITEM_FAILURE,
      addingError: e
    });
  }
}

function* adminDeltCartItem(action) {
  try {
    const response = yield call(CartService.removeItemFromCart, action.id);
    const cart = response.data.items;
    const total = response.data.total;
    console.log("adminDeltCartItem resp", response);

    yield put({
      type: cst.ADMIN_DEL_CART_ITEM_SUCCESSFUL,
      cart,
      total
    });
  } catch (e) {
    yield put({
      type: cst.ADMIN_DEL_CART_ITEM_FAILURE,
      addingError: e
    });
  }
}

function* adminGetCart() {
  try {
    const response = yield call(CartService.getCart);
    const cart = response.data.items;
    const total = response.data.total;
    console.log("adminGetCart response", response);

    yield put({ type: cst.ADMIN_GET_CART_SUCCESSFUL, cart, total });
  } catch (e) {
    yield put({ type: cst.ADMIN_GET_CART_FAILURE, addingError: e });
  }
}

function* cancelAdminOrder(action) {
  try {
    const responce = yield call(CartService.deleteAdminCart);

    yield put({ type: cst.ADMIN_CLEAR_CART_SUCCESSFUL });

    const inventoryList = yield call(InventoryService.getInventory);

    console.log("inventoryList admin", inventoryList);
    const { inventory, currentPage, total, itemsPerPage } = inventoryList.data;

    yield put({
      type: cst.INVENTORY_SUCCESSFUL,
      inventory, currentPage, total, itemsPerPage
    });

    Toast.show({
      text: 'Clear cart was successfully',
      type: 'success'
    });

  } catch (e) {
    console.log(e);
    yield put({ type: cst.ADMIN_CLEAR_CART_FAILURE, errors: e });
    Toast.show({
      text: 'Clear cart was failed',
      type: 'danger'
    });
  }
}

function* updateAdminCartItem(action) {
  try {
    const response = yield call(CartService.updateCartItem, action.order);
    const { items, total } = response.data;
    console.log("adminGetCart response", response);

    yield put({
      type: cst.ADMIN_UPDATE_CART_ITEM_SUCCESSFUL,
      cart: items,
      total: total
    });
  } catch (e) {
    yield put({
      type: cst.ADMIN_UPDATE_CART_ITEM_FAILURE,
      addingError: e
    });
  }
}

function* watchAdminAddItemToCart() {
  yield takeEvery(cst.ADMIN_ADD_CART_ITEM, adminAddItemToCart);
}

function* watchAdminGetCart() {
  yield takeEvery(cst.ADMIN_GET_CART, adminGetCart);
}
function* watchAdminadminDeltCartItem() {
  yield takeEvery(cst.ADMIN_DEL_CART_ITEM, adminDeltCartItem);
}
function* watchCancelAdminOrder() {
  yield takeEvery(cst.ADMIN_CLEAR_CART, cancelAdminOrder);
}
function* watchUpdateAdminCartItem() {
  yield takeEvery(cst.ADMIN_UPDATE_CART_ITEM, updateAdminCartItem);
}


export default function* () {
  yield [
    fork(watchAdminAddItemToCart),
    fork(watchAdminGetCart),
    fork(watchAdminadminDeltCartItem),
    fork(watchCancelAdminOrder),
    fork(watchUpdateAdminCartItem),
  ];
}
