import React, { Component } from 'react';
import { Icon, Button, Container, Left, Right } from 'native-base';
import Header from './CustomHeader';
import { View } from 'react-native';

class Layout extends Component {
  render() {
    const { title, drawerOpen } = this.props;
    return (
      <Container>
        <Header title={title} drawerOpen={drawerOpen} />
        {this.props.children}
      </Container>
    );
  }
}
export default Layout;
