import http from './http';

class CartService {
  static async getCart() {
    return http.get('cart');
  }
  static async adminAddItemToCart(item) {
    return http.put('cart', item);
  }
  static async removeItemFromCart(itemId) {
    return http.delete(`cart-item/${itemId}`);
  }
  static async removeItemFromUserCart(itemId) {
    return http.delete(`market-cart-item/${itemId}`);
  }
  static async getUserCart() {
    return http.get('market-cart');
  }
  static async userAddItemToCart(cart) {
    return http.post('market-cart', cart);
  }
  static async deleteUserCartTimeout() {
    return http.delete('market-cart-timeout');
  }
  static async deleteMarketCart() {
    return http.delete('market-cart');
  }
  static async deleteAdminCart() {
    return http.delete('cart');
  }
  static async updateCartItem(data) {
    console.log("CartService updateCartItem", data)
    return http.put(`cart-item/${data.id}`, data);
  }
}
export default CartService;
