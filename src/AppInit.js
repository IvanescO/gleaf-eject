import React from 'react';
import { StyleSheet, Text, View, AsyncStorage } from 'react-native';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import connect from 'react-redux/lib/connect/connect';
import AuthTabs from './auth/AuthTabs';
import Router from './routing';
// import { SecureStore } from 'expo';
import { Spinner, Container } from 'native-base';
import { AUTHENTICATION_SUCCESSFUL } from './constants/index';
import { CUSTOMER_ADMIN, CUSTOMER_AGENT, AGENT_ADMIN, ADMIN } from './constants/roles';

class AppInitialize extends React.Component {
  state = { loading: true };

  async componentWillMount() {
    const { authUser } = this.props;
    let [token, user] = await Promise.all([
      AsyncStorage.getItem('TOKEN'),
      AsyncStorage.getItem('USER')
    ]);
    user = JSON.parse(user);
    this.setState({ loading: false });
    if (token && user) {
      authUser(token, user);
    }
  }
  render() {
    const { CustomerAdminRouter, AdminRouter, CustomerAgentRouter } = Router;
    const { loading } = this.state;
    const { AuthReducer } = this.props;
    if (loading) {
      return (
        <Container style={styles.container}>
          <Spinner />
        </Container>
      );
    }
    if (AuthReducer.token && this.userHasRole(CUSTOMER_ADMIN)) {
      return <CustomerAdminRouter />;
    }
    if (AuthReducer.token && this.userHasRole(CUSTOMER_AGENT)) {
      return <CustomerAgentRouter />;
    }
    if (AuthReducer.token && this.userHasRole(AGENT_ADMIN)) {
      return <AdminRouter />;
    }
    if (AuthReducer.token && this.userHasRole(ADMIN)) {
      return <AdminRouter />;
    }
    return <AuthTabs />;
  }
  userHasRole = role => {
    const { AuthReducer } = this.props;
    return AuthReducer.user.roles.includes(role);
  };
}

function mapStateToProps(state) {
  // console.log('hreree');
  const { AuthReducer } = state;
  return { AuthReducer };
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});
function mapDispatchToProps(dispatch) {
  return {
    authUser: (token, user) => dispatch({ type: AUTHENTICATION_SUCCESSFUL, token, user })
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppInitialize);
