import http from './http';

class MarketCustomerService {
  static getCustomersList(params) {
    console.log(params);
    console.log('************');
    return http.get('/market-customer', { params });
  }
  static createCustomer(customer) {
    return http.post('/market-customer', customer);
  }
}
export default MarketCustomerService;
