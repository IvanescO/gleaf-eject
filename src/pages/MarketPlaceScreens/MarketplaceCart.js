import React from 'react';
import connect from 'react-redux/lib/connect/connect';
import { Container, List, ListItem, Row, Col, Icon, Button, Spinner } from 'native-base';
import { StyleSheet, Text, View, ListView, RefreshControl } from 'react-native';
import Layout from '../../components/Layout';
import * as cst from '../../constants/index';
import { colors } from '../../theme';

class MarketplaceCart extends React.Component {
  state = {
    cart: [],
    isRefreshing: false,
    loadingSaveOrder: false,
    customer: '',
    total: '',
  };

  ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

  componentWillMount() {
    const { getUserCart } = this.props;
    getUserCart();
  }

  componentWillReceiveProps(nextProps) {
    const { MarketplaceCartReducer } = nextProps;
    const {
      cart,
      loadingAdding,
      loadingGetting,
      loadingSaveOrder,
      total
    } = MarketplaceCartReducer;

    console.log('nextProps Cart', nextProps);

    this.setState({
      cart,
      total,
      loadingAdding,
      loadingGetting,
      loadingSaveOrder,
    });
  }

  refreshCart = () => {
    const { getUserCart } = this.props;
    getUserCart();
  };

  deleteInventoryByID = data => {
    console.log('deleteInventoryByID', data);
    const { userRemoveItem } = this.props;
    userRemoveItem(data.id);
  };

  createOrder = async () => {
    const { createOrder } = this.props;
    const { customer } = this.state;
    console.log('createOrder', customer);

    createOrder(customer);
    this.setState({ total: 0 });
  };

  cancelOrder = () => {
    const { clearCart } = this.props;
    clearCart();
  };

  render() {
    const {
      navigation,
      cart,
      loadingGetting,
      loadingSaveOrder,
      loadingAdding,
      customer,
      total,
      isRefreshing
    } = this.state;
    const TITLE = ' Marketplace Cart';

    return (
      <Layout title={TITLE} drawerOpen={() => navigation.navigate('DrawerOpen')}>
        <Container>
          {Boolean(loadingGetting) && <Spinner style={{ flex: 1 }} />}
          {Boolean(!loadingGetting) &&
            Boolean(cart.length) && (
              <Container>
                <View style={{ height: 50 }}>
                  <Row>
                    <Col size={2} style={{ justifyContent: 'center', alignItems: 'center' }}>
                      <Text>Type</Text>
                    </Col>
                    <Col size={2} style={{ justifyContent: 'center', alignItems: 'center' }}>
                      <Text>Strain</Text>
                    </Col>
                    <Col size={2} style={{ justifyContent: 'center', alignItems: 'center' }}>
                      <Text>THC</Text>
                    </Col>
                    <Col size={2} style={{ justifyContent: 'center', alignItems: 'center' }}>
                      <Text>price</Text>
                    </Col>
                    <Col size={2} style={{ justifyContent: 'center', alignItems: 'center' }}>
                      <Text>package</Text>
                    </Col>
                  </Row>
                </View>

                {Boolean(loadingSaveOrder) && <Spinner style={{ flex: 0.75 }} />}

                {Boolean(cart.length) &&
                  Boolean(!loadingSaveOrder) && (
                    <List
                      refreshControl={
                        <RefreshControl refreshing={isRefreshing} onRefresh={this.refreshCart} />
                      }
                      dataSource={this.ds.cloneWithRows(cart)}
                      renderFooter={() => (
                        <Row>
                          <Col>
                            <Text>Total price: ${this.state.total}</Text>
                          </Col>
                        </Row>
                      )}
                      renderRow={data => (
                        <ListItem>
                          <Row>
                            <Col
                              size={2}
                              style={{ justifyContent: 'center', alignItems: 'center' }}
                            >
                              <Text style={{ fontSize: 12 }}> {data.type_name} </Text>
                            </Col>
                            <Col
                              size={2}
                              style={{ justifyContent: 'center', alignItems: 'center' }}
                            >
                              <Text style={{ fontSize: 12 }}> {data.strain} </Text>
                            </Col>
                            <Col
                              size={2}
                              style={{ justifyContent: 'center', alignItems: 'center' }}
                            >
                              <Text style={{ fontSize: 12 }}> {data.thc} </Text>
                            </Col>
                            <Col
                              size={2}
                              style={{ justifyContent: 'center', alignItems: 'center' }}
                            >
                              <Text style={{ fontSize: 12 }}> ${data.price} </Text>
                            </Col>
                            <Col
                              size={2}
                              style={{ justifyContent: 'center', alignItems: 'center' }}
                            >
                              <Text style={{ fontSize: 12 }}> {data.package} </Text>
                            </Col>
                          </Row>
                        </ListItem>
                      )}
                      renderLeftHiddenRow={() => {}}
                      renderRightHiddenRow={(data, secId, rowId, rowMap) => (
                        <Row>
                          <Col>
                            <Button
                              danger
                              onPress={_ => {
                                this.deleteInventoryByID(data, secId, rowId, rowMap);
                                rowMap[`${secId}${rowId}`].props.closeRow();
                              }}
                            >
                              <Icon active type="FontAwesome" name="trash" />
                            </Button>
                          </Col>
                        </Row>
                      )}
                      rightOpenValue={-75}
                      leftOpenValue={75}
                      disableRightSwipe
                    />
                  )}
                <Row style={{ flex: 0 }}>
                  <Col style={styles.pd10}>
                    {Boolean(!loadingSaveOrder) && (
                      <Button
                        style={{ marginTop: 10 }}
                        danger
                        full
                        onPress={() => this.cancelOrder()}
                      >
                        <Text style={{ color: '#fff' }}> Clear Cart </Text>
                      </Button>
                    )}
                  </Col>
                  <Col style={styles.pd10}>
                    {Boolean(!loadingSaveOrder) && (
                      <Button
                        style={{ marginTop: 10 }}
                        success
                        full
                        onPress={() => this.createOrder()}
                      >
                        <Text style={{ color: '#fff' }}> Save Order </Text>
                      </Button>
                    )}
                  </Col>
                </Row>
              </Container>
            )}

          {Boolean(!cart.length) && (
            <Container style={styles.container}>
              <Text>Cart Is Empty</Text>
            </Container>
          )}
        </Container>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 40
  },
  pd10: {
    padding: 10
  },
  inputContainer: {
    marginTop: 20
  }
});

function mapStateToProps(state) {
  const { MarketplaceCartReducer } = state;
  return { MarketplaceCartReducer };
}

function mapDispatchToProps(dispatch) {
  return {
    getUserCart: () => {
      dispatch({ type: cst.MARKETPLACE_GET_CART });
    },
    userRemoveItem: id => {
      dispatch({ type: cst.MARKETPLACE_DELL_CART_ITEM, id });
    },
    createOrder: customer => {
      dispatch({ type: cst.MARKETPLACE_CREATE_ORDER, order: { customer } });
    },
    clearCart: () => {
      dispatch({ type: cst.MARKETPLACE_CLEAR_CART });
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MarketplaceCart);
