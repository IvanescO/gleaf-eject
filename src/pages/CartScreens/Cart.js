import React from 'react';
import connect from 'react-redux/lib/connect/connect';
import {
  Container,
  List,
  ListItem,
  Row,
  Col,
  Icon,
  Button,
  Spinner,
  Form,
  Item,
  Label,
  Input
} from 'native-base';
import { StyleSheet, Text, View, ListView, RefreshControl, Modal, Alert } from 'react-native';
import Layout from '../../components/Layout';
import * as cst from '../../constants/index';
import { colors } from '../../theme';

class AdminCart extends React.Component {
  state = {
    cart: [],
    isRefreshing: false,
    loadingSaveOrder: false,
    customer: '',
    modalVisible: false,
    total: '',
    modalEditingVisible: false,
    orderEditing: { id: '', quantity: '0', price: '0' }
  };
  ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

  componentWillMount() {
    this.refreshCart();
  }

  componentWillReceiveProps(nextProps) {
    const { CartReducer } = nextProps;
    const {
      cart,
      loadingAdding,
      loadingGetting,
      loadingSaveOrder,
      total
    } = CartReducer;

    console.log("nextProps CART", cart);

    this.setState({
      cart,
      total,
      loadingAdding,
      loadingGetting,
      loadingSaveOrder,
    });
  }

  refreshCart = () => this.props.getCart();

  deleteInventoryByID = data => {
    const { removeItemFromCart } = this.props;
    console.log("deleteInventoryByID", data);

    removeItemFromCart(data.inventory_id);
  };

  createOrder = () => {
    const { customer } = this.state;
    const { createOrder } = this.props;
    console.log("createOrder", customer);

    createOrder(customer);
    this.setState({ modalVisible: false, total: 0 });
  };

  updateOrder = () => {
    const { orderEditing } = this.state;
    const { updateOrder } = this.props;

    updateOrder(orderEditing);
    this.setState({ modalEditingVisible: false });
  };

  ediInventoryByID = (data, index) => {
    const { orderEditing } = this.state;
    const { inventory_id, order_price, order_quantity, price, quantity } = data;
    console.log("ediInventoryByID", data);

    if (!order_quantity) {
      Alert.alert('Allert', 'Your can`t edit this inventory', [
        // { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'OK', onPress: () => {} }
      ]);
      return;
    }

    this.setState({
      modalEditingVisible: true,
      orderEditing: {
        ...orderEditing,
        index,
        id: inventory_id,
        price,
        quantity: quantity.split('.')[0]
      }
    });
  };

  cancelOrder = () => this.props.clearCart();

  toggleModalVisible = () => this.setState({ modalVisible: true });

  render() {
    const {
      cart,
      loadingGetting,
      loadingSaveOrder,
      customer,
      total,
      orderEditing,
      modalVisible,
      modalEditingVisible,
      isRefreshing,
    } = this.state;
    const { navigation } = this.props;
    const TITLE = 'Cart';

    return (
      <Layout title={TITLE} drawerOpen={() => navigation.navigate('DrawerOpen')}>
        <Container>
          <Modal
            onRequestClose={() => {}}
            animationType="slide"
            transparent
            visible={modalVisible}
          >
            <Container style={{ justifyContent: 'center' }}>
              <Form
                style={{
                  backgroundColor: 'white',
                  height: 160,
                  justifyContent: 'center',
                  borderColor: colors.primary,
                  borderWidth: 1
                }}
              >
                <Item floatingLabel>
                  <Label>customer</Label>
                  <Input
                    value={customer}
                    onChangeText={value => this.setState({ customer: value })}
                  />
                </Item>
                {Boolean(!loadingSaveOrder) && (
                  <Row style={{ justifyContent: 'center', marginTop: 10, marginBottom: 10 }}>
                    <Col style={{ paddingHorizontal: 20, backgroundColor: 'white' }}>
                      <Button
                        style={{ marginTop: 10 }}
                        danger
                        block
                        onPress={() => {
                          this.setState({ modalVisible: false });
                        }}
                      >
                        <Text style={{ color: '#fff' }}> cancel </Text>
                      </Button>
                    </Col>
                    <Col style={{ paddingHorizontal: 20, backgroundColor: 'white' }}>
                      <Button style={{ marginTop: 10 }} success block onPress={this.createOrder}>
                        <Text style={{ color: '#fff' }}> save </Text>
                      </Button>
                    </Col>
                  </Row>
                )}

                {Boolean(loadingSaveOrder) && <Spinner />}
              </Form>
            </Container>
          </Modal>

          <Modal
            onRequestClose={() => {}}
            animationType="slide"
            transparent
            visible={modalEditingVisible}
          >
            <Container style={{ justifyContent: 'center' }}>
              <Form
                style={{
                  backgroundColor: 'white',
                  height: 260,
                  justifyContent: 'center',
                  borderColor: colors.primary,
                  borderWidth: 1
                }}
              >
                <Item floatingLabel>
                  <Label>quantity</Label>
                  <Input
                    value={orderEditing.quantity}
                    onChangeText={quantity =>
                      this.setState({ orderEditing: { ...orderEditing, quantity } })
                    }
                  />
                </Item>
                <Item floatingLabel>
                  <Label>price</Label>
                  <Input
                    value={orderEditing.price}
                    onChangeText={price =>
                      this.setState({ orderEditing: { ...orderEditing, price } })
                    }
                  />
                </Item>

                {
                  <Row style={{ justifyContent: 'center', marginTop: 10, marginBottom: 10 }}>
                    <Col style={{ paddingHorizontal: 20, backgroundColor: 'white' }}>
                      <Button
                        style={{ marginTop: 10 }}
                        danger
                        block
                        onPress={() => {
                          this.setState({ modalEditingVisible: false });
                        }}
                      >
                        <Text style={{ color: '#fff' }}> cancel </Text>
                      </Button>
                    </Col>
                    <Col style={{ paddingHorizontal: 20, backgroundColor: 'white' }}>
                      <Button style={{ marginTop: 10 }} success block onPress={this.updateOrder}>
                        <Text style={{ color: '#fff' }}> save </Text>
                      </Button>
                    </Col>
                  </Row>
                }
                {Boolean(loadingSaveOrder) && <Spinner />}
              </Form>
            </Container>
          </Modal>

          {Boolean(loadingGetting) && <Spinner style={{ flex: 1 }} />}
          {Boolean(!loadingGetting) &&
            Boolean(cart.length) && (
              <Container>
                <View style={{ height: 50 }}>
                  <Row>
                    <Col size={2} style={{ justifyContent: 'center', alignItems: 'center' }}>
                      <Text>Type</Text>
                    </Col>
                    <Col size={2} style={{ justifyContent: 'center', alignItems: 'center' }}>
                      <Text>Strain</Text>
                    </Col>
                    <Col size={2} style={{ justifyContent: 'center', alignItems: 'center' }}>
                      <Text>THC</Text>
                    </Col>
                    <Col size={2} style={{ justifyContent: 'center', alignItems: 'center' }}>
                      <Text>price</Text>
                    </Col>
                    <Col size={2} style={{ justifyContent: 'center', alignItems: 'center' }}>
                      <Text>package</Text>
                    </Col>
                  </Row>
                </View>

                {Boolean(cart.length) && (
                  <List
                    refreshControl={
                      <RefreshControl
                        refreshing={isRefreshing}
                        onRefresh={this.refreshCart}
                      />
                    }
                    dataSource={this.ds.cloneWithRows(cart)}
                    renderFooter={() => (
                      <Row>
                        <Col>
                          <Text>Total price: ${total}</Text>
                        </Col>
                      </Row>
                    )}
                    renderRow={data => {
                        return (
                          <ListItem>
                            <Row>
                              <Col
                                size={2}
                                style={{ justifyContent: 'center', alignItems: 'center' }}
                              >
                                <Text style={{ fontSize: 12 }}>
                                  {' '}
                                  {data.inventory_item.type_name}{' '}
                                </Text>
                              </Col>
                              <Col
                                size={2}
                                style={{ justifyContent: 'center', alignItems: 'center' }}
                              >
                                <Text style={{ fontSize: 12 }}> {data.inventory_item.strain} </Text>
                              </Col>
                              <Col
                                size={2}
                                style={{ justifyContent: 'center', alignItems: 'center' }}
                              >
                                <Text style={{ fontSize: 12 }}> {data.inventory_item.thc} </Text>
                              </Col>
                              <Col
                                size={2}
                                style={{ justifyContent: 'center', alignItems: 'center' }}
                              >
                                <Text style={{ fontSize: 12 }}> ${data.price} </Text>
                              </Col>
                              <Col
                                size={2}
                                style={{ justifyContent: 'center', alignItems: 'center' }}
                              >
                                <Text style={{ fontSize: 12 }}> {data.quantity} </Text>
                              </Col>
                            </Row>
                          </ListItem>
                        );
                    }}
                    renderLeftHiddenRow={(data, secId, rowId, rowMap) => (
                      <Col>
                        <Button
                          warning
                          onPress={_ => {
                            this.ediInventoryByID(data, rowId, rowMap);
                            rowMap[`${secId}${rowId}`].props.closeRow();
                          }}
                        >
                          <Icon active type="FontAwesome" name="pencil" />
                        </Button>
                      </Col>
                    )}
                    renderRightHiddenRow={(data, secId, rowId, rowMap) => (
                      <Row>
                        <Col>
                          <Button
                            danger
                            onPress={_ => {
                              this.deleteInventoryByID(data, secId, rowId, rowMap);
                              rowMap[`${secId}${rowId}`].props.closeRow();
                            }}
                          >
                            <Icon active type="FontAwesome" name="trash" />
                          </Button>
                        </Col>
                      </Row>
                    )}
                    rightOpenValue={-75}
                    leftOpenValue={75}
                  />
                )}
                <Row style={{ flex: 0 }}>
                  <Col style={styles.pd10}>
                    {Boolean(!loadingSaveOrder) && (
                      <Button
                        style={{ marginTop: 10 }}
                        danger
                        full
                        onPress={this.cancelOrder}
                      >
                        <Text style={{ color: '#fff' }}> Clear Cart </Text>
                      </Button>
                    )}
                  </Col>
                  <Col style={styles.pd10}>
                    {Boolean(!loadingSaveOrder) && (
                      <Button
                        style={{ marginTop: 10 }}
                        success
                        full
                        onPress={this.toggleModalVisible}
                      >
                        <Text style={{ color: '#fff' }}> Save Order </Text>
                      </Button>
                    )}
                  </Col>
                </Row>

                {Boolean(loadingSaveOrder) && <Spinner  />}
              </Container>
            )}
          {Boolean(!cart.length) && (
            <Container style={styles.container}>
              <Text>Cart Is Empty</Text>
            </Container>
          )}
        </Container>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#d3da49',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 40
  },
  pd10: {
    padding: 10
  },
  inputContainer: {
    marginTop: 20
  }
});

function mapStateToProps(state) {
  const { CartReducer, AuthReducer } = state;
  return { CartReducer, AuthReducer };
}

function mapDispatchToProps(dispatch) {
  return {
    getCart: () => {
      dispatch({ type: cst.ADMIN_GET_CART });
    },
    removeItemFromCart: id => {
      dispatch({ type: cst.ADMIN_DEL_CART_ITEM, id });
    },
    createOrder: (customer) => {
      dispatch({ type: cst.CREATE_ADMIN_ORDER, order: { customer }});
    },
    clearCart: () => {
      dispatch({ type: cst.ADMIN_CLEAR_CART });
    },
    updateOrder: (order) => {
      dispatch({ type: cst.ADMIN_UPDATE_CART_ITEM, order });
    }
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdminCart);
