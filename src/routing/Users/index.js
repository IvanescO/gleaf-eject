import StackNavigator from 'react-navigation/src/navigators/StackNavigator';
import UsersList from '../../pages/Adminka/UsersScreens/UsersList';
import UserEdit from '../../pages/Adminka/UsersScreens/UserEdit';

const UsersRouting = StackNavigator(
  {
    UsersList: { screen: UsersList },
    UserEdit: { screen: UserEdit }
  },
  {
    initialRouteName: 'UsersList',
    headerMode: 'none',

    navigationOptions: {
      drawerLockMode: 'locked-closed',
      headerVisible: false
    }
  }
);

export default UsersRouting;
