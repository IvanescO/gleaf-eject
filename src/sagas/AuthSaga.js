/**
 * Watchers
 */
import { takeEvery, call, put, fork, all } from 'redux-saga/effects';
import {
  USER_AUTHENTICATION,
  AUTHENTICATION_FAILURE,
  AUTHENTICATION_SUCCESSFUL,
  USER_REGISTRATION,
  REGISTRATION__FAILURE,
  REGISTRATION_SUCCESSFUL,
  USER_LOGOUT
} from '../constants';
import AuthService from '../services/AuthService';
import { NavigationActions } from 'react-navigation';
import { AsyncStorage, Alert } from 'react-native';
import { Toast } from 'native-base';
// import { AsyncStorage } from 'expo';

function* userAthentication(action) {
  // console.log(action);
  const { cerdentials } = action.payload;
  console.log(cerdentials);
  // console.log(cerdentials);
  try {
    const response = yield call(AuthService.Login, cerdentials);
    console.log(response);
    console.log('//////')
    let { user, token } = response.data;
    console.log(user, token);

    user = {
      ...user.data,
      roles: user.roles
    };
    console.log(user);
    yield all([
      call(AsyncStorage.setItem, 'TOKEN', token),
      call(AsyncStorage.setItem, 'USER', JSON.stringify(user))
    ]);

    yield put({ type: AUTHENTICATION_SUCCESSFUL, token, user });
  } catch (e) {
    yield put({ type: AUTHENTICATION_FAILURE, errors: e.message });
  }
}

function* userLogout() {
  let [token, user] = yield all([AsyncStorage.getItem('TOKEN'), AsyncStorage.getItem('USER')]);
  console.log(token, user);
  yield all([call(AsyncStorage.removeItem, 'TOKEN'), call(AsyncStorage.removeItem, 'USER')]);
  [token, user] = yield all([AsyncStorage.getItem('TOKEN'), AsyncStorage.getItem('USER')]);
  console.log(token, user);
}

function* userRegistration(action) {
  try {
    const { cerdentials } = action.payload;
    let {
      data: { token, user }
    } = yield call(AuthService.Registration, cerdentials);
    user = {
      ...user.data,
      roles: user.roles
    };
    console.log(user);
    // yield all([
    //   call(AsyncStorage.setItem, 'TOKEN', token),
    //   call(AsyncStorage.setItem, 'USER', JSON.stringify(user))
    // ]);
    yield put({
      type: REGISTRATION_SUCCESSFUL,
      succsessMessage: 'Registration Successful'
      // user,
      // token
    });
    // Toast.show({
    //   text: 'Registration Successful',
    //   type: 'success'
    // });
  } catch (e) {
    yield put({ type: REGISTRATION__FAILURE, errors: e.message });
  }
}

function* watchUserLogin() {
  console.log('TRIGIRRRED');
  yield takeEvery(USER_AUTHENTICATION, userAthentication);
}

function* watchUserRegistration() {
  yield takeEvery(USER_REGISTRATION, userRegistration);
}

function* watchUserLogout() {
  yield takeEvery(USER_LOGOUT, userLogout);
}

export default function* () {
  yield [fork(watchUserLogin), fork(watchUserRegistration), fork(watchUserLogout)];
}
