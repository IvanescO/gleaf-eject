import DrawerNavigator from 'react-navigation/src/navigators/DrawerNavigator';
import Inventory from '../pages/InventoryScreens/Inventory';
import InventoryRouting from './Inventory';
import OrderRouting from './Orders';
import ReportRouting from './Reports';
import CustomersRouting from './Customers';
import MarketPlaceRouting from './Marketplace'
import UsersRouting from './Users';
// import Settings from './Settings';
// import CustomersList from '../pages/Customers/CustomersList';

const AdminRouter = DrawerNavigator(
  {
    Inventory: { screen: InventoryRouting },
    Marketplace: { screen: MarketPlaceRouting },
    Orders: { screen: OrderRouting },
    Users: { screen: UsersRouting },
    Customers: { screen: CustomersRouting },
    NewInventory: {
      screen: Inventory,
      navigationOptions: {
        // title: 'Home Page', //<= Does nothing
        drawerLabel: 'New Inventory'
        // drawerIcon: () => <MaterialCommunityIcons name={'home'} size={25} />
      }
    },
    Reports: { screen: ReportRouting }
  },
  {
    navigationOptions: {
      drawerLockMode: 'locked-closed'
    }
  }
  /*{
      headerMode: 'none',
      mode: 'modal',
      navigationOptions: {
        gesturesEnabled: false,
      }
    }*/
);
export default AdminRouter;
