import * as cst from '../constants/index';

const initialState = {
  cart: [],
  // addingSuccessMes: '',
  // addingError: '',
  loadingAdding: false,
  loadingGetting: false,
  loadingSaveOrder: false,
  expireTime: null,
  timestamp: null
};

function MarketplaceCartReducer(state = initialState, action) {
  switch (action.type) {
    case cst.MARKETPLACE_GET_CART: {
      return {
        ...state,
        loadingGetting: true
      };
    }
    case cst.MARKETPLACE_GET_CART_SUCCESSFUL: {
      return {
        ...state,
        total: action.total,
        cart: action.cart,
        loadingGetting: false,
        expireTime: action.expireTime,
        timestamp: action.timestamp
      };
    }
    case cst.MARKETPLACE_GET_CART_FAILURE: {
      return { ...state, loadingGetting: false };
    }
    case cst.MARKETPLACE_DELL_CART_ITEM: {
      return { ...state, loadingGetting: true };
    }
    case cst.MARKETPLACE_DELL_CART_ITEM_SUCCESSFUL: {
      return { ...state, cart: action.cart, loadingGetting: false };
    }
    case cst.MARKETPLACE_DELL_CART_ITEM_FAILURE: {
      return { ...state, loadingGetting: false  };
    }
    case cst.MARKETPLACE_ADD_CART_ITEM: {
      return {
        ...state,
        loadingAdding: true
      };
    }
    case cst.MARKETPLACE_ADD_CART_ITEM_SUCCESSFUL: {
      return {
        ...state,
        // addingError: '',
        loadingAdding: false,
        cart: action.cart,
        expireTime: action.expireTime,
        timestamp: action.timestamp,
        // addingSuccessMes: 'Item was added successfully'
      };
    }
    case cst.MARKETPLACE_ADD_CART_ITEM_FAILURE: {
      return {
        ...state,
        loadingAdding: false,
        // addingError: action.addingError,
        // addingSuccessMes: ''
      };
    }
    case cst.MARKETPLACE_DELL_CART_SUCCESSFUL: {
      console.log("MARKETPLACE_DELL_CART_SUCCESSFUL", action)
      return {
        ...state,
        cart: action.cart,
        expireTime: action.expireTime,
        timestamp: action.timestamp
      };
    }
    case cst.MARKETPLACE_CREATE_ORDER: {
      return { ...state, loadingSaveOrder: true };
    }
    case cst.MARKETPLACE_CREATE_ORDER_SUCCESSFUL: {
      const cart = action.cart || state.cart;
      return {
        ...state,
        cart: cart,
        saveMessage: action.message,
        loadingSaveOrder: false
      };
    }
    case cst.MARKETPLACE_CREATE_ORDER_FAILURE: {
      return { ...state, saveError: action.errors, loadingSaveOrder: false };
    }
    case cst.MARKETPLACE_CLEAR_CART: {
      return { ...state, loadingSaveOrder: true };
    }
    case cst.MARKETPLACE_CLEAR_CART_SUCCESSFUL: {
      return {
        ...state,
        loadingSaveOrder: false,
        cart: [],
        expireTime: 0,
        timestamp: 0
       };
    }
    case cst.MARKETPLACE_CLEAR_CART_FAILURE: {
      return { ...state, errors: action.errors, loadingSaveOrder: false };
    }

    default:
      return state;
  }
}
export default MarketplaceCartReducer;
