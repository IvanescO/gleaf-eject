import {
  USER_EDIT_STORE,
  CUSTOMER_EDIT_STORE,
  STORE_CUSTOMERS,
  STORE_USERS,
  UPDATE_CUSTOMER,
  UPDATE_USER
} from '../constants/index';

const initialState = {
  customers: null,
  users: null,
  user: null
};

function AdminEditing(state = initialState, action) {
  switch (action.type) {
    case USER_EDIT_STORE: {
      return { ...state, user: action.user };
    }
    case CUSTOMER_EDIT_STORE: {
      return { ...state, customer: action.customer };
    }
    case STORE_CUSTOMERS: {
      return { ...state, customers: action.customers };
    }
    case STORE_USERS: {
      return { ...state, users: action.users };
    }
    case UPDATE_CUSTOMER: {
      const customers = [...state.customers];
      const { index, customer } = action;
      customers[index] = customer;

      return { ...state, customers, customer };
    }
    case UPDATE_USER: {
      const users = [...state.users];
      const { index, user } = action;
      users[index] = user;
      return { ...state, users, user };
    }
    default:
      return state;
  }
}
export default AdminEditing;
