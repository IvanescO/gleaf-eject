import http from './http';

class CustomersService {
  static async getCustomersList(params) {
    return http.get('customer', { params });
  }
  static async getCustomersRoles() {
    return http.get('customer-roles');
  }
  static async getCustomerByID(cerdetials) {
    return http.post('registration', cerdetials);
  }
  static async ResetPassword() {}
}
export default CustomersService;
