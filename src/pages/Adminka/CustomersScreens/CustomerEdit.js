import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Layout from '../../../components/Layout';
import { Item, Input } from 'native-base';
import CustomerForm from './CustomerForm';
import { CUSTOMER_EDIT_STORE } from '../../../constants/index';
import { connect } from 'react-redux';
import UserService from '../../../services/UserService';

class CustomerEdit extends React.Component {
  state = { statuses: null };
  componentWillMount = async () => {
    const { params } = this.props.navigation.state;
    const { STORE_CUSTOMER } = this.props;
    const userStatus = await UserService.getUserStatue();
    // console.log(user);
    this.setState({ statuses: userStatus.data });
    STORE_CUSTOMER(params.customer);
  };

  render() {
    const TITLE = 'CustomerEdit';
    const { params } = this.props.navigation.state;
    console.log(this.props);
    return (
      <Layout title={TITLE} drawerOpen={() => this.props.navigation.navigate('DrawerOpen')}>
        <CustomerForm mode="customers" customerID={params.rowId} statuses={this.state.statuses} />
      </Layout>
    );
  }
}
function mapStateToProps(state) {
  return {};
}
function mapDispatchToProps(dispatch) {
  return {
    STORE_CUSTOMER: customer => {
      dispatch({ type: CUSTOMER_EDIT_STORE, customer });
    }
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerEdit);
