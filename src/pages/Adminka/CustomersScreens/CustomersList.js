import React from 'react';
import { StyleSheet, Text, View, RefreshControl, ListView } from 'react-native';
import Layout from '../../../components/Layout';
import CustomerService from '../../../services/CustomerService';
import { Grid, Row, Col, Container, List, ListItem, Button, Icon, Title } from 'native-base';
import { STORE_CUSTOMERS } from '../../../constants/index';
import { Pagination } from '../../../components';
import { connect } from 'react-redux';

class CustomerList extends React.Component {
  state = { customers: null, isRefreshing: false };
  componentWillMount = async () => {
    const usersResp = await CustomerService.getCustomersList({ page: 1 });
    const customers = usersResp.data.users;
    const { currentPage, total, itemsPerPage } = usersResp.data;
    const { storeCustomers } = this.props;
    storeCustomers(customers);
    this.setState({ customers, totalPage: Math.ceil(total / itemsPerPage), currentPage });
    console.log(this.state);
  };
  componentWillReceiveProps(nextProps) {
    const { customers } = nextProps;
    this.setState({ customers });
  }
  editUser = async (customer, secId, rowId) => {
    this.props.navigation.navigate('CustomerEdit', {
      customer,
      rowId
    });
  };
  ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

  goToPage = async num => {
    const usersResp = await CustomerService.getCustomersList({ page: num });
    const customers = usersResp.data.users;
    const { currentPage, total, itemsPerPage } = usersResp.data;
    const { storeCustomers } = this.props;
    storeCustomers(customers);
    this.setState({ customers, totalPage: Math.ceil(total / itemsPerPage), currentPage });
  };

  render() {
    const TITLE = 'CustomerList';
    const { customers } = this.state;
    return (
      <Layout title={TITLE} drawerOpen={() => this.props.navigation.navigate('DrawerOpen')}>
        {customers && (
          <Container>
            <List
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isRefreshing}
                  onRefresh={this.refreshCustomer}
                />
              }
              dataSource={this.ds.cloneWithRows(customers)}
              renderRow={data => (
                <ListItem>
                  <Grid>
                    <Row>
                      <Col style={styles.pdLeft5} size={22}>
                        <Text style={styles.fs12}>{data.user_login} </Text>
                      </Col>
                      <Col  size={50}>
                        <Text style={styles.fs12}>{data.user_email}</Text>
                      </Col>
                      <Col   size={28}>
                        <Text style={styles.fs12}>{data.roles}</Text>
                      </Col>
                    </Row>
                  </Grid>
                </ListItem>
              )}
              renderRightHiddenRow={(data, secId, rowId, rowMap) => (
                <Row>
                  <Col>
                    <Button
                      info
                      onPress={_ => {
                        this.editUser(data, secId, rowId, rowMap);
                        rowMap[`${secId}${rowId}`].props.closeRow();
                      }}
                    >
                      <Icon info type="FontAwesome" name="pencil" />
                    </Button>
                  </Col>
                </Row>
              )}
              renderHeader={() => (
                <Row>
                  <Col style={styles.pdLeft5} size={22}>
                    <Text>Login</Text>
                  </Col>
                  <Col size={50}>
                    <Text>Email</Text>
                  </Col>
                  <Col size={28}>
                    <Text>Roles </Text>
                  </Col>
                </Row>
              )}
              leftOpenValue={75}
              rightOpenValue={-50}
              //disableRightSwipe
            />
            <Pagination
              currentPage={this.state.currentPage}
              totalPage={this.state.totalPage}
              pageHandler={this.goToPage}
            />
          </Container>
        )}
      </Layout>
    );
  }
}

function mapStateToProps(state) {
  const {
    AdminEditing: { customers }
  } = state;
  return { customers };
}
function mapDispatchToProps(dispatch) {
  return {
    storeCustomers: customers => {
      dispatch({ type: STORE_CUSTOMERS, customers });
    }
  };
}
const styles = StyleSheet.create({
    fs12: {
        fontSize:12
    },
    pdLeft5:{
        paddingLeft: 5
    }
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerList);
