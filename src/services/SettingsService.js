import http from './http';

class SettingsService {
  static async fetchSettings() {
    return http.get('notification');
  }
  static async addSettings(settings) {
    return http.post('notification', settings);
  }
}
export default SettingsService;
