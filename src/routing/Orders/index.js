import StackNavigator from 'react-navigation/src/navigators/StackNavigator';
import OrdersList from '../../pages/OrdersScreens/OrdersList';
import Order from '../../pages/OrdersScreens/Order';
import OrderEdit from '../../pages/OrdersScreens/OrderEdit';
import OrderHistory from '../../pages/OrdersScreens/OrderHistory';
import Invoice from '../../pages/OrdersScreens/Invoice';

const OrderRouting = StackNavigator(
  {
    OrdersList: { screen: OrdersList },
    OrderInfo: { screen: Order },
    OrderHistory: { screen: OrderHistory },
    OrderEdit: { screen: OrderEdit },
    Invoice: { screen: Invoice }
  },
  {
    headerMode: 'none',
    navigationOptions: {
      drawerLockMode: 'locked-closed',
      headerVisible: false
    }
  }
);

export default OrderRouting;
