import React from 'react';
import { StyleSheet, Text, View, RefreshControl, ListView } from 'react-native';
import connect from 'react-redux/lib/connect/connect';
import { USER_AUTHENTICATION, SINGLE_ORDER } from '../../constants';
import Layout from '../../components/Layout';
import { Container, Col, List, ListItem, Icon, Button, Row, Content } from 'native-base';
import { colors } from '../../theme';

class Order extends React.Component {
  ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
  state = { singleOrder: { order_items: [] }, isRefreshing: false };
  componentWillMount() {
    const {
      params: { id, isMarket }
    } = this.props.navigation.state;
    const { getSingleOrder } = this.props;
    getSingleOrder(id, isMarket);
  }
  refreshOrderList = () => {};
  componentWillReceiveProps(nextProps) {
    const {
      OrdersReducer: { singleOrder }
    } = nextProps;

    this.setState({ singleOrder });
  }
  render() {
    const { OrdersReducer } = this.props;

    const { singleLoading } = OrdersReducer;
    console.log(OrdersReducer);
    console.log(singleLoading, singleOrder);
    const { singleOrder } = this.state;
    const {
      params: { id }
    } = this.props.navigation.state;
    const items = singleOrder ? singleOrder.order_items : [];
    const total = singleOrder.total_price ? `$${singleOrder.total_price}` : '';
    const TITLE = `Order ${id}`;

    return (
      <Layout title={TITLE} drawerOpen={() => this.props.navigation.navigate('DrawerOpen')}>
        {!singleLoading &&
          singleOrder && (
            <Container>
              <View style={{ height: 50 }}>
                <Row>
                  <Col size={1} style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text>type</Text>
                  </Col>

                  <Col size={2} style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text>thc</Text>
                  </Col>
                  <Col size={2} style={{ justifyContent: 'center' }}>
                    <Text>batch</Text>
                  </Col>
                  <Col size={2} style={{ justifyContent: 'center' }}>
                    <Text>quantity</Text>
                  </Col>
                </Row>
              </View>
              <List
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.isRefreshing}
                    onRefresh={this.refreshInventoryList}
                  />
                }
                dataSource={this.ds.cloneWithRows(items)}
                renderRow={data => (
                  <ListItem>
                    <Row>
                      <Col size={1} style={{ justifyContent: 'center' }}>
                        <Text>{data.inventory_type}</Text>
                      </Col>
                      <Col size={1} style={{ justifyContent: 'center' }}>
                        <Text>{data.thc}</Text>
                      </Col>
                      <Col size={1} style={{ justifyContent: 'center' }}>
                        <Text>{data.batch}</Text>
                      </Col>
                      <Col size={1} style={{ justifyContent: 'center' }}>
                        <Text>{data.quantity}</Text>
                      </Col>
                    </Row>
                  </ListItem>
                )}
                renderFooter={() => (
                  <Row
                    style={{
                      justifyContent: 'space-between',
                      paddingRight: 20
                    }}
                  >
                    <Text style={{ color: colors.black, fontSize: 14 }}>Total Price {total}</Text>
                  </Row>
                )}
                renderRightHiddenRow={(data, secId, rowId, rowMap) => (
                  <Row>
                    <Col>
                      <Button info onPress={_ => this.editInventory(data, secId, rowId, rowMap)}>
                        <Icon info type="FontAwesome" name="pencil" />
                      </Button>
                    </Col>
                    <Col>
                      <Button
                        danger
                        onPress={_ => this.deleteInventoryByID(data, secId, rowId, rowMap)}
                      >
                        <Icon active type="FontAwesome" name="trash" />
                      </Button>
                    </Col>
                  </Row>
                )}
                // leftOpenValue={75}
                disableRightSwipe
                disableLeftSwipe
                rightOpenValue={-150}
              />
            </Container>
          )}
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#d3da49',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 40
  },
  inputContainer: {
    marginTop: 20
  }
});
function mapStateToProps(state) {
  const { OrdersReducer } = state;
  return { OrdersReducer };
}
function mapDispatchToProps(dispatch) {
  return {
    getSingleOrder: (id, isMarket) => {
      dispatch({ type: SINGLE_ORDER, id, isMarket });
    }
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Order);
