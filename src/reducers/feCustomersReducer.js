import {
  FE_CUSTOMER_LIST_SUCCESSFUL,
  FE_CUSTOMER_LIST_FAILURE,
  CREATE_FE_CUSTOMER_SUCCESSFUL,
  CREATE_FE_CUSTOMER_FAILURE,
  FE_CUSTOMER_STORE
} from '../constants/index';

const initialState = {
  feCustomerLoading: true,
  feCustomers: null
};

function feCustomersReducer(state = initialState, action) {
  switch (action.type) {
    case FE_CUSTOMER_LIST_SUCCESSFUL: {
      const { currentPage, itemsPerPage, total, feCustomers } = action;
      console.log(currentPage, itemsPerPage, total, feCustomers);
      return { ...state, currentPage, itemsPerPage, total, feCustomers, feCustomerLoading: false };
    }
    case FE_CUSTOMER_LIST_FAILURE: {
      return { ...state, error: action.errors.message, feCustomerLoading: false };
    }

    case FE_CUSTOMER_STORE: {
      console.log('FE_CUSTOMER_STORE', action);
      const { feCustomer } = action;
      return { ...state, feCustomer };
    }

    case CREATE_FE_CUSTOMER_SUCCESSFUL: {
      console.log('CREATE_FE_CUSTOMER_SUCCESSFUL', action);
      const { feCustomers } = state;
      const { payload } = action;
      const newCustomers = [...feCustomers, payload];
      return { ...state, feCustomers: newCustomers };
    }
    case CREATE_FE_CUSTOMER_FAILURE: {
      return { ...state, error: action.errors.message };
    }

    default:
      return state;
  }
}
export default feCustomersReducer;
