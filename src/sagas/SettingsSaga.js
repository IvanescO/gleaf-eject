/**
 * Watchers
 */
import { takeEvery, call, put, fork } from 'redux-saga/effects';
import {
  SETTINGS_GET_REQUEST,
  SETTINGS_GET_SUCCESSFUL,
  SETTINGS_GET_FAILURE,
  SETTINGS_UPDATE_REQUEST,
  SETTINGS_UPDATE_SUCCESSFUL,
  SETTINGS_UPDATE_FAILURE
} from '../constants';
import SettingsService from '../services/SettingsService';

function* getSettings() {
  try {
    const response = yield call(SettingsService.fetchSettings);
    const payload = response.data;

    yield put({ type: SETTINGS_GET_SUCCESSFUL, payload });
  } catch (e) {
    yield put({ type: SETTINGS_GET_FAILURE, errors: e.message });
  }
}

function* addSettings(action) {
  try {
    const settings = action.payload;
    const request = yield call(SettingsService.addSettings, settings);
    const payload = request.data;

    yield put({ type: SETTINGS_UPDATE_SUCCESSFUL, payload });
  } catch (e) {
    yield put({ type: SETTINGS_UPDATE_FAILURE, errors: e.message });
  }
}

function* watchGetSettings() {
  yield takeEvery(SETTINGS_GET_REQUEST, getSettings);
}

function* watchSetSettings() {
  yield takeEvery(SETTINGS_UPDATE_REQUEST, addSettings);
}

export default function* () {
  yield [fork(watchGetSettings), fork(watchSetSettings)];
}
