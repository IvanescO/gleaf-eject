import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Fields, Field, reduxForm, submit, values, change } from 'redux-form';
import Layout from '../../../components/Layout';
import {
  Item,
  Input,
  Content,
  Label,
  Icon,
  Spinner,
  Form,
  Picker,
  Row,
  Col,
  Button,
    Toast
} from 'native-base';
import { connect } from 'react-redux';
import UserService from '../../../services/UserService';
import { UPDATE_CUSTOMER } from '../../../constants/index';
import { colors } from '../../../theme';

const validate = values => {
  const errors = {};
    console.log(values);
  if (!values.first_name) {
    errors.first_name = 'Required';
  }
  if (!values.last_name) {
    errors.last_name = 'Required';
  }
  if (!values.user_email) {
    errors.user_email = 'Required';
  }
  return errors;
};

class CustomerForm extends React.Component {
  state = { initialValues: null };
  componentWillReceiveProps(nextProps) {
    const { initialValues } = nextProps;
    this.setState({ initialValues });
  }
  submit = async values => {
    this.setState({ updatingError: '' });
    try {
      const { customerID, updateCustomer } = this.props;
      console.log(customerID);
      console.log(values);
      const { initialValues:{advanced_customer,account_status, receive_notifications_email,receive_notifications_sms } } = this.state;
      values = {...values, advanced_customer,account_status, receive_notifications_email,receive_notifications_sms};
      console.log(values);
      const user = await UserService.updUser(values);
      // console.log('___________');
      // console.log(customerID);
      // console.log('___________');

      updateCustomer(user.data, customerID);
        Toast.show({
          text: 'Updating Successful',
          type: 'success'
        });
    } catch (updatingError) {
      this.setState({ updatingError });
    }
  };
  renderInput = ({ input, disabled, label, type, meta: { touched, error, warning } }) => {
    let hasError = false;
    if (error !== undefined) {
      hasError = true;
    }
    return (
      <Item disabled={disabled} style={{ margin: 10 }} error={hasError}>
        <Label>{label}</Label>
        <Input disabled={disabled} {...input} />
        {hasError ? <Icon name="close-circle" /> : <Text />}
      </Item>
    );
  };

  render() {
    const TITLE = 'UserEdit';
    const { handleSubmit } = this.props;
    const { initialValues } = this.state;
    if (initialValues) {
      return (
        <Content>
          <Form>
            <Field disabled label="Login" name="user_login" component={this.renderInput} />
            <Field label="First Name" name="first_name" component={this.renderInput} />
            <Field label="Last Name" name="last_name" component={this.renderInput} />
            <Field disabled label="Role" name="roles" component={this.renderInput} />
            <Field label="Email" name="user_email" component={this.renderInput} />
            <Field label="Display name" name="display_name" component={this.renderInput} />
            <Field label="Extra email" name="extra_email" component={this.renderInput} />
            <Field label="Dispensary name" name="dispensary_name" component={this.renderInput} />
            <Field label="Address" name="address" component={this.renderInput} />
            <Row>
              <Col size={50} style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: 16 }}>Advanced customer</Text>
              </Col>
              <Col size={50}>
                <Item picker>
                  <Picker
                    mode="dropdown"
                    iosIcon={<Icon name="ios-arrow-down-outline" />}
                    style={{ width: undefined }}
                    placeholder="Status"
                    placeholderStyle={{ color: '#bfc6ea' }}
                    placeholderIconColor="#007aff"
                    selectedValue={initialValues.advanced_customer} //TODO: Change it
                    onValueChange={advanced_customer => {
                      console.log(advanced_customer);
                      const initialValues = {
                        ...this.state.initialValues,
                          advanced_customer
                      };
                      this.setState({ initialValues });
                      change('customerEdit', 'advanced_customer', advanced_customer);
                    }}
                  >
                    <Picker.Item label="Yes" value={Boolean(true)} />
                    <Picker.Item label="No" value={false} />
                  </Picker>
                </Item>
              </Col>
            </Row>
            <Row>
              <Col size={30} style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: 16 }}>User Status</Text>
              </Col>
              <Col size={70}>
                <Item picker>
                  <Picker
                    mode="dropdown"
                    iosIcon={<Icon name="ios-arrow-down-outline" />}
                    style={{ width: undefined }}
                    placeholder="Status"
                    placeholderStyle={{ color: '#bfc6ea' }}
                    placeholderIconColor="#007aff"
                    selectedValue={initialValues.account_status}
                    onValueChange={account_status => {
                      const initialValues = {
                        ...this.state.initialValues,
                        account_status
                      };
                      this.setState({ initialValues });
                      change('customerEdit', 'account_status', account_status);
                    }}
                  >
                    <Picker.Item label="Pending" value="awaiting_admin_review" />
                    <Picker.Item label="Active" value="approved" />
                    <Picker.Item label="Rejected" value="rejected" />
                  </Picker>
                </Item>
              </Col>
            </Row>

            <Field label="Phone number" name="phone_number" component={this.renderInput} />
            {/* TODO: FIX THIS BLOCK*/}
            <Row>
              <Col size={50} style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: 15 }}>notifications by email</Text>
              </Col>
              <Col size={50}>
                <Item picker>
                  <Picker
                    mode="dropdown"
                    iosIcon={<Icon name="ios-arrow-down-outline" />}
                    style={{ width: undefined }}
                    placeholder="Status"
                    placeholderStyle={{ color: '#bfc6ea' }}
                    placeholderIconColor="#007aff"
                    selectedValue={initialValues.receive_notifications_email} //TODO: Change it
                    onValueChange={receive_notifications_email => {
                      const initialValues = {
                        ...this.state.initialValues,
                        receive_notifications_email
                      };
                      console.log(initialValues);
                      this.setState({ initialValues });
                      change(
                        'customerEdit',
                        'receive_notifications_email',
                        receive_notifications_email
                      );
                    }}
                  >
                    <Picker.Item label="Yes" value={Boolean(true)} />
                    <Picker.Item label="No" value={false} />
                  </Picker>
                </Item>
              </Col>
            </Row>
            <Field
              label="notifications email"
              name="notification_email"
              component={this.renderInput}
            />
            <Row>
              <Col size={50} style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: 15 }}>notifications by sms</Text>
              </Col>
              <Col size={50}>
                <Item picker>
                  <Picker
                    mode="dropdown"
                    iosIcon={<Icon name="ios-arrow-down-outline" />}
                    style={{ width: undefined }}
                    placeholder="Status"
                    placeholderStyle={{ color: '#bfc6ea' }}
                    placeholderIconColor="#007aff"
                    selectedValue={initialValues.receive_notifications_sms}
                    onValueChange={receive_notifications_sms => {
                      const initialValues = {
                        ...this.state.initialValues,
                        receive_notifications_sms
                      };
                      console.log(initialValues);
                      this.setState({ initialValues });
                      change('customerEdit', 'receive_notifications_sms', receive_notifications_sms);
                    }}
                  >
                    <Picker.Item label="Yes" value={Boolean(true)} />
                    <Picker.Item label="No" value={false} />
                  </Picker>
                </Item>
              </Col>
            </Row>
            <Field
              label="notifications phone"
              name="notification_phone"
              component={this.renderInput}
            />
            <Field label="QB customer id" name="qb_customer_id" component={this.renderInput} />
            <Button style={{ marginTop: 10 }} success block onPress={handleSubmit(this.submit)}>
              <Text style={{ color: '#fff' }}> Update Customer </Text>
            </Button>
            {this.state.updatingError && (
              <Text style={{ color: colors.error }}>{this.state.updatingError}</Text>
            )}
          </Form>
          {/*
        <Field label="Email for notifications" component={this.renderInput} />
        <Field label="Phone for notifications" component={this.renderInput} /> */}
        </Content>
      );
    }
    return <Spinner />;
  }
}
const withReduxForm = reduxForm({
  form: 'customerEdit', // a unique identifier for this form
  validate
})(CustomerForm);

const mapStateToProps = state => {
  const customer = state.AdminEditing.customer;

  if (!customer) return {};
  return {
    initialValues: {
      ...customer,
      roles: customer.roles.toString()
    }
  };
};
function mapDispatchToProps(dispatch) {
  return {
    updateCustomer: (customer, index) => {
      dispatch({ type: UPDATE_CUSTOMER, customer, index });
    }
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withReduxForm);
