import StackNavigator from 'react-navigation/src/navigators/StackNavigator';
import feCustomersComponent from '../../pages/feCustomers/feCustomers';
import customer from '../../pages/feCustomers/customer';

const feCustomersRouting = StackNavigator(
  {
    feCustomers: { screen: feCustomersComponent },
    customer: { screen: customer }
  },
  {
    initialRouteName: 'feCustomers',
    headerMode: 'none',

    navigationOptions: {
      drawerLockMode: 'locked-closed',
      headerVisible: false
    }
  }
);

export default feCustomersRouting;
